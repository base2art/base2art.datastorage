﻿namespace Base2art.DataStorage.Provider.SqlServer
{
    using System;
    using Base2art.Dapper;
    using Base2art.DataStorage.Dapper;
    using Base2art.DataStorage.SqlServer;
    
    public class SqlServerProvider : IDataStorageProvider
    {
        private readonly bool isDebug;

        private readonly TimeSpan defaultCommandTimeout;
        
        public SqlServerProvider(bool isDebug, TimeSpan defaultCommandTimeout)
        {
            this.defaultCommandTimeout = defaultCommandTimeout;
            this.isDebug = isDebug;
        }
        
        public IDataStore CreateDataStoreAccess(NamedConnectionString named)
        {
            return new DataStore(this.Backing(named));
        }
        
        public IDbms CreateDbmsAccess(NamedConnectionString named)
        {
            return new Dbms(this.Backing(named));
        }

        private SqlFormatter Backing(NamedConnectionString named)
        {
            var cstr = named.ConnectionString;
            
            var factory = new OdbcConnectionFactory<System.Data.SqlClient.SqlConnection>(cstr);
            var schema = string.Empty;
            if (named.AdditionalParameters != null && named.AdditionalParameters.ContainsKey("schema"))
            {
                schema = named.AdditionalParameters["schema"];
            }
            
            return new SqlFormatter(
                new DapperExecutionEngine(factory), 
                this.isDebug, 
                schema, 
                this.defaultCommandTimeout);
        }
    }
}