﻿
namespace Base2art.DataStorage.Dapper
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Base2art.Dapper;

    public class DapperExecutionEngine : IExecutionEngine
    {
        private readonly IConnectionFactory factory;
        
        public DapperExecutionEngine(IConnectionFactory factory)
        {
            this.factory = factory;
        }
        
        public Task<IEnumerable<T>> QueryReaderAsync<T>(string sql, Dictionary<string, object> data, IDataConverter dataConverter, TimeSpan? timeout)
        {
            return factory.QueryReaderAsync<T>(sql, data, x => Parser.Parse<T>(x, (y, z) => dataConverter.ConvertDataFromStorage(y, z)), timeout);
        }

        public IEnumerable<T> QueryReader<T>(string sql, Dictionary<string, object> data, IDataConverter dataConverter, TimeSpan? timeout)
        {
            return factory.QueryReader<T>(sql, data, x => Parser.Parse<T>(x, (y, z) => dataConverter.ConvertDataFromStorage(y, z)), timeout);
        }

        public Task<IEnumerable<Tuple<T1, T2>>> QueryReaderAsync<T1, T2>(string sql, Dictionary<string, object> data, IDataConverter dataConverter, TimeSpan? timeout)
        {
            return factory.QueryReaderAsync<Tuple<T1, T2>>(sql, data, x => Parser.ParseTuple<T1, T2>(x, (y, z) => dataConverter.ConvertDataFromStorage(y, z)), timeout);
        }
        
        public IEnumerable<Tuple<T1, T2>> QueryReader<T1, T2>(string sql, Dictionary<string, object> data, IDataConverter dataConverter, TimeSpan? timeout)
        {
            return factory.QueryReader<Tuple<T1, T2>>(sql, data, x => Parser.ParseTuple<T1, T2>(x, (y, z) => dataConverter.ConvertDataFromStorage(y, z)), timeout);
        }
        
        public Task<IEnumerable<Tuple<T1, T2, T3>>> QueryReaderAsync<T1, T2, T3>(string sql, Dictionary<string, object> data, IDataConverter dataConverter, TimeSpan? timeout)
        {
            return factory.QueryReaderAsync<Tuple<T1, T2, T3>>(sql, data, x => Parser.ParseTuple<T1, T2, T3>(x, (y, z) => dataConverter.ConvertDataFromStorage(y, z)), timeout);
        }
        
        public IEnumerable<Tuple<T1, T2, T3>> QueryReader<T1, T2, T3>(string sql, Dictionary<string, object> data, IDataConverter dataConverter, TimeSpan? timeout)
        {
            return factory.QueryReader<Tuple<T1, T2, T3>>(sql, data, x => Parser.ParseTuple<T1, T2, T3>(x, (y, z) => dataConverter.ConvertDataFromStorage(y, z)), timeout);
        }
        
        public Task<IEnumerable<Tuple<T1, T2, T3, T4>>> QueryReaderAsync<T1, T2, T3, T4>(string sql, Dictionary<string, object> data, IDataConverter dataConverter, TimeSpan? timeout)
        {
            return factory.QueryReaderAsync<Tuple<T1, T2, T3, T4>>(sql, data, x => Parser.ParseTuple<T1, T2, T3, T4>(x, (y, z) => dataConverter.ConvertDataFromStorage(y, z)), timeout);
        }
        
        public IEnumerable<Tuple<T1, T2, T3, T4>> QueryReader<T1, T2, T3, T4>(string sql, Dictionary<string, object> data, IDataConverter dataConverter, TimeSpan? timeout)
        {
            return factory.QueryReader<Tuple<T1, T2, T3, T4>>(sql, data, x => Parser.ParseTuple<T1, T2, T3, T4>(x, (y, z) => dataConverter.ConvertDataFromStorage(y, z)), timeout);
        }
        
        public Task<IEnumerable<Tuple<T1, T2, T3, T4, T5>>> QueryReaderAsync<T1, T2, T3, T4, T5>(string sql, Dictionary<string, object> data, IDataConverter dataConverter, TimeSpan? timeout)
        {
            return factory.QueryReaderAsync<Tuple<T1, T2, T3, T4, T5>>(sql, data, x => Parser.ParseTuple<T1, T2, T3, T4, T5>(x, (y, z) => dataConverter.ConvertDataFromStorage(y, z)), timeout);
        }
        
        public IEnumerable<Tuple<T1, T2, T3, T4, T5>> QueryReader<T1, T2, T3, T4, T5>(string sql, Dictionary<string, object> data, IDataConverter dataConverter, TimeSpan? timeout)
        {
            return factory.QueryReader<Tuple<T1, T2, T3, T4, T5>>(sql, data, x => Parser.ParseTuple<T1, T2, T3, T4, T5>(x, (y, z) => dataConverter.ConvertDataFromStorage(y, z)), timeout);
        }
        
        public Task<IEnumerable<Tuple<T1, T2, T3, T4, T5, T6>>> QueryReaderAsync<T1, T2, T3, T4, T5, T6>(string sql, Dictionary<string, object> data, IDataConverter dataConverter, TimeSpan? timeout)
        {
            return factory.QueryReaderAsync<Tuple<T1, T2, T3, T4, T5, T6>>(sql, data, x => Parser.ParseTuple<T1, T2, T3, T4, T5, T6>(x, (y, z) => dataConverter.ConvertDataFromStorage(y, z)), timeout);
        }
        
        public IEnumerable<Tuple<T1, T2, T3, T4, T5, T6>> QueryReader<T1, T2, T3, T4, T5, T6>(string sql, Dictionary<string, object> data, IDataConverter dataConverter, TimeSpan? timeout)
        {
            return factory.QueryReader<Tuple<T1, T2, T3, T4, T5, T6>>(sql, data, x => Parser.ParseTuple<T1, T2, T3, T4, T5, T6>(x, (y, z) => dataConverter.ConvertDataFromStorage(y, z)), timeout);
        }
        
        public Task<IEnumerable<Tuple<T1, T2, T3, T4, T5, T6, T7>>> QueryReaderAsync<T1, T2, T3, T4, T5, T6, T7>(string sql, Dictionary<string, object> data, IDataConverter dataConverter, TimeSpan? timeout)
        {
            return factory.QueryReaderAsync<Tuple<T1, T2, T3, T4, T5, T6, T7>>(sql, data, x => Parser.ParseTuple<T1, T2, T3, T4, T5, T6, T7>(x, (y, z) => dataConverter.ConvertDataFromStorage(y, z)), timeout);
        }
        
        public IEnumerable<Tuple<T1, T2, T3, T4, T5, T6, T7>> QueryReader<T1, T2, T3, T4, T5, T6, T7>(string sql, Dictionary<string, object> data, IDataConverter dataConverter, TimeSpan? timeout)
        {
            return factory.QueryReader<Tuple<T1, T2, T3, T4, T5, T6, T7>>(sql, data, x => Parser.ParseTuple<T1, T2, T3, T4, T5, T6, T7>(x, (y, z) => dataConverter.ConvertDataFromStorage(y, z)), timeout);
        }
        
        public Task Execute(string sql, Dictionary<string, object> data, TimeSpan? timeout)
        {
            return factory.ExecuteAsync(sql, data, timeout);
        }
    }
}
