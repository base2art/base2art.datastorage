﻿namespace Base2art.DataStorage
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Reflection;
    using System.Threading.Tasks;
    using Base2art.DataStorage.DataDefinition;
    using Base2art.DataStorage.DataManipulation;
    using Base2art.DataStorage.DataManipulation.Data;

    public abstract class FormatterBase : IDataDefiner, IDataManipulator
    {
        private readonly bool isDebug;

        private readonly SharedBuilderMaps commonBuilder;

        private readonly IExecutionEngine factory;

        private readonly IStorageTypeMap typeMap;

        private readonly string defaultSchema;

        private readonly TimeSpan defaultTimeout;
        
        protected FormatterBase(
            IExecutionEngine factory,
            string defaultSchema,
            bool isDebug,
            SharedBuilderMaps commonBuilder,
            IStorageTypeMap typeMap, 
            TimeSpan defaultTimeout)
        {
            this.defaultTimeout = defaultTimeout;
            this.defaultSchema = defaultSchema;
            this.typeMap = typeMap;
            this.factory = factory;
            this.commonBuilder = commonBuilder;
            this.isDebug = isDebug;
        }

        protected virtual TimeSpan? Timeout
        {
            get { return this.defaultTimeout > TimeSpan.Zero ? (TimeSpan?)this.defaultTimeout : null; }
        }
        
        public virtual bool SupportsMultipleStatementsPerCommand
        {
            get { return true; }
        }
        
        public virtual IEscapeCharacters EscapeCharacters
        {
            get { return this.commonBuilder; }
        }

        protected abstract string ExceptionTypeName
        {
            get;
        }
        
        public Task InsertAsync(string schemaName, string tableName, FieldList list, SelectData records)
        {
            return this.RunAsyncAction(this.CreateInsertSelectBuilder, (x, data) => x.BuildSql(this.defaultSchema, this.SchemaName(schemaName), tableName, list, records, "", data));
        }
        
        public Task InsertAsync(string schemaName, string tableName, IReadOnlyList<SetsList> records)
        {
            return this.RunAsyncAction(this.CreateInsertRecordsBuilder, (x, data) => x.BuildSql(this.SchemaName(schemaName), tableName, records, "", data));
        }

        public Task<IEnumerable<T>> SelectAsync<T>(SelectData selectData)
        {
            return this.SelectSqlAsync(selectData, this.defaultSchema, (sql, data) => factory.QueryReaderAsync<T>(sql, data, this.commonBuilder, this.Timeout));
        }

        public IEnumerable<T> Select<T>(SelectData selectData)
        {
            return this.SelectSql(selectData, this.defaultSchema, (sql, data) => factory.QueryReader<T>(sql, data, this.commonBuilder, this.Timeout));
        }

        public Task<IEnumerable<Tuple<T1, T2>>> SelectAsync<T1, T2>(SelectData selectData)
        {
            return this.SelectSqlAsync(selectData, this.defaultSchema, (sql, data) => factory.QueryReaderAsync<T1, T2>(sql, data, this.commonBuilder, this.Timeout));
        }

        public IEnumerable<Tuple<T1, T2>> Select<T1, T2>(SelectData selectData)
        {
            return this.SelectSql(selectData, this.defaultSchema, (sql, data) => factory.QueryReader<T1, T2>(sql, data, this.commonBuilder, this.Timeout));
        }

        public Task<IEnumerable<Tuple<T1, T2, T3>>> SelectAsync<T1, T2, T3>(SelectData selectData)
        {
            return this.SelectSqlAsync(selectData, this.defaultSchema, (sql, data) => factory.QueryReaderAsync<T1, T2, T3>(sql, data, this.commonBuilder, this.Timeout));
        }

        public IEnumerable<Tuple<T1, T2, T3>> Select<T1, T2, T3>(SelectData selectData)
        {
            return this.SelectSql(selectData, this.defaultSchema, (sql, data) => factory.QueryReader<T1, T2, T3>(sql, data, this.commonBuilder, this.Timeout));
        }

        public Task<IEnumerable<Tuple<T1, T2, T3, T4>>> SelectAsync<T1, T2, T3, T4>(SelectData selectData)
        {
            return this.SelectSqlAsync(selectData, this.defaultSchema, (sql, data) => factory.QueryReaderAsync<T1, T2, T3, T4>(sql, data, this.commonBuilder, this.Timeout));
        }

        public IEnumerable<Tuple<T1, T2, T3, T4>> Select<T1, T2, T3, T4>(SelectData selectData)
        {
            return this.SelectSql(selectData, this.defaultSchema, (sql, data) => factory.QueryReader<T1, T2, T3, T4>(sql, data, this.commonBuilder, this.Timeout));
        }

        public Task<IEnumerable<Tuple<T1, T2, T3, T4, T5>>> SelectAsync<T1, T2, T3, T4, T5>(SelectData selectData)
        {
            return this.SelectSqlAsync(selectData, this.defaultSchema, (sql, data) => factory.QueryReaderAsync<T1, T2, T3, T4, T5>(sql, data, this.commonBuilder, this.Timeout));
        }

        public IEnumerable<Tuple<T1, T2, T3, T4, T5>> Select<T1, T2, T3, T4, T5>(SelectData selectData)
        {
            return this.SelectSql(selectData, this.defaultSchema, (sql, data) => factory.QueryReader<T1, T2, T3, T4, T5>(sql, data, this.commonBuilder, this.Timeout));
        }

        public Task<IEnumerable<Tuple<T1, T2, T3, T4, T5, T6>>> SelectAsync<T1, T2, T3, T4, T5, T6>(SelectData selectData)
        {
            return this.SelectSqlAsync(selectData, this.defaultSchema, (sql, data) => factory.QueryReaderAsync<T1, T2, T3, T4, T5, T6>(sql, data, this.commonBuilder, this.Timeout));
        }

        public IEnumerable<Tuple<T1, T2, T3, T4, T5, T6>> Select<T1, T2, T3, T4, T5, T6>(SelectData selectData)
        {
            return this.SelectSql(selectData, this.defaultSchema, (sql, data) => factory.QueryReader<T1, T2, T3, T4, T5, T6>(sql, data, this.commonBuilder, this.Timeout));
        }

        public Task<IEnumerable<Tuple<T1, T2, T3, T4, T5, T6, T7>>> SelectAsync<T1, T2, T3, T4, T5, T6, T7>(SelectData selectData)
        {
            return this.SelectSqlAsync(selectData, this.defaultSchema, (sql, data) => factory.QueryReaderAsync<T1, T2, T3, T4, T5, T6, T7>(sql, data, this.commonBuilder, this.Timeout));
        }

        public IEnumerable<Tuple<T1, T2, T3, T4, T5, T6, T7>> Select<T1, T2, T3, T4, T5, T6, T7>(SelectData selectData)
        {
            return this.SelectSql(selectData, this.defaultSchema, (sql, data) => factory.QueryReader<T1, T2, T3, T4, T5, T6, T7>(sql, data, this.commonBuilder, this.Timeout));
        }

        public Task UpdateAsync(string schemaName, string tableName, SetsList sets, WhereClause wheres)
        {
            return this.RunAsyncAction(this.CreateUpdateBuilder, (builder, data) => builder.BuildSql(this.defaultSchema, this.SchemaName(schemaName), tableName, sets, wheres, "", data));
        }

        public Task DeleteAsync(string schemaName, string tableName, WhereClause wheres)
        {
            return this.RunAsyncAction(this.CreateDeleteBuilder, (builder, data) => builder.BuildSql(this.defaultSchema, this.SchemaName(schemaName), tableName, wheres, "", data));
        }

        public Task CreateTable(
            string schemaName,
            string tableName,
            bool allowUpdate,
            bool ifNotExists,
            IReadOnlyList<Tuple<string, DataTypes, bool, RangeConstraint>> records,
            ILookup<string, string> indexes,
            ILookup<string, string> keyes)
        {
            if (allowUpdate)
            {
                return this.UpdateTable(this.SchemaName(schemaName), tableName, ifNotExists, records, indexes, keyes);
            }
            
            return this.CreateTableInternal(this.SchemaName(schemaName), tableName, ifNotExists, records, indexes, keyes);
        }

        public Task DropTable(string schemaName, string tableName, bool ifExists)
        {
            return this.RunAsyncAction(
                x => this.CreateDropTableBuilder(),
                (builder, data) => builder.BuildSql(this.SchemaName(schemaName), tableName, ifExists));
        }
        
        protected abstract InsertSelectBuilder CreateInsertSelectBuilder(SharedBuilderMaps commonBuilder);

        protected abstract InsertRecordsBuilder CreateInsertRecordsBuilder(SharedBuilderMaps commonBuilder);
        
        protected abstract SelectBuilder CreateSelectBuilder(SharedBuilderMaps commonBuilder);
        
        protected abstract UpdateBuilder CreateUpdateBuilder(SharedBuilderMaps commonBuilder);
        
        protected abstract DeleteBuilder CreateDeleteBuilder(SharedBuilderMaps commonBuilder);
        
        protected abstract CreateTableBuilder CreateTableBuilder(IStorageTypeMap commonBuilder);
        
        protected abstract CreateTableAltererBuilder CreateTableAltererBuilder(IStorageTypeMap commonBuilder);
        
        protected abstract CreateTableIndexesBuilder CreateTableIndexesBuilder(IStorageTypeMap commonBuilder);
        
        protected abstract CreateTableKeysBuilder CreateTableKeysBuilder(IStorageTypeMap commonBuilder);
        
        protected abstract DropTableBuilder CreateDropTableBuilder();

        private J SelectSql<J>(SelectData selectData, string defaultSchema, Func<string, Dictionary<string, object>, J> runner)
            where J : class
        {
            return this.Run(this.CreateSelectBuilder, (x, data) => x.BuildSql(selectData, defaultSchema, "", data, true, true), runner, () => default(J));
        }
        
        private Task<J> SelectSqlAsync<J>(SelectData selectData, string defaultSchema, Func<string, Dictionary<string, object>, Task<J>> runner)
            where J : class
        {
            return this.Run(this.CreateSelectBuilder, (x, data) => x.BuildSql(selectData, defaultSchema, "", data, true, true), runner, () => Task.FromResult(default(J)));
        }
        
        private Task RunAsyncAction<Builder>(
            Func<SharedBuilderMaps, Builder> createBuilder,
            Func<Builder, Dictionary<string, object>, string> build)
        {
            return this.Run(createBuilder, build, (s, r) => this.factory.Execute(s, r, this.Timeout), () => Task.FromResult(true));
        }

        private J Run<Builder, J>(
            Func<SharedBuilderMaps, Builder> createBuilder,
            Func<Builder, Dictionary<string, object>, string> build,
            Func<string, Dictionary<string, object>, J> runner,
            Func<J> defaultCreator)
        {
            var builder = createBuilder(this.commonBuilder);
            var data = new Dictionary<string, object>();
            var sql = build(builder, data);
            if (string.IsNullOrWhiteSpace(sql))
            {
                this.Log("No SQL: " + MethodBase.GetCurrentMethod().ToString(), data);
                return defaultCreator();
            }
            
            this.Log(sql, data);
            return runner(sql, data);
        }
        
        private void Log(string sql, Dictionary<string, object> data)
        {
            if (!this.isDebug)
            {
                return;
            }
            
            Console.WriteLine(sql);
            Console.WriteLine("{");
            foreach (var key in data.Keys)
            {
                Console.WriteLine("  {0} : `{1}`", key, data[key]);
            }
            
            Console.WriteLine("}");
        }

        private async Task UpdateTable(
            string schemaName,
            string tableName,
            bool ifNotExists,
            IReadOnlyList<Tuple<string, DataTypes, bool, RangeConstraint>> records,
            ILookup<string, string> indexes,
            ILookup<string, string> keyes)
        {
            if (await this.TableExists(schemaName, tableName))
            {
                KeyedCollection<string, Tuple<string, DataTypes, bool, RangeConstraint>> columnsToAdd =
                    new MyKeyedCollection<DataTypes, bool, RangeConstraint>();
                
                foreach (var columnName in records)
                {
                    var colName = columnName.Item1;
                    if (!await this.TableColumnExists(schemaName, tableName, colName, typeof(string)))
                    {
                        columnsToAdd.Add(columnName);
                    }
                }
                
                if (columnsToAdd.Count > 0)
                {
                    await this.RunAsyncAction(
                        (x) => this.CreateTableAltererBuilder(this.typeMap),
                        (builder, data) => builder.BuildSql(schemaName, tableName, ifNotExists, columnsToAdd));
                }
                
                foreach (var item in indexes)
                {
                    try
                    {
                        await this.RunAsyncAction(
                            (x) => this.CreateTableIndexesBuilder(this.typeMap),
                            (builder, data) => builder.BuildSql(schemaName, tableName, item.Key, item));
                    }
                    catch (Exception ex)
                    {
                        if (ex.GetType().FullName != this.ExceptionTypeName)
                        {
                            throw;
                        }
                    }
                }
                
                
                foreach (var item in keyes)
                {
                    try
                    {
                        await this.RunAsyncAction(
                            (x) => this.CreateTableKeysBuilder(this.typeMap),
                            (builder, data) => builder.BuildSql(schemaName, tableName, item.Key, item));
                    }
                    catch (Exception ex)
                    {
                        if (ex.GetType().FullName != this.ExceptionTypeName)
                        {
                            throw;
                        }
                    }
                }
            }
            else
            {
                await this.CreateTableInternal(schemaName, tableName, ifNotExists, records, indexes, keyes);
            }
        }

        private async Task<bool> TableExists(string schemaName, string tableName)
        {
            try
            {
                var t = Tuple.Create<Column, OperatorType?, Column>(Column.Custom(SpecialColumns.All), null, null);
                var result = await this.SelectAsync<object>(new SelectData
                    {
                        FieldList = new FieldList(new []{ t }),
                        SchemaName = schemaName,
                        TableName = tableName,
                        Limit = 1,
                        Offset = 0
                    });
                return true;
            }
            catch (Exception ex)
            {
                if (ex.GetType().FullName != this.ExceptionTypeName)
                {
                    throw;
                }
                
                return false;
            }
        }

        private async Task<bool> TableColumnExists(string schemaName, string tableName, string columnName, Type columnType)
        {
            try
            {
                var t = Tuple.Create<Column, OperatorType?, Column>(Column.Simple(tableName, columnName, columnType), null, null);
                var result = await this.SelectAsync<object>(new SelectData
                    {
                        FieldList = new FieldList(new []{ t }),
                        SchemaName = schemaName,
                        TableName = tableName,
                        Limit = 1,
                        Offset = 0
                    });
                return true;
            }
            catch (Exception ex)
            {
                if (ex.GetType().FullName != this.ExceptionTypeName)
                {
                    throw;
                }
                
                return false;
            }
        }

        private async Task CreateTableInternal(
            string schemaName,
            string tableName,
            bool ifNotExists,
            IReadOnlyList<Tuple<string, DataTypes, bool, RangeConstraint>> records,
            ILookup<string, string> indexes,
            ILookup<string, string> keyes)
        {
            await this.RunAsyncAction(
                (x) => this.CreateTableBuilder(this.typeMap),
                (builder, data) => builder.BuildSql(schemaName, tableName, ifNotExists, records));
            
            if (this.SupportsMultipleStatementsPerCommand)
            {
                await this.RunAsyncAction(
                    (x) => this.CreateTableIndexesBuilder(this.typeMap),
                    (builder, data) => builder.BuildSql(schemaName, tableName, indexes));
                
                await this.RunAsyncAction(
                    (x) => this.CreateTableKeysBuilder(this.typeMap),
                    (builder, data) => builder.BuildSql(schemaName, tableName, keyes));
            }
            else
            {
                foreach (var idx in indexes)
                {
                    await this.RunAsyncAction(
                        (x) => this.CreateTableIndexesBuilder(this.typeMap),
                        (builder, data) => builder.BuildSql(schemaName, tableName, idx.Key, idx));
                    
                }
                
                foreach (var key in keyes)
                {
                    await this.RunAsyncAction(
                        (x) => this.CreateTableKeysBuilder(this.typeMap),
                        (builder, data) => builder.BuildSql(schemaName, tableName, key.Key, key));
                }
            }
        }

        private string SchemaName(string schemaName)
        {
            if (string.IsNullOrWhiteSpace(schemaName))
            {
                return this.defaultSchema;
            }
            
            return schemaName;
        }
        
        private class MyKeyedCollection<T2, T3, T4> : KeyedCollection<string, Tuple<string, T2, T3, T4>>
        {
            protected override string GetKeyForItem(Tuple<string, T2, T3, T4> item)
            {
                return item.Item1;
            }
        }
    }
}

// ?? new SelectData
//                {
////                    TableName = typeof(T1).Name
//                }