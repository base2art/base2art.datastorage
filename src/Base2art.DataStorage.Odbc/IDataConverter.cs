﻿
using System;

namespace Base2art.DataStorage
{
    public interface IDataConverter
    {
        object ConvertDataFromStorage(object value, Type propertyType);
        
    }
}
