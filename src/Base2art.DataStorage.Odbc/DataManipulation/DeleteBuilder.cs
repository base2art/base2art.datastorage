﻿namespace Base2art.DataStorage.DataManipulation
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Base2art.DataStorage.DataManipulation.Data;

    public class DeleteBuilder
    {
        private readonly SharedBuilderMaps convertData;

        public DeleteBuilder(SharedBuilderMaps convertData)
        {
            this.convertData = convertData;
        }
        
        protected IEscapeCharacters EscapeCharacters 
        { 
            get { return this.convertData; }
        }
        
        public string BuildSql(
            string defaultSchema,
            string schemaName,
            string typeName,
            WhereClause wheres,
            string suffix,
            IDictionary<string, object> data)
        {
            
            System.Text.StringBuilder sb = new StringBuilder();
            sb.Append("DELETE FROM ");
            
            if (!string.IsNullOrWhiteSpace(schemaName))
            {
                sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
                sb.Append(schemaName);
                sb.Append(this.EscapeCharacters.ClosingEscapeSequence);
                sb.Append(".");
            }
            
            sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
            sb.Append(typeName);
            sb.AppendLine(this.EscapeCharacters.ClosingEscapeSequence);
            
            if (wheres != null)
            {
                sb.AppendLine(" WHERE ");
                
                this.convertData.AddWhereClause(sb, wheres, data, defaultSchema, string.Empty, "", "_Where");
            }
            
            sb.AppendLine(";");
            
            var sql = sb.ToString();
            
            return sql;
        }
    }
}
