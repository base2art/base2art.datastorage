﻿namespace Base2art.DataStorage.DataManipulation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Base2art.DataStorage.DataManipulation.Data;
    
    public class UpdateBuilder
    {
        private readonly SharedBuilderMaps convertData;

        public UpdateBuilder(SharedBuilderMaps convertData)
        {
            this.convertData = convertData;
        }
        
        protected IEscapeCharacters EscapeCharacters 
        { 
            get { return this.convertData; }
        }
        
        public string BuildSql(
            string defaultSchema,
            string schemaName,
            string typeName,
            SetsList sets,
            WhereClause wheres,
            string suffix,
            IDictionary<string, object> data)
        {
            var recordNames = sets == null ? new string[0] : sets.GetNames().ToArray();
            
            if (recordNames.Length == 0)
            {
                return null;
            }
            
            StringBuilder sb = new StringBuilder();
            sb.Append("UPDATE ");
            
            if (!string.IsNullOrWhiteSpace(schemaName))
            {
                sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
                sb.Append(schemaName);
                sb.Append(this.EscapeCharacters.ClosingEscapeSequence);
                sb.Append(".");
            }
            
            sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
            sb.Append(typeName);
            sb.AppendLine(this.EscapeCharacters.ClosingEscapeSequence);
            
            sb.AppendLine("SET");
            sb.AppendJoined(", ", recordNames, (x, j) =>
                {
                    var name = string.Format("@{0}_{1}_Set", x, 0);
                    sb.AppendFormat("{2}{0}{3} = {1}", x, name, this.EscapeCharacters.OpeningEscapeSequence, this.EscapeCharacters.ClosingEscapeSequence);
                    data.Add(name, this.convertData.ConvertDataForSql(sets.GetFieldData(x)));
                });
            
            sb.AppendLine();
            
            if (wheres != null)
            {
                sb.AppendLine(" WHERE ");
                
                this.convertData.AddWhereClause(sb, wheres, data, defaultSchema, string.Empty, string.Empty, "_Where");
            }
            
            sb.AppendLine(";");
            
            var str = sb.ToString();
            return str;
        }
    }
}
