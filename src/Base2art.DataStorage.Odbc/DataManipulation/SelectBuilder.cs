﻿namespace Base2art.DataStorage.DataManipulation
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Text;
    using Base2art.DataStorage.DataManipulation.Data;

    public class SelectBuilder
    {
        private readonly SharedBuilderMaps convertData;

        public SelectBuilder(SharedBuilderMaps convertData)
        {
            this.convertData = convertData;
        }
        
        protected IEscapeCharacters EscapeCharacters
        {
            get { return this.convertData; }
        }

        protected virtual bool SupportsCrossJoin
        {
            get{ return false; }
        }

        protected virtual string TableLevelNoLockHint
        {
            get { return ""; }
        }
        
        public string BuildSql(
            SelectData selectData,
            string defaultSchema,
            string suffix,
            IDictionary<string, object> data,
            bool end,
            bool includeMarkers)
        {
            StringBuilder sb = new StringBuilder();
            
            this.BuildSql(sb, suffix, data, selectData, defaultSchema, selectData.TableName, 1, end, includeMarkers);
            return sb.ToString();
        }

        protected virtual void BuildSql(
            StringBuilder sb,
            string suffix,
            IDictionary<string, object> data,
            SelectData selectData,
            string defaultSchema,
            string tableName,
            int currentTable,
            bool end,
            bool includeMarkers)
        {
            var schemaName = defaultSchema;
            if (!string.IsNullOrWhiteSpace(selectData.SchemaName))
            {
                schemaName = selectData.SchemaName;
            }
            
            this.Select(sb);
            this.Distinct(sb, selectData.Distinct);
            this.Fields(sb, selectData, currentTable, false, includeMarkers);
            this.From(sb, schemaName, tableName, currentTable, selectData.WithNoLock);
            this.Joins(sb, selectData, defaultSchema, currentTable, selectData.WithNoLock);
            this.Wheres(sb, selectData, defaultSchema, data, suffix, currentTable, false);
            this.Group(sb, selectData, currentTable);
            this.Order(sb, selectData, currentTable);
            this.LimitOffset(sb, selectData);
            this.End(sb, end);
        }
        
        protected virtual void Select(StringBuilder sb)
        {
            sb.Append("SELECT ");
        }

        protected virtual void Distinct(StringBuilder sb, bool distinct)
        {
            if (distinct)
            {
                sb.Append("DISTINCT ");
            }
        }

        protected virtual void From(
            StringBuilder sb,
            string schemaName,
            string tableName,
            int currentTable,
            bool withNoLock)
        {
            sb.Append("FROM ");
            
            if (!string.IsNullOrWhiteSpace(schemaName))
            {
                sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
                sb.Append(schemaName);
                sb.Append(this.EscapeCharacters.ClosingEscapeSequence);
                sb.Append(".");
            }
            
            sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
            sb.Append(tableName);
            sb.Append(this.EscapeCharacters.ClosingEscapeSequence);
            sb.Append(" ");
            sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
            sb.Append("t");
            sb.Append(currentTable);
            sb.Append(this.EscapeCharacters.ClosingEscapeSequence);
            
            if (withNoLock)
            {
                sb.Append(" ");
                sb.Append(this.TableLevelNoLockHint);
            }
            
            sb.AppendLine();
        }

        protected virtual void LimitOffset(StringBuilder sb, SelectData selectData)
        {
            var count = selectData.Limit;
            if (count.HasValue)
            {
                this.Limit(sb, count.Value);
            }
            
            var offset = selectData.Offset;
            if (offset.HasValue)
            {
                this.Offset(sb, offset.Value);
            }
        }
        
        protected virtual void Offset(StringBuilder sb, int offset)
        {
            sb.Append("OFFSET ");
            sb.Append(offset);
            sb.AppendLine();
        }

        protected virtual void Limit(StringBuilder sb, int count)
        {
            sb.Append("LIMIT ");
            sb.Append(count);
            sb.AppendLine();
        }

        protected void End(StringBuilder sb, bool end)
        {
            if (end)
            {
                sb.AppendLine(";");
            }
        }
        
        protected virtual void Fields(StringBuilder sb, SelectData selectData, int tableId, bool hasPreviousField, bool includeMarkers)
        {
            var fields = selectData.FieldList;
            
            if (fields != null && fields.GetNames().Length != 0)
            {
                sb.AppendLine();
                if (hasPreviousField)
                {
                    sb.Append(",");
                }
                
                sb.AppendJoined(", ", fields.GetNames(), (x, i) =>
                                {
                                    var tableAlias = string.Concat("  ", this.EscapeCharacters.OpeningEscapeSequence, "t", tableId, this.EscapeCharacters.ClosingEscapeSequence, ".");
                                    var item = this.convertData.ConvertColumn(tableAlias, x);
                                    if (item.Item1)
                                    {
                                        sb.Append(tableAlias);
                                    }
                                    
                                    sb.AppendLine(item.Item2);
                                    hasPreviousField = true;
                                });
                sb.AppendLine();
            }
            
            if (hasPreviousField)
            {
                if (includeMarkers)
                {
                    sb.Append(", 'MARKER' AS __FIELD__SPLIT");
                    sb.Append(tableId);
                    sb.AppendLine();
                }
            }
            else
            {
                if (includeMarkers)
                {
                    sb.Append("'MARKER' AS __FIELD__SPLIT");
                    sb.Append(tableId);
                    sb.AppendLine();
                    
                    hasPreviousField = true;
                }
            }
            
            
            if (selectData.JoinData != null && selectData.JoinData.SelectData != null)
            {
                this.Fields(sb, selectData.JoinData.SelectData, tableId + 1, hasPreviousField, includeMarkers);
            }
        }

        protected virtual void Wheres(
            StringBuilder sb,
            SelectData selectData,
            string defaultSchema,
            IDictionary<string, object> data,
            string suffix,
            int tableId_,
            bool hasPreviousField)
        {
            
            this.AppendComplexClausesRecursive(
                sb,
                " WHERE ",
                " AND ",
                tableId_,
                selectData,
                (s)=> (s.WhereClause),
                wheres => wheres != null && wheres.GetNames().Length != 0,
                (currentTableId, wheres) =>this.convertData.AddWhereClause(sb, wheres, data, defaultSchema, "t" + currentTableId, "t" + currentTableId + "_", suffix),
                hasPreviousField);
        }
        
        protected virtual void Joins(StringBuilder sb, SelectData selectData, string defaultSchema, int currentTable, bool withNoLock)
        {
            if (selectData == null)
            {
                return;
            }
            
            var data = selectData.JoinData;
            if (data == null)
            {
                return;
            }
            
            var joinString = this.Map(data.JoinType);
            if (!string.IsNullOrWhiteSpace(joinString))
            {
                sb.Append(joinString);
                sb.Append(" ");
                
                
                var schemaName = defaultSchema;
                if (!string.IsNullOrWhiteSpace(data.SelectData.SchemaName))
                {
                    schemaName = data.SelectData.SchemaName;
                }
                
                if (!string.IsNullOrWhiteSpace(schemaName))
                {
                    sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
                    sb.Append(schemaName);
                    sb.Append(this.EscapeCharacters.ClosingEscapeSequence);
                    sb.Append(".");
                }
                
                sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
                sb.Append(data.SelectData.TableName);
                sb.Append(this.EscapeCharacters.ClosingEscapeSequence);
                
                sb.Append(" ");
                sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
                sb.Append("t");
                sb.Append(currentTable + 1);
                sb.Append(this.EscapeCharacters.ClosingEscapeSequence);
                
                if (withNoLock)
                {
                    sb.Append(" ");
                    sb.Append(this.TableLevelNoLockHint);
                }
                
                sb.AppendLine();
                
                if (data.OnData.Length > 0)
                {
                    sb.Append("  ON");
                }
                
                for (int i = 0; i < data.OnData.Length; i++)
                {
                    var on = data.OnData[i];
                    if (on.Item2 != null && on.Item5 != null)
                    {
                        if (i > 0)
                        {
                            sb.Append("  AND");
                        }
                        
                        sb.Append(" ");
                        
                        sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
                        sb.Append("t");
                        sb.Append(on.Item1 + 1);
                        sb.Append(this.EscapeCharacters.ClosingEscapeSequence);
                        sb.Append(".");
                        sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
                        sb.Append(on.Item2);
                        sb.Append(this.EscapeCharacters.ClosingEscapeSequence);
                        sb.Append(" = ");
                        
                        sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
                        sb.Append("t");
                        sb.Append(on.Item4 + 1);
                        sb.Append(this.EscapeCharacters.ClosingEscapeSequence);
                        sb.Append(".");
                        sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
                        sb.Append(on.Item5);
                        sb.Append(this.EscapeCharacters.ClosingEscapeSequence);
                        sb.AppendLine();
                    }
                }
            }
            
            
            this.Joins(sb, data.SelectData, defaultSchema, currentTable + 1, withNoLock);
        }

        protected virtual void Group(
            StringBuilder sb,
            SelectData selectData,
            int tableId)
        {
            Action<int, Column, int> build = (currentTableId, item, idx) =>
            {
                if (item.SpecialColumn.HasValue)
                {
                    throw new InvalidOperationException("Cannot group on Special Columns");
                }
                else
                {
                    
                    sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
                    sb.Append("t");
                    sb.Append(currentTableId);
                    sb.Append(this.EscapeCharacters.ClosingEscapeSequence);
                    sb.Append(".");
                    
                    sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
                    sb.Append(item.ColumnName);
                    sb.Append(this.EscapeCharacters.ClosingEscapeSequence);
                }
            };
            
            this.AppendClausesRecursive(sb, "GROUP BY ", ",", tableId, selectData, x => x.Grouping, build);
        }

        protected virtual void Order(StringBuilder sb, SelectData selectData, int tableId)
        {
            Action<int, OrderingData, int> build = (currentTableId, item, idx) =>
            {
                if (item.Column.SpecialColumn.HasValue)
                {
                    sb.Append(this.convertData.NewUUIDSyntax());
                }
                else
                {
                    sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
                    sb.Append("t");
                    sb.Append(currentTableId);
                    sb.Append(this.EscapeCharacters.ClosingEscapeSequence);
                    sb.Append(".");
                    
                    sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
                    sb.Append(item.Column.ColumnName);
                    sb.Append(this.EscapeCharacters.ClosingEscapeSequence);
                    
                    sb.Append(" ");
                    sb.Append(item.Direction == ListSortDirection.Ascending ? "ASC" : "DESC");
                }
            };
            
            this.AppendClausesRecursive(sb, "ORDER BY ", ",", tableId, selectData, x => x.Ordering, build);
        }

        private void AppendComplexClausesRecursive<T>(
            StringBuilder sb,
            string keyword,
            string join,
            int tableId,
            SelectData selectData,
            Func<SelectData, T> lookup,
            Func<T, bool> check,
            Action<int, T> build,
            bool hasPreviousField)
        {
            
            var groups = lookup(selectData);
            
            if (check(groups))
            {
                sb.AppendLine();
                
                if (!hasPreviousField)
                {
                    sb.Append(keyword);
                }
                else
                {
                    sb.Append(join);
                }
                
                build(tableId, groups);
                
                sb.AppendLine();
                hasPreviousField = true;
            }
            
            if (selectData.JoinData != null && selectData.JoinData.SelectData != null)
            {
                this.AppendComplexClausesRecursive(sb, keyword, join, tableId + 1, selectData.JoinData.SelectData, lookup, check, build, hasPreviousField);
            }
        }

        private void AppendClausesRecursive<T>(
            StringBuilder sb,
            string keyword,
            string join,
            int tableId,
            SelectData selectData,
            Func<SelectData, T[]> lookup,
            Action<int, T, int> build)
        {
            this.AppendComplexClausesRecursive<T[]>(
                sb,
                keyword,
                join,
                tableId,
                selectData,
                lookup,
                groups => groups != null && groups.Length != 0,
                (currentTableId, groups) => sb.AppendJoined<T>(join, groups, (item, idx) => build(currentTableId, item, idx)),
                false);
        }
        
        private string Map(JoinType joinType)
        {
            Dictionary<JoinType, Func<JoinType, string>> map = new Dictionary<JoinType, Func<JoinType, string>>();
            map[JoinType.Cross] = (x) =>
            {
                if (!this.SupportsCrossJoin)
                {
                    throw new NotSupportedException("Not Supported: " + x);
                }
                
                return "CROSS JOIN";
            };
            map[JoinType.Full] = (x) =>
            {
                throw new NotSupportedException("Not Supported: " + x);
            };
            map[JoinType.Inner] = (x) => "INNER JOIN";
            map[JoinType.Left] = (x) => "LEFT OUTER JOIN";
            map[JoinType.Right] = (x) => "RIGHT OUTER JOIN";
            map[JoinType.Aggregate] = (x) => null;
            map[JoinType.AggregateGrouped] = (x) => null;
            
            
            
            return map[joinType](joinType);
        }
    }
}
