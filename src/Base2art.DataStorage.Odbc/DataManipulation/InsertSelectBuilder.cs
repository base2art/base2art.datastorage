﻿namespace Base2art.DataStorage.DataManipulation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Data;
    
    public class InsertSelectBuilder
    {
        private readonly SharedBuilderMaps convertData;

        public InsertSelectBuilder(SharedBuilderMaps convertData)
        {
            this.convertData = convertData;
        }
        
        protected IEscapeCharacters EscapeCharacters
        {
            get { return this.convertData; }
        }
        
        public string BuildSql(
            string defaultSchema,
            string schemaName,
            string typeName,
            FieldList list,
            SelectData records,
            string suffix,
            IDictionary<string, object> data)
        {
            list = list ?? new FieldList(new Tuple<Column, OperatorType?, Column>[0]);
            var names = list.GetNames();
            
            if (names.Length == 0)
            {
                return null;
            }
            
            StringBuilder sb = new StringBuilder();
            sb.Append("INSERT INTO ");
            
            if (!string.IsNullOrWhiteSpace(schemaName))
            {
                sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
                sb.Append(schemaName);
                sb.Append(this.EscapeCharacters.ClosingEscapeSequence);
                sb.Append(".");
            }
            
            sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
            sb.Append(typeName);
            sb.Append(this.EscapeCharacters.ClosingEscapeSequence);
            sb.AppendLine(" (");
            sb.AppendJoined(", ", names, (x, i) => {
                                sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
                                sb.Append(x.Item1.ColumnName);
                                sb.Append(this.EscapeCharacters.ClosingEscapeSequence);
                            });
            //                            (x, i) => sb.Append(x.Item1.ColumnName));
            sb.AppendLine(" )");
            
            
            var builder = this.convertData.CreateSelectBuilder();
            var content = builder.BuildSql(
                records,
                defaultSchema,
                suffix + "_" + 0,
                data,
                false,
                false);
            sb.AppendLine(content);
            
            sb.AppendLine(";");
            var sql = sb.ToString();
            return sql;
        }
        
        private string[] GetNames<Ti>(IEnumerable<Ti> records, Func<Ti, IEnumerable<string>> transform)
        {
            var hashSet = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            foreach (var record in records)
            {
                hashSet.UnionWith(transform(record));
            }
            
            return hashSet.ToArray();
        }
    }
}
