﻿
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Base2art.DataStorage
{
    public interface IExecutionEngine
    {
        Task<IEnumerable<T>> QueryReaderAsync<T>(string sql, Dictionary<string, object> data, IDataConverter dataConverter, TimeSpan? timeout);

        IEnumerable<T> QueryReader<T>(string sql, Dictionary<string, object> data, IDataConverter dataConverter, TimeSpan? timeout);
        
        Task<IEnumerable<Tuple<T1, T2>>> QueryReaderAsync<T1, T2>(string sql, Dictionary<string, object> data, IDataConverter dataConverter, TimeSpan? timeout);

        IEnumerable<Tuple<T1, T2>> QueryReader<T1, T2>(string sql, Dictionary<string, object> data, IDataConverter dataConverter, TimeSpan? timeout);
        
        Task<IEnumerable<Tuple<T1, T2, T3>>> QueryReaderAsync<T1, T2, T3>(string sql, Dictionary<string, object> data, IDataConverter dataConverter, TimeSpan? timeout);

        IEnumerable<Tuple<T1, T2, T3>> QueryReader<T1, T2, T3>(string sql, Dictionary<string, object> data, IDataConverter dataConverter, TimeSpan? timeout);
        
        Task<IEnumerable<Tuple<T1, T2, T3, T4>>> QueryReaderAsync<T1, T2, T3, T4>(string sql, Dictionary<string, object> data, IDataConverter dataConverter, TimeSpan? timeout);

        IEnumerable<Tuple<T1, T2, T3, T4>> QueryReader<T1, T2, T3, T4>(string sql, Dictionary<string, object> data, IDataConverter dataConverter, TimeSpan? timeout);
        
        Task<IEnumerable<Tuple<T1, T2, T3, T4, T5>>> QueryReaderAsync<T1, T2, T3, T4, T5>(string sql, Dictionary<string, object> data, IDataConverter dataConverter, TimeSpan? timeout);

        IEnumerable<Tuple<T1, T2, T3, T4, T5>> QueryReader<T1, T2, T3, T4, T5>(string sql, Dictionary<string, object> data, IDataConverter dataConverter, TimeSpan? timeout);
        
        Task<IEnumerable<Tuple<T1, T2, T3, T4, T5, T6>>> QueryReaderAsync<T1, T2, T3, T4, T5, T6>(string sql, Dictionary<string, object> data, IDataConverter dataConverter, TimeSpan? timeout);

        IEnumerable<Tuple<T1, T2, T3, T4, T5, T6>> QueryReader<T1, T2, T3, T4, T5, T6>(string sql, Dictionary<string, object> data, IDataConverter dataConverter, TimeSpan? timeout);
        
        Task<IEnumerable<Tuple<T1, T2, T3, T4, T5, T6, T7>>> QueryReaderAsync<T1, T2, T3, T4, T5, T6, T7>(string sql, Dictionary<string, object> data, IDataConverter dataConverter, TimeSpan? timeout);

        IEnumerable<Tuple<T1, T2, T3, T4, T5, T6, T7>> QueryReader<T1, T2, T3, T4, T5, T6, T7>(string sql, Dictionary<string, object> data, IDataConverter dataConverter, TimeSpan? timeout);
        
        Task Execute(string sql, Dictionary<string, object> data, TimeSpan? timeout);
    }
}
