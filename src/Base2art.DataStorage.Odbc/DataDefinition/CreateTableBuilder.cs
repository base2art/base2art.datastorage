﻿namespace Base2art.DataStorage.DataDefinition
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    
    public class CreateTableBuilder
    {
        private readonly IStorageTypeMap convertData;

        private readonly IEscapeCharacters escapeCharacters;
        
        public CreateTableBuilder(IStorageTypeMap convertData, IEscapeCharacters escapeCharacters)
        {
            this.escapeCharacters = escapeCharacters;
            this.convertData = convertData;
        }
        
        public IStorageTypeMap ConvertData
        {
            get { return this.convertData; }
        }

        protected IEscapeCharacters EscapeCharacters
        {
            get { return this.escapeCharacters; }
        }
        
        public virtual string BuildSql(
            string schemaName,
            string typeName,
            bool ifNotExists,
            IReadOnlyList<Tuple<string, DataTypes, bool, RangeConstraint>> columns)
        {
            StringBuilder sb = new StringBuilder();
            
            columns = columns ?? new Tuple<string, DataTypes, bool, RangeConstraint>[0];
            
            sb.AppendLine("CREATE TABLE ");
            
            if (ifNotExists)
            {
                sb.Append("IF NOT EXISTS ");
            }
            
            if (!string.IsNullOrWhiteSpace(schemaName))
            {
                sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
                sb.Append(schemaName);
                sb.Append(this.EscapeCharacters.ClosingEscapeSequence);
                sb.Append(".");
            }
            
            sb.Append(escapeCharacters.OpeningEscapeSequence);
            sb.Append(typeName);
            sb.Append(escapeCharacters.ClosingEscapeSequence);
            sb.AppendLine("(");
            sb.AppendJoined(", ", columns.ToArray(), (item, i) => this.BuildField(sb, item));
            sb.AppendLine(");");
            
            return sb.ToString();
        }

        protected virtual void BuildField(StringBuilder sb, Tuple<string, DataTypes, bool, RangeConstraint> item)
        {
            sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
            sb.Append(item.Item1);
            sb.Append(this.EscapeCharacters.ClosingEscapeSequence);
            sb.Append(" ");
            sb.Append(this.ConvertData.ColumnType(item.Item2, item.Item3, item.Item4));
        }
    }
}
