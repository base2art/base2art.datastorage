﻿namespace Base2art.DataStorage.DataDefinition
{
    using System.Text;

    public class DropTableBuilder
    {
        private readonly IEscapeCharacters escapeCharacters;
        
        public DropTableBuilder(IEscapeCharacters escapeCharacters)
        {
            this.escapeCharacters = escapeCharacters;
        }

        protected IEscapeCharacters EscapeCharacters
        {
            get { return this.escapeCharacters; }
        }
        
        public virtual string BuildSql(
            string schemaName,
            string typeName,
            bool ifExists)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("DROP TABLE ");
            if (ifExists)
            {
                sb.Append("IF EXISTS ");
            }
            
            if (!string.IsNullOrWhiteSpace(schemaName))
            {
                sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
                sb.Append(schemaName);
                sb.Append(this.EscapeCharacters.ClosingEscapeSequence);
                sb.Append(".");
            }
            
            sb.Append(escapeCharacters.OpeningEscapeSequence);
            sb.Append(typeName);
            sb.Append(escapeCharacters.ClosingEscapeSequence);
            sb.AppendLine(";");
            return sb.ToString();
        }
    }
}


