﻿namespace Base2art.DataStorage.DataDefinition
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class CreateTableIndexesBuilder
    {
        private readonly IStorageTypeMap convertData;

        private readonly IEscapeCharacters escapeCharacters;

        public CreateTableIndexesBuilder(IStorageTypeMap convertData, IEscapeCharacters escapeCharacters)
        {
            this.escapeCharacters = escapeCharacters;
            this.convertData = convertData;
        }

        public IStorageTypeMap ConvertData
        {
            get
            {
                return this.convertData;
            }
        }

        protected IEscapeCharacters EscapeCharacters
        {
            get
            {
                return this.escapeCharacters;
            }
        }
        

        public virtual string BuildSql(
            string schemaName,
            string typeName,
            ILookup<string, string> keys)
        {
            StringBuilder sb = new StringBuilder();
            
            foreach (var key in keys)
            {
                AppendItem(sb, schemaName, typeName, key.Key, key);
            }
            
            return sb.ToString();
        }
        

        public virtual string BuildSql(
            string schemaName,
            string typeName, 
            string key, 
            IGrouping<string, string> key2)
        {
            StringBuilder sb = new StringBuilder();
            
            AppendItem(sb, schemaName, typeName, key, key2);
            
            return sb.ToString();
        }

        
        private void AppendItem(StringBuilder sb, string schemaName, string typeName, string key, IGrouping<string, string> key2)
        {
            // CREATE INDEX Persons
            // ADD CONSTRAINT uc_PersonID UNIQUE (P_Id,LastName)
            sb.Append("CREATE INDEX ");
            sb.Append(escapeCharacters.OpeningEscapeSequence);
            sb.Append("idx_");
            sb.Append(key);
            sb.AppendLine(escapeCharacters.ClosingEscapeSequence);
            
            sb.Append("ON ");
            
            if (!string.IsNullOrWhiteSpace(schemaName))
            {
                sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
                sb.Append(schemaName);
                sb.Append(this.EscapeCharacters.ClosingEscapeSequence);
                sb.Append(".");
            }
            
            sb.Append(escapeCharacters.OpeningEscapeSequence);
            sb.Append(typeName);
            sb.Append(escapeCharacters.ClosingEscapeSequence);
            sb.Append(" (");
            sb.AppendJoined(",", key2.Select(x => x).ToArray(), (x, i) =>
                            {
                                sb.Append(escapeCharacters.OpeningEscapeSequence);
                                sb.Append(x);
                                sb.Append(escapeCharacters.ClosingEscapeSequence);
                            });
            
            sb.Append(")");
            sb.AppendLine(";");
        }
    }
}

