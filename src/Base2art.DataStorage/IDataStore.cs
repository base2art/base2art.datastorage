﻿namespace Base2art.DataStorage
{
    using System;
    using Base2art.DataStorage.DataManipulation;

    public interface IDataStore
    {
        IQueryInsert<T> Insert<T>();
        
        IQuerySingleSelect<T> SelectSingle<T>();
        
        IQuerySelect<T> Select<T>();
        
        IQueryDelete<T> Delete<T>();
        
        IQueryUpdate<T> Update<T>();
    }
}
