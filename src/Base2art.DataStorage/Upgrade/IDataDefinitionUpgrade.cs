﻿namespace Base2art.DataStorage.Upgrade
{
    using System.Threading.Tasks;
    
    public interface IDataDefinitionUpgrade
    {
        Task Execute(IDbms dbms, DataDefinitionUpgradeProperties properties);
    }
    
    public interface INamedConnectionDataDefinitionUpgrade
    {
        Task Execute(IDbmsFactory dbmsFactory, DataDefinitionUpgradeProperties properties);
    }
}
