﻿namespace Base2art.DataStorage.Upgrade
{
    public class DataDefinitionUpgradeProperties
    {
        public bool DbmsCanDropTables { get; set; }
        public bool IsProduction { get; set; }
    }
}


