﻿
using System;

namespace Base2art.DataStorage.DataDefinition
{
    
    public class Range
    {
        public IComparable Min
        {
            get;
            set;
        }

        public IComparable Max
        {
            get;
            set;
        }

        public static Range Create(IComparable min, IComparable max)
        {
            return new Range {
                Min = min,
                Max = max,
            };
        }

        public static Range<T> Create<T>(T min, T max) where T : struct, IComparable<T>, IComparable
        {
            return new Range<T>(min, max);
        }
    }
    
    
    public class Range<T> : Range
        where T : struct, IComparable<T>, IComparable
    {
        
        private T? min;
        private T? max;
        public Range(T? min, T? max)
        {
            this.Min = min;
            this.Max = max;
        }
        
        public new T? Min
        {
            get
            {
                return min;
            }
            
            set
            {
                base.Min = value;
                min = value;
            }
        }
        
        public new T? Max
        {
            get
            {
                return max;
            }
            
            set
            {
                base.Max = value;
                max = value;
            }
        }
    }
}
