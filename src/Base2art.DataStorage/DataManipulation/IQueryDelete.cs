﻿namespace Base2art.DataStorage.DataManipulation
{
    using System;
    using System.Threading.Tasks;
    using Base2art.DataStorage.DataManipulation.Builders;

    public interface IQueryDelete<T>
    {
        IQueryDelete<T> Where(Action<IWhereClauseBuilder<T>> recordSetup);
        
        Task Execute();
    }
}




