﻿using System;
using System.Linq.Expressions;
namespace Base2art.DataStorage.DataManipulation.Builders
{
    public interface ISetListBuilder<T>
    {
        // REGION {eb57f2f0a6a28ff1c59984d3093b5c35}

        ISetListBuilder<T> Field(Expression<Func<T, string>> caller, string data);

        ISetListBuilder<T> Field(Expression<Func<T, byte[]>> caller, byte[] data);

        ISetListBuilder<T> Field(Expression<Func<T, Guid?>> caller, Guid? data);

        ISetListBuilder<T> Field(Expression<Func<T, long?>> caller, long? data);

        ISetListBuilder<T> Field(Expression<Func<T, int?>> caller, int? data);

        ISetListBuilder<T> Field(Expression<Func<T, DateTime?>> caller, DateTime? data);

        ISetListBuilder<T> Field(Expression<Func<T, TimeSpan?>> caller, TimeSpan? data);

        ISetListBuilder<T> Field(Expression<Func<T, short?>> caller, short? data);

        ISetListBuilder<T> Field(Expression<Func<T, System.Xml.XmlDocument>> caller, System.Xml.XmlDocument data);

        ISetListBuilder<T> Field(Expression<Func<T, System.Xml.Linq.XElement>> caller, System.Xml.Linq.XElement data);

        ISetListBuilder<T> Field(Expression<Func<T, decimal?>> caller, decimal? data);

        ISetListBuilder<T> Field(Expression<Func<T, float?>> caller, float? data);

        ISetListBuilder<T> Field(Expression<Func<T, double?>> caller, double? data);

        ISetListBuilder<T> Field(Expression<Func<T, bool?>> caller, bool? data);

        ISetListBuilder<T> Field(Expression<Func<T, System.Collections.Generic.Dictionary<string, object>>> caller, System.Collections.Generic.Dictionary<string, object> data);
        // END-REGION {eb57f2f0a6a28ff1c59984d3093b5c35}
    }
}
