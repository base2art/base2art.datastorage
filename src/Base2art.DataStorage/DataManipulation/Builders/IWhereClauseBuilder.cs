﻿namespace Base2art.DataStorage.DataManipulation.Builders
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    
    public interface IWhereClauseBuilder<T>
    {
        // REGION {28a1d1e0c34a280be740d0f272cdd5b1}
        
        IWhereClauseBuilder<T> FieldNotIn<TOther>(Expression<Func<T, string>> caller, IEnumerable<TOther> data);

        IWhereClauseBuilder<T> FieldNotIn<TOther>(Expression<Func<T, byte[]>> caller, IEnumerable<TOther> data);

        IWhereClauseBuilder<T> FieldNotIn<TOther>(Expression<Func<T, Guid?>> caller, IEnumerable<TOther> data);

        IWhereClauseBuilder<T> FieldNotIn<TOther>(Expression<Func<T, long?>> caller, IEnumerable<TOther> data);

        IWhereClauseBuilder<T> FieldNotIn<TOther>(Expression<Func<T, int?>> caller, IEnumerable<TOther> data);

        IWhereClauseBuilder<T> FieldNotIn<TOther>(Expression<Func<T, DateTime?>> caller, IEnumerable<TOther> data);

        IWhereClauseBuilder<T> FieldNotIn<TOther>(Expression<Func<T, TimeSpan?>> caller, IEnumerable<TOther> data);

        IWhereClauseBuilder<T> FieldNotIn<TOther>(Expression<Func<T, short?>> caller, IEnumerable<TOther> data);

        IWhereClauseBuilder<T> FieldNotIn<TOther>(Expression<Func<T, System.Xml.XmlDocument>> caller, IEnumerable<TOther> data);

        IWhereClauseBuilder<T> FieldNotIn<TOther>(Expression<Func<T, System.Xml.Linq.XElement>> caller, IEnumerable<TOther> data);

        IWhereClauseBuilder<T> FieldNotIn<TOther>(Expression<Func<T, decimal?>> caller, IEnumerable<TOther> data);

        IWhereClauseBuilder<T> FieldNotIn<TOther>(Expression<Func<T, float?>> caller, IEnumerable<TOther> data);

        IWhereClauseBuilder<T> FieldNotIn<TOther>(Expression<Func<T, double?>> caller, IEnumerable<TOther> data);

        IWhereClauseBuilder<T> FieldNotIn<TOther>(Expression<Func<T, bool?>> caller, IEnumerable<TOther> data);

        IWhereClauseBuilder<T> FieldNotIn<TOther>(Expression<Func<T, System.Collections.Generic.Dictionary<string, object>>> caller, IEnumerable<TOther> data);

        // END-REGION {28a1d1e0c34a280be740d0f272cdd5b1}
        
        // REGION {28a1d1e0c34a280be740d0f272cdd5b1}
        
        IWhereClauseBuilder<T> FieldIn<TOther>(Expression<Func<T, string>> caller, IEnumerable<TOther> data);

        IWhereClauseBuilder<T> FieldIn<TOther>(Expression<Func<T, byte[]>> caller, IEnumerable<TOther> data);

        IWhereClauseBuilder<T> FieldIn<TOther>(Expression<Func<T, Guid?>> caller, IEnumerable<TOther> data);

        IWhereClauseBuilder<T> FieldIn<TOther>(Expression<Func<T, long?>> caller, IEnumerable<TOther> data);

        IWhereClauseBuilder<T> FieldIn<TOther>(Expression<Func<T, int?>> caller, IEnumerable<TOther> data);

        IWhereClauseBuilder<T> FieldIn<TOther>(Expression<Func<T, DateTime?>> caller, IEnumerable<TOther> data);

        IWhereClauseBuilder<T> FieldIn<TOther>(Expression<Func<T, TimeSpan?>> caller, IEnumerable<TOther> data);

        IWhereClauseBuilder<T> FieldIn<TOther>(Expression<Func<T, short?>> caller, IEnumerable<TOther> data);

        IWhereClauseBuilder<T> FieldIn<TOther>(Expression<Func<T, System.Xml.XmlDocument>> caller, IEnumerable<TOther> data);

        IWhereClauseBuilder<T> FieldIn<TOther>(Expression<Func<T, System.Xml.Linq.XElement>> caller, IEnumerable<TOther> data);

        IWhereClauseBuilder<T> FieldIn<TOther>(Expression<Func<T, decimal?>> caller, IEnumerable<TOther> data);

        IWhereClauseBuilder<T> FieldIn<TOther>(Expression<Func<T, float?>> caller, IEnumerable<TOther> data);

        IWhereClauseBuilder<T> FieldIn<TOther>(Expression<Func<T, double?>> caller, IEnumerable<TOther> data);

        IWhereClauseBuilder<T> FieldIn<TOther>(Expression<Func<T, bool?>> caller, IEnumerable<TOther> data);

        IWhereClauseBuilder<T> FieldIn<TOther>(Expression<Func<T, System.Collections.Generic.Dictionary<string, object>>> caller, IEnumerable<TOther> data);

        // END-REGION {28a1d1e0c34a280be740d0f272cdd5b1}
        
        // REGION {118fc52858bbd215d75f52a60cb9f6aa}
        
        IWhereClauseBuilder<T> Field(Expression<Func<T, string>> caller, string data, Expression<Func<string, string, bool>> op);

        IWhereClauseBuilder<T> Field(Expression<Func<T, byte[]>> caller, byte[] data, Expression<Func<byte[], byte[], bool>> op);

        IWhereClauseBuilder<T> Field(Expression<Func<T, Guid?>> caller, Guid? data, Expression<Func<Guid?, Guid?, bool>> op);

        IWhereClauseBuilder<T> Field(Expression<Func<T, long?>> caller, long? data, Expression<Func<long?, long?, bool>> op);

        IWhereClauseBuilder<T> Field(Expression<Func<T, int?>> caller, int? data, Expression<Func<int?, int?, bool>> op);

        IWhereClauseBuilder<T> Field(Expression<Func<T, DateTime?>> caller, DateTime? data, Expression<Func<DateTime?, DateTime?, bool>> op);

        IWhereClauseBuilder<T> Field(Expression<Func<T, TimeSpan?>> caller, TimeSpan? data, Expression<Func<TimeSpan?, TimeSpan?, bool>> op);

        IWhereClauseBuilder<T> Field(Expression<Func<T, short?>> caller, short? data, Expression<Func<short?, short?, bool>> op);

        IWhereClauseBuilder<T> Field(Expression<Func<T, System.Xml.XmlDocument>> caller, System.Xml.XmlDocument data, Expression<Func<System.Xml.XmlDocument, System.Xml.XmlDocument, bool>> op);

        IWhereClauseBuilder<T> Field(Expression<Func<T, System.Xml.Linq.XElement>> caller, System.Xml.Linq.XElement data, Expression<Func<System.Xml.Linq.XElement, System.Xml.Linq.XElement, bool>> op);

        IWhereClauseBuilder<T> Field(Expression<Func<T, decimal?>> caller, decimal? data, Expression<Func<decimal?, decimal?, bool>> op);

        IWhereClauseBuilder<T> Field(Expression<Func<T, float?>> caller, float? data, Expression<Func<float?, float?, bool>> op);

        IWhereClauseBuilder<T> Field(Expression<Func<T, double?>> caller, double? data, Expression<Func<double?, double?, bool>> op);

        IWhereClauseBuilder<T> Field(Expression<Func<T, bool?>> caller, bool? data, Expression<Func<bool?, bool?, bool>> op);

        IWhereClauseBuilder<T> Field(Expression<Func<T, System.Collections.Generic.Dictionary<string, object>>> caller, System.Collections.Generic.Dictionary<string, object> data, Expression<Func<System.Collections.Generic.Dictionary<string, object>, System.Collections.Generic.Dictionary<string, object>, bool>> op);

        // END-REGION {118fc52858bbd215d75f52a60cb9f6aa}
        
        // REGION {550da461c3743c1ed889540d2db9ae6d}
        
        IWhereClauseBuilder<T> Field(Expression<Func<T, string>> caller, Expression<Func<string, bool>> op);

        IWhereClauseBuilder<T> Field(Expression<Func<T, byte[]>> caller, Expression<Func<byte[], bool>> op);

        IWhereClauseBuilder<T> Field(Expression<Func<T, Guid?>> caller, Expression<Func<Guid?, bool>> op);

        IWhereClauseBuilder<T> Field(Expression<Func<T, long?>> caller, Expression<Func<long?, bool>> op);

        IWhereClauseBuilder<T> Field(Expression<Func<T, int?>> caller, Expression<Func<int?, bool>> op);

        IWhereClauseBuilder<T> Field(Expression<Func<T, DateTime?>> caller, Expression<Func<DateTime?, bool>> op);

        IWhereClauseBuilder<T> Field(Expression<Func<T, TimeSpan?>> caller, Expression<Func<TimeSpan?, bool>> op);

        IWhereClauseBuilder<T> Field(Expression<Func<T, short?>> caller, Expression<Func<short?, bool>> op);

        IWhereClauseBuilder<T> Field(Expression<Func<T, System.Xml.XmlDocument>> caller, Expression<Func<System.Xml.XmlDocument, bool>> op);

        IWhereClauseBuilder<T> Field(Expression<Func<T, System.Xml.Linq.XElement>> caller, Expression<Func<System.Xml.Linq.XElement, bool>> op);

        IWhereClauseBuilder<T> Field(Expression<Func<T, decimal?>> caller, Expression<Func<decimal?, bool>> op);

        IWhereClauseBuilder<T> Field(Expression<Func<T, float?>> caller, Expression<Func<float?, bool>> op);

        IWhereClauseBuilder<T> Field(Expression<Func<T, double?>> caller, Expression<Func<double?, bool>> op);

        IWhereClauseBuilder<T> Field(Expression<Func<T, bool?>> caller, Expression<Func<bool?, bool>> op);

        IWhereClauseBuilder<T> Field(Expression<Func<T, System.Collections.Generic.Dictionary<string, object>>> caller, Expression<Func<System.Collections.Generic.Dictionary<string, object>, bool>> op);

        // END-REGION {550da461c3743c1ed889540d2db9ae6d}
    }
}
