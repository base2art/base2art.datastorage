﻿namespace Base2art.DataStorage.DataManipulation
{
    using System;
    using System.Threading.Tasks;
    using Base2art.DataStorage.DataManipulation.Builders;
    
    public interface IQueryInsertRecords<T>
    {
        IQueryInsertRecords<T> Record(Action<ISetListBuilder<T>> recordSetup);
        
        Task Execute();
    }
}


