﻿namespace Base2art.DataStorage
{
    using Base2art.DataStorage.DataDefinition;
    
    public interface IDbms
    {
        ITableCreator<T> CreateTable<T>();

        ITableCreator<T> CreateOrUpdateTable<T>();
        
        ITableDropper<T> DropTable<T>();
    }
}
