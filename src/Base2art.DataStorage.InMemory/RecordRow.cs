﻿namespace Base2art.DataStorage.InMemory
{
    using System.Collections.Generic;
    using System.Linq;
    
    public class RecordRow : IEnumerable<RecordRowSlice>
    {
        private readonly RecordRowSlice[] slices;
        
        private readonly List<object> items = new List<object>();
        
        public RecordRow(RecordRowSlice[] slices)
        {
            this.slices = slices;
        }

        public RecordRowSlice this[string tableName]
        {
            get { return this.Slices.FirstOrDefault(x=>x.TableName == tableName); }
        }
        
        public object[] Items
        {
            get { return items.ToArray(); }
        }

        public RecordRowSlice[] Slices
        {
            get { return slices; }
        }
        
        public void AddObject(object item)
        {
            this.items.Add(item);
        }
        
        public IEnumerator<RecordRowSlice> GetEnumerator()
        {
            return (this.slices ?? new RecordRowSlice[0]).ToList<RecordRowSlice>().GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
