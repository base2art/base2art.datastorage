﻿namespace Base2art.DataStorage.InMemory
{
    public class MutableStruct<T>
        where T : struct
    {
        public T Value { get; set; }
    }
}
