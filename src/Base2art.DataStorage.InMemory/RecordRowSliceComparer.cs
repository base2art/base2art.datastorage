﻿namespace Base2art.DataStorage.InMemory
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class RecordRowSliceComparer : IEqualityComparer<RecordRowSlice>
    {
        private readonly DictionaryComparer comp = new DictionaryComparer();
        
        public bool Equals(RecordRowSlice x, RecordRowSlice y)
        {
            return comp.Equals(x.Viewable, y.Viewable);
        }

        public int GetHashCode(RecordRowSlice record)
        {
            return this.comp.GetHashCode(record.Viewable);
        }
    }
}
