﻿namespace Base2art.DataStorage.InMemory
{
    using System;
    using System.Runtime.Serialization;

    public class InMemoryDatabaseException : Exception, ISerializable
    {
        public InMemoryDatabaseException()
        {
        }

        public InMemoryDatabaseException(string message) : base(message)
        {
        }

        public InMemoryDatabaseException(string message, Exception innerException) : base(message, innerException)
        {
        }

        // This constructor is needed for serialization.
        protected InMemoryDatabaseException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}