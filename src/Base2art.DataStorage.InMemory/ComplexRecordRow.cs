﻿
namespace Base2art.DataStorage.InMemory
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base2art.DataStorage.DataManipulation;
    using Base2art.DataStorage.DataManipulation.Data;
    using Base2art.Serialization;
    
    public class ComplexRecordRow
    {
        private static readonly ISerializer serializer = new NewtonsoftSerializer();
        
        protected T DictToObject<T>(Dictionary<string, object> dict)
        {
            var objString = serializer.Serialize(dict);
            if (!typeof(T).IsInterface)
            {
                return serializer.Deserialize<T>(objString);
            }
            
            return (T)new TypedDictionaryProxy<T>(dict).GetTransparentProxy();
        }
        
        protected IEnumerable<Dictionary<string, object>> Where(
            DataDefiner definer,
            IEnumerable<Dictionary<string, object>> matches,
            WhereClause whereClauses)
        {
            foreach (var clause in whereClauses.GetNames())
            {
                matches = matches.Where(record => Comper.IsMatch(definer, record, clause));
            }
            
            return matches;
        }
    }
    
    public class ComplexRecordRow<T1> : ComplexRecordRow
    {
        private readonly RecordRowSlice dict0;

        private readonly DataDefiner definer;
        
        public ComplexRecordRow(DataDefiner definer, RecordRowSlice dict0)
        {
            this.definer = definer;
            this.dict0 = dict0;
        }
        
        public Tuple<T1> Data
        {
            get { return Tuple.Create(this.DictToObject<T1>(this.dict0.Viewable)); }
        }
    }
    
    public class ComplexRecordRow<T1, T2> : ComplexRecordRow
    {
        private readonly RecordRowSlice dict0;

        private readonly RecordRowSlice dict1;

        private readonly DataDefiner definer;
        
        public ComplexRecordRow(DataDefiner definer, RecordRowSlice dict0, RecordRowSlice dict1)
        {
            this.definer = definer;
            this.dict1 = dict1;
            this.dict0 = dict0;
        }
        
        public Tuple<T1, T2> Data
        {
            get
            {
                return Tuple.Create<T1, T2>(
                    this.dict0 == null ? default(T1) : this.DictToObject<T1>(this.dict0.Viewable),
                    this.dict1 == null ? default(T2) : this.DictToObject<T2>(this.dict1.Viewable));
            }
        }
    }
    
    public class ComplexRecordRow<T1, T2, T3> : ComplexRecordRow
    {
        private readonly RecordRowSlice dict0;

        private readonly RecordRowSlice dict1;

        private readonly DataDefiner definer;

        private readonly RecordRowSlice dict2;
        
        public ComplexRecordRow(DataDefiner definer, RecordRowSlice dict0, RecordRowSlice dict1, RecordRowSlice dict2)
        {
            this.dict2 = dict2;
            this.definer = definer;
            this.dict1 = dict1;
            this.dict0 = dict0;
        }
        
        public Tuple<T1, T2, T3> Data
        {
            get
            {
                return Tuple.Create<T1, T2, T3>(
                    this.dict0 == null ? default(T1) : this.DictToObject<T1>(this.dict0.Viewable),
                    this.dict1 == null ? default(T2) : this.DictToObject<T2>(this.dict1.Viewable),
                    this.dict2 == null ? default(T3) : this.DictToObject<T3>(this.dict2.Viewable));
            }
        }
    }
    
    public class ComplexRecordRow<T1, T2, T3, T4> : ComplexRecordRow
    {
        private readonly RecordRowSlice dict0;

        private readonly RecordRowSlice dict1;

        private readonly RecordRowSlice dict2;

        private readonly RecordRowSlice dict3;

        private readonly DataDefiner definer;
        
        public ComplexRecordRow(
            DataDefiner definer,
            RecordRowSlice dict0,
            RecordRowSlice dict1,
            RecordRowSlice dict2,
            RecordRowSlice dict3)
        {
            this.definer = definer;
            this.dict0 = dict0;
            this.dict1 = dict1;
            this.dict2 = dict2;
            this.dict3 = dict3;
        }
        
        public Tuple<T1, T2, T3, T4> Data
        {
            get
            {
                return Tuple.Create<T1, T2, T3, T4>(
                    this.dict0 == null ? default(T1) : this.DictToObject<T1>(this.dict0.Viewable),
                    this.dict1 == null ? default(T2) : this.DictToObject<T2>(this.dict1.Viewable),
                    this.dict2 == null ? default(T3) : this.DictToObject<T3>(this.dict2.Viewable),
                    this.dict3 == null ? default(T4) : this.DictToObject<T4>(this.dict3.Viewable));
            }
        }
    }
    
    public class ComplexRecordRow<T1, T2, T3, T4, T5> : ComplexRecordRow
    {
        private readonly RecordRowSlice dict0;

        private readonly RecordRowSlice dict1;

        private readonly RecordRowSlice dict2;

        private readonly RecordRowSlice dict3;

        private readonly RecordRowSlice dict4;

        private readonly DataDefiner definer;
        
        public ComplexRecordRow(
            DataDefiner definer,
            RecordRowSlice dict0,
            RecordRowSlice dict1,
            RecordRowSlice dict2,
            RecordRowSlice dict3,
            RecordRowSlice dict4)
        {
            this.definer = definer;
            this.dict0 = dict0;
            this.dict1 = dict1;
            this.dict2 = dict2;
            this.dict3 = dict3;
            this.dict4 = dict4;
        }
        
        public Tuple<T1, T2, T3, T4, T5> Data
        {
            get
            {
                return Tuple.Create<T1, T2, T3, T4, T5>(
                    this.dict0 == null ? default(T1) : this.DictToObject<T1>(this.dict0.Viewable),
                    this.dict1 == null ? default(T2) : this.DictToObject<T2>(this.dict1.Viewable),
                    this.dict2 == null ? default(T3) : this.DictToObject<T3>(this.dict2.Viewable),
                    this.dict3 == null ? default(T4) : this.DictToObject<T4>(this.dict3.Viewable),
                    this.dict4 == null ? default(T5) : this.DictToObject<T5>(this.dict4.Viewable));
            }
        }
    }
    
    public class ComplexRecordRow<T1, T2, T3, T4, T5, T6> : ComplexRecordRow
    {
        private readonly RecordRowSlice dict0;

        private readonly RecordRowSlice dict1;

        private readonly RecordRowSlice dict2;

        private readonly RecordRowSlice dict3;

        private readonly RecordRowSlice dict4;

        private readonly RecordRowSlice dict5;

        private readonly DataDefiner definer;
        
        public ComplexRecordRow(
            DataDefiner definer,
            RecordRowSlice dict0,
            RecordRowSlice dict1,
            RecordRowSlice dict2,
            RecordRowSlice dict3,
            RecordRowSlice dict4,
            RecordRowSlice dict5)
        {
            this.definer = definer;
            this.dict0 = dict0;
            this.dict1 = dict1;
            this.dict2 = dict2;
            this.dict3 = dict3;
            this.dict4 = dict4;
            this.dict5 = dict5;
        }
        
        public Tuple<T1, T2, T3, T4, T5, T6> Data
        {
            get
            {
                return Tuple.Create<T1, T2, T3, T4, T5, T6>(
                    this.dict0 == null ? default(T1) : this.DictToObject<T1>(this.dict0.Viewable),
                    this.dict1 == null ? default(T2) : this.DictToObject<T2>(this.dict1.Viewable),
                    this.dict2 == null ? default(T3) : this.DictToObject<T3>(this.dict2.Viewable),
                    this.dict3 == null ? default(T4) : this.DictToObject<T4>(this.dict3.Viewable),
                    this.dict4 == null ? default(T5) : this.DictToObject<T5>(this.dict4.Viewable),
                    this.dict5 == null ? default(T6) : this.DictToObject<T6>(this.dict5.Viewable));
            }
        }
    }
    
    public class ComplexRecordRow<T1, T2, T3, T4, T5, T6, T7> : ComplexRecordRow
    {
        private readonly RecordRowSlice dict0;

        private readonly RecordRowSlice dict1;

        private readonly RecordRowSlice dict2;

        private readonly RecordRowSlice dict3;

        private readonly RecordRowSlice dict4;

        private readonly RecordRowSlice dict5;

        private readonly RecordRowSlice dict6;

        private readonly DataDefiner definer;
        
        public ComplexRecordRow(
            DataDefiner definer,
            RecordRowSlice dict0,
            RecordRowSlice dict1,
            RecordRowSlice dict2,
            RecordRowSlice dict3,
            RecordRowSlice dict4,
            RecordRowSlice dict5,
            RecordRowSlice dict6)
        {
            this.definer = definer;
            this.dict0 = dict0;
            this.dict1 = dict1;
            this.dict2 = dict2;
            this.dict3 = dict3;
            this.dict4 = dict4;
            this.dict5 = dict5;
            this.dict6 = dict6;
        }
        
        public Tuple<T1, T2, T3, T4, T5, T6, T7> Data
        {
            get
            {
                return Tuple.Create<T1, T2, T3, T4, T5, T6, T7>(
                    this.dict0 == null ? default(T1) : this.DictToObject<T1>(this.dict0.Viewable),
                    this.dict1 == null ? default(T2) : this.DictToObject<T2>(this.dict1.Viewable),
                    this.dict2 == null ? default(T3) : this.DictToObject<T3>(this.dict2.Viewable),
                    this.dict3 == null ? default(T4) : this.DictToObject<T4>(this.dict3.Viewable),
                    this.dict4 == null ? default(T5) : this.DictToObject<T5>(this.dict4.Viewable),
                    this.dict5 == null ? default(T6) : this.DictToObject<T6>(this.dict5.Viewable),
                    this.dict6 == null ? default(T7) : this.DictToObject<T7>(this.dict6.Viewable));
            }
        }
    }
}

