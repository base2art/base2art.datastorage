﻿
using System;
using System.Collections.Generic;
using System.Linq;
using Base2art.DataStorage.DataManipulation;
using Base2art.DataStorage.DataManipulation.Data;

namespace Base2art.DataStorage.InMemory
{
    public static class Comper
    {
        public static IEnumerable<Dictionary<string, object>> Where(
            DataDefiner definer,
            IEnumerable<Dictionary<string, object>> matches,
            WhereClause whereClauses)
        {
            foreach (var clause in whereClauses.GetNames())
            {
                matches = matches.Where(record => IsMatch(definer, record, clause));
            }
            
            return matches;
        }
        
        public static bool IsMatch(
            DataDefiner definer,
            Dictionary<string, object> record,
            Tuple<Column, Tuple<OperatorType, Column>[], OperatorType, Column> clause)
        {
            var op = clause.Item3;
            if (op == OperatorType.In)
            {
                IEnumerable<object> innerList = null;
                if (clause.Item4.SelectData != null)
                {
                    innerList = definer.SelectInternal(clause.Item4.SelectData, null)
                        .Select(x => x.Select(q =>
                                              {
                                                  var z = q.Viewable;
                                                  var y = z.Keys.FirstOrDefault();
                                                  return y != null && z.ContainsKey(y) ? z[y] : null;
                                              }))
                        .Where(x => x != null)
                        .Select(x => x.FirstOrDefault());
                }
                else
                {
                    innerList = clause.Item4.ColumnData == null
                        ? new List<object>()
                        : ((System.Collections.IEnumerable)clause.Item4.ColumnData).OfType<object>();
                }
                
                bool isIn = innerList.Any(x =>
                                          {
                                              if (!record.ContainsKey(clause.Item1.ColumnName))
                                              {
                                                  return false;
                                              }
                                              
                                              var extra = record[clause.Item1.ColumnName];
                                              var y = x as IComparable;
                                              
                                              if (y != null)
                                              {
                                                  return y.CompareTo(extra) == 0;
                                              }
                                              
                                              return x == extra;
                                          });
                return isIn;
            }
            
            if (op == OperatorType.NotIn)
            {
                IEnumerable<object> innerList = null;
                if (clause.Item4.SelectData != null)
                {
                    innerList = definer.SelectInternal(clause.Item4.SelectData, null)
                        .Select(x => x.Select(q =>
                                              {
                                                  var z = q.Viewable;
                                                  var y = z.Keys.FirstOrDefault();
                                                  return y != null && z.ContainsKey(y) ? z[y] : null;
                                              }))
                        .Where(x => x != null)
                        .Select(x => x.FirstOrDefault());
                }
                else
                {
                    innerList = clause.Item4.ColumnData == null
                        ? new List<object>()
                        : ((System.Collections.IEnumerable)clause.Item4.ColumnData).OfType<object>();
                }
                
                bool isNotIn = !innerList.Any(x =>
                                              {
                                                  if (!record.ContainsKey(clause.Item1.ColumnName))
                                                  {
                                                      return false;
                                                  }
                                                  
                                                  var extra = record[clause.Item1.ColumnName];
                                                  var y = x as IComparable;
                                                  
                                                  if (y != null)
                                                  {
                                                      return y.CompareTo(extra) == 0;
                                                  }
                                                  
                                                  return x == extra;
                                              });
                return isNotIn;
            }
            
            bool isIn1 = Comp(
                op,
                record.ContainsKey(clause.Item1.ColumnName) ? record[clause.Item1.ColumnName] : null,
                (IComparable)clause.Item4.ColumnData);
            return isIn1;
        }
        
        public static bool Comp(OperatorType op, object obj, IComparable columnData)
        {
            if (op == OperatorType.IdentityEquality)
            {
                if (obj == null || columnData == null)
                {
                    return columnData == obj;
                }
                
                return columnData.CompareTo(Convert.ChangeType(obj, columnData.GetType())) == 0;
            }
            
            if (op == OperatorType.IdentityInequality)
            {
                if (obj == null || columnData == null)
                {
                    return columnData != obj;
                }
                
                return columnData.CompareTo(obj) != 0;
            }
            
            if (columnData == null)
            {
                return false;
            }
            
            var comp = columnData.CompareTo(Convert.ChangeType(obj, columnData.GetType()));
            
            if (op == OperatorType.GreaterThan)
            {
                return comp < 0;
            }
            
            if (op == OperatorType.GreaterThanOrEqual)
            {
                return comp <= 0;
            }
            
            if (op == OperatorType.LessThan)
            {
                return comp > 0;
            }
            
            if (op == OperatorType.LessThanOrEqual)
            {
                return comp >= 0;
            }
            
            if (op == OperatorType.Like)
            {
                var objStr = obj as string;
                if (objStr == null)
                {
                    return false;
                }
                
                return SqlLikeStringUtilities.SqlLike((string)columnData, objStr);
            }
            
            if (op == OperatorType.NotLike)
            {
                var objStr = obj as string;
                if (objStr == null)
                {
                    return false;
                }
                
                return !SqlLikeStringUtilities.SqlLike((string)columnData, objStr);
            }
            
            throw new NotImplementedException();
        }
    }
}
