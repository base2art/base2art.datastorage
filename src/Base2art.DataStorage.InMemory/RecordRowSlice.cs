﻿namespace Base2art.DataStorage.InMemory
{
    using System;
    using System.Collections.Generic;
    
    public class RecordRowSlice
    {
        private readonly Dictionary<string, object> original;

        private readonly Dictionary<string, object> viewable;

        private readonly string tableName;
        
        public RecordRowSlice(
            string tableName,
            Dictionary<string, object> original,
            Dictionary<string, object> viewable)
        {
            this.tableName = tableName;
            this.viewable = viewable;
            this.original = original;
        }

        public string TableName
        {
            get { return tableName; }
        }
        
        public Dictionary<string, object> Original
        {
            get{ return this.original; }
        }

        public Dictionary<string, object> Viewable
        {
            get{ return this.viewable; }
        }
    }
}


