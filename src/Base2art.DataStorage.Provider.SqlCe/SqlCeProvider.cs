﻿namespace Base2art.DataStorage.Provider.SqlCe
{
    using System;
    using System.Data.SqlClient;
    using System.Data.SqlServerCe;
    using Base2art.Dapper;
    using Base2art.DataStorage.Dapper;
    using Base2art.DataStorage.SqlCe;
    
    public class SqlCeProvider : IDataStorageProvider
    {
        private readonly bool isDebug;

        private readonly bool shouldAttemptToCreateDataFile;
        
        public SqlCeProvider(bool isDebug, bool shouldAttemptToCreateDataFile)
        {
            this.shouldAttemptToCreateDataFile = shouldAttemptToCreateDataFile;
            this.isDebug = isDebug;
        }
        
        public IDataStore CreateDataStoreAccess(NamedConnectionString named)
        {
            return new DataStore(this.Backing(named));
        }
        
        public IDbms CreateDbmsAccess(NamedConnectionString named)
        {
            return new Dbms(this.Backing(named));
        }

        private SqlCeFormatter Backing(NamedConnectionString named)
        {
            var cstr = named.ConnectionString;
            if (this.shouldAttemptToCreateDataFile)
            {
                var builder = new SqlConnectionStringBuilder(cstr);
                if (!System.IO.File.Exists(builder.DataSource))
                {
                    SqlCeEngine engine = new SqlCeEngine(cstr);
                    engine.CreateDatabase();
                }
            }
            
            var factory = new OdbcConnectionFactory<SqlCeConnection>(cstr);
            return new SqlCeFormatter(new DapperExecutionEngine(factory), this.isDebug);
        }
    }
}