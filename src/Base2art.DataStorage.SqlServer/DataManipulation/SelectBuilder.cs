﻿namespace Base2art.DataStorage.SqlServer.DataManipulation
{
    using System;
    using System.Text;
    using Base2art.DataStorage.DataManipulation;
    using Base2art.DataStorage.DataManipulation.Data;

    public class SelectBuilder : Base2art.DataStorage.DataManipulation.SelectBuilder
    {
        public SelectBuilder(SharedBuilderMaps commonBuilder) : base(commonBuilder)
        {
        }
        
        protected override bool SupportsCrossJoin
        {
            get { return true; }
        }
        
        protected override string TableLevelNoLockHint
        {
            get { return "WITH (NOLOCK)"; }
        }

        protected override void LimitOffset(StringBuilder sb, SelectData selectData)
        {
            var offset = selectData.Offset;
            var count = selectData.Limit;
            Func<SelectData, bool> hasOrderBy = null;
            
            hasOrderBy = x =>
            {
                if (x.Ordering != null && x.Ordering.Length > 0)
                {
                    return true;
                }
                
                if (x.JoinData == null || x.JoinData.SelectData == null)
                {
                    return false;
                }
                
                return hasOrderBy(x.JoinData.SelectData);
            };
            
            if (count.HasValue)
            {
                if (!hasOrderBy(selectData))
                {
                    sb.AppendLine("ORDER BY NEWID()");
                }
                
                this.Offset(sb, offset.GetValueOrDefault(0));
                this.Limit(sb, count.Value);
                return;
            }
            if (offset.HasValue)
            {
                if (!hasOrderBy(selectData))
                {
                    sb.AppendLine("ORDER BY NEWID()");
                }
                this.Offset(sb, offset.Value);
                //                this.Limit(sb, count.GetValueOrDefault(0));
            }
        }

        protected override void Offset(StringBuilder sb, int offset)
        {
            sb.Append("OFFSET ");
            sb.Append(offset);
            sb.Append(" ROWS ");
            sb.AppendLine();
        }

        protected override void Limit(StringBuilder sb, int count)
        {
            sb.Append("FETCH NEXT ");
            sb.Append(count);
            sb.Append(" ROWS ONLY ");
            sb.AppendLine();
        }
    }
}


