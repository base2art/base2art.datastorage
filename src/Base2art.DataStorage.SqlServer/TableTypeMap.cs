﻿namespace Base2art.DataStorage.SqlServer
{
    using System;
    using Base2art.DataStorage.DataDefinition;

    public class TableTypeMap : ITypeMap
    {
        public  string StringType(int? min, int? max)
        {
            if (max.HasValue)
            {
                if (min.GetValueOrDefault(-1) == max.Value)
                {
                    return "NCHAR(" + (max) + ")";
                }
            }
            
            return "NVARCHAR(" + (max.HasValue ? "" + max : "MAX") + ")";
        }

        public string ObjectType(int? min, int? max)
        {
            return "NVARCHAR(" + (max.HasValue ? "" + max : "MAX") + ")";
        }

        public string BinaryType(int? min, int? max)
        {
            return "VARBINARY(" + (max.HasValue ? "" + max : "MAX") + ")";
        }

        public string XmlType()
        {
            return "XML";
        }

        public string ShortType()
        {
            return "SMALLINT";
        }

        public string TimeSpanType()
        {
            return "TIME";
        }

        public string LongType()
        {
            return "BIGINT";
        }

        public string IntType()
        {
            return "INT";
        }

        public string GuidType()
        {
            return "UNIQUEIDENTIFIER";
        }

        public string FloatType()
        {
            return "REAL";
        }

        public string DoubleType()
        {
            return "FLOAT";
        }

        public string DecimalType()
        {
            return "DECIMAL";
        }

        public string DateTimeOffsetType()
        {
            return "DATETIMEOFFSET";
        }

        public string DateTimeType()
        {
            return "DATETIME";
        }

        public string BooleanType()
        {
            return "BIT";
        }
    }
}
