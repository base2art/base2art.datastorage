﻿namespace Base2art.Dapper
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using global::Dapper;
    
    public static class DapperAsync
    {
        public static Task<int> ExecuteStoredProcAsync(this IConnectionFactory connectionFactory, string sprocName, object param, TimeSpan? commandTimeout)
        {
            return connectionFactory.ExecuteInternal(sprocName, param, CommandType.StoredProcedure, commandTimeout);
        }
        
        public static Task<IEnumerable<T>> QueryStoredProcAsync<T>(this IConnectionFactory connectionFactory, string sprocName, object param, TimeSpan? commandTimeout)
        {
            return connectionFactory.QueryInternal<T>(sprocName, param, CommandType.StoredProcedure, commandTimeout);
        }
        
        public static Task<T> SingleOfStoredProcAsync<T>(this IConnectionFactory connectionFactory, string sprocName, object param, TimeSpan? commandTimeout)
        {
            return connectionFactory.SingleOfInternal<T>(sprocName, param, CommandType.StoredProcedure, commandTimeout);
        }
        
        public static Task<int> ExecuteAsync(this IConnectionFactory connectionFactory, string sql, object param, TimeSpan? commandTimeout)
        {
            return connectionFactory.ExecuteInternal(sql, param, CommandType.Text, commandTimeout);
        }
        
        public static Task<IEnumerable<T>> QueryAsync<T>(this IConnectionFactory connectionFactory, string sql, object param, TimeSpan? commandTimeout)
        {
            return connectionFactory.QueryInternal<T>(sql, param, CommandType.Text, commandTimeout);
        }
        
        public static Task<T> SingleOfAsync<T>(this IConnectionFactory connectionFactory, string sql, object param, TimeSpan? commandTimeout)
        {
            return connectionFactory.SingleOfInternal<T>(sql, param, CommandType.Text, commandTimeout);
        }
        
        public static Task<IEnumerable<T>> QueryReaderAsync<T>(this IConnectionFactory connectionFactory, string sql, object param, Func<IDataRecord, T> buildObject, TimeSpan? commandTimeout)
        {
            return connectionFactory.QueryInternal<T>(sql, param, buildObject, CommandType.Text, commandTimeout);
        }
        
        public static Task<T> SingleOfReaderAsync<T>(this IConnectionFactory connectionFactory, string sql, object param, Func<IDataRecord, T> buildObject, TimeSpan? commandTimeout)
        {
            return connectionFactory.SingleOfInternal<T>(sql, param, buildObject, CommandType.Text, commandTimeout);
        }

        private static async Task<IEnumerable<T>> QueryInternal<T>(this IConnectionFactory connectionFactory, string content, object parm, CommandType commandType, TimeSpan? commandTimeout)
        {
            using (var conn = await connectionFactory.ConnectionAsync())
            {
                var rez = await conn.QueryAsync<T>(content, param: parm, commandType: commandType, commandTimeout: commandTimeout.AsTimeout());
                
                return rez;
            }
        }

        private static async Task<T> SingleOfInternal<T>(this IConnectionFactory connectionFactory, string content, object parm, CommandType commandType, TimeSpan? commandTimeout)
        {
            using (var conn = await connectionFactory.ConnectionAsync())
            {
                var rez = await conn.QueryAsync<T>(content, param: parm, commandType: commandType, commandTimeout: commandTimeout.AsTimeout());
                
                return rez.FirstOrDefault();
            }
        }

        private static async Task<int> ExecuteInternal(this IConnectionFactory connectionFactory, string content, object parm, CommandType commandType, TimeSpan? commandTimeout)
        {
            using (var conn = await connectionFactory.ConnectionAsync())
            {
                return await conn.ExecuteAsync(content, param: parm, commandType: commandType, commandTimeout: commandTimeout.AsTimeout());
            }
        }

        private static async Task<IEnumerable<T>> QueryInternal<T>(
            this IConnectionFactory connectionFactory,
            string content,
            object parm,
            Func<IDataRecord, T> buildObject,
            CommandType commandType,
            TimeSpan? commandTimeout)
        {
            using (var conn = await connectionFactory.ConnectionAsync())
            {
                var reader = await conn.ExecuteReaderAsync(content, param: parm, commandType: commandType, commandTimeout: commandTimeout.AsTimeout());
                
                List<T> items = new List<T>();
                while (reader.Read())
                {
                    items.Add(buildObject(reader));
                }
                
                return items;
            }
        }

        private static async Task<T> SingleOfInternal<T>(
            this IConnectionFactory connectionFactory,
            string content,
            object parm,
            Func<IDataRecord, T> buildObject,
            CommandType commandType,
            TimeSpan? commandTimeout)
        {
            using (var conn = await connectionFactory.ConnectionAsync())
            {
                var reader = await conn.ExecuteReaderAsync(content, param: parm, commandType: commandType, commandTimeout: commandTimeout.AsTimeout());
                
                if (reader.Read())
                {
                    return buildObject(reader);
                }
                
                return default(T);
            }
        }
    }
}
