﻿namespace Base2art.Dapper
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using global::Dapper;

    public static class DapperSync
    {
        public static int ExecuteStoredProc(this IConnectionFactory connectionFactory, string sprocName, object param, TimeSpan? commandTimeout)
        {
            return connectionFactory.ExecuteInternal(sprocName, param, CommandType.StoredProcedure, commandTimeout);
        }

        public static IEnumerable<T> QueryStoredProc<T>(this IConnectionFactory connectionFactory, string sprocName, object param, TimeSpan? commandTimeout)
        {
            return connectionFactory.QueryInternal<T>(sprocName, param, CommandType.StoredProcedure, commandTimeout);
        }

        public static T SingleOfStoredProc<T>(this IConnectionFactory connectionFactory, string sprocName, object param, TimeSpan? commandTimeout)
        {
            return connectionFactory.SingleOfInternal<T>(sprocName, param, CommandType.StoredProcedure, commandTimeout);
        }

        public static int Execute(this IConnectionFactory connectionFactory, string sql, object param, TimeSpan? commandTimeout)
        {
            return connectionFactory.ExecuteInternal(sql, param, CommandType.Text, commandTimeout);
        }

        public static IEnumerable<T> Query<T>(this IConnectionFactory connectionFactory, string sql, object param, TimeSpan? commandTimeout)
        {
            return connectionFactory.QueryInternal<T>(sql, param, CommandType.Text, commandTimeout);
        }

        public static T SingleOf<T>(this IConnectionFactory connectionFactory, string sql, object param, TimeSpan? commandTimeout)
        {
            return connectionFactory.SingleOfInternal<T>(sql, param, CommandType.Text, commandTimeout);
        }

        public static IEnumerable<T> QueryReader<T>(this IConnectionFactory connectionFactory, string sql, object param, Func<IDataRecord, T> buildObject, TimeSpan? commandTimeout)
        {
            return connectionFactory.QueryInternal<T>(sql, param, buildObject, CommandType.Text, commandTimeout);
        }

        public static T SingleOfReader<T>(this IConnectionFactory connectionFactory, string sql, object param, Func<IDataRecord, T> buildObject, TimeSpan? commandTimeout)
        {
            return connectionFactory.SingleOfInternal<T>(sql, param, buildObject, CommandType.Text, commandTimeout);
        }

        private static IEnumerable<T> QueryInternal<T>(this IConnectionFactory connectionFactory, string content, object parm, CommandType commandType, TimeSpan? commandTimeout)
        {
            using (var conn = connectionFactory.Connection())
            {
                var rez = conn.Query<T>(content, param: parm, commandType: commandType, commandTimeout: commandTimeout.AsTimeout());
                return rez;
            }
        }

        private static T SingleOfInternal<T>(this IConnectionFactory connectionFactory, string content, object parm, CommandType commandType, TimeSpan? commandTimeout)
        {
            using (var conn = connectionFactory.Connection())
            {
                var rez = conn.Query<T>(content, param: parm, commandType: commandType, commandTimeout: commandTimeout.AsTimeout());
                return rez.FirstOrDefault();
            }
        }

        private static int ExecuteInternal(this IConnectionFactory connectionFactory, string content, object parm, CommandType commandType, TimeSpan? commandTimeout)
        {
            using (var conn = connectionFactory.Connection())
            {
                return conn.Execute(content, param: parm, commandType: commandType, commandTimeout: commandTimeout.AsTimeout());
            }
        }

        private static IEnumerable<T> QueryInternal<T>(this IConnectionFactory connectionFactory, string content, object parm, Func<IDataRecord, T> buildObject, CommandType commandType, TimeSpan? commandTimeout)
        {
            using (var conn = connectionFactory.Connection())
            {
                var reader = conn.ExecuteReader(content, param: parm, commandType: commandType, commandTimeout: commandTimeout.AsTimeout());
                List<T> items = new List<T>();
                while (reader.Read())
                {
                    items.Add(buildObject(reader));
                }
                
                return items;
            }
        }

        private static T SingleOfInternal<T>(this IConnectionFactory connectionFactory, string content, object parm, Func<IDataRecord, T> buildObject, CommandType commandType, TimeSpan? commandTimeout)
        {
            using (var conn = connectionFactory.Connection())
            {
                var reader = conn.ExecuteReader(content, param: parm, commandType: commandType, commandTimeout: commandTimeout.AsTimeout());
                if (reader.Read())
                {
                    return buildObject(reader);
                }
                
                return default(T);
            }
        }
        
        
        internal static int? AsTimeout(this TimeSpan? commandTimeout)
        {
            if (commandTimeout.HasValue) 
            {
                return (int)commandTimeout.Value.TotalSeconds;
            }
            
            return null;
        }
    }
}


