﻿namespace Base2art.Dapper
{
    using System;
    using System.Data.Common;

    public class OdbcConnectionStringFactory : ConnectionFactory
    {
        public OdbcConnectionStringFactory(string connectionString, string odbcProviderName) : base(connectionString, x => Create(x, odbcProviderName))
        {
        }

        private static DbConnection Create(string connectionString, string odbcProviderName)
        {
            var conn = DbProviderFactories.GetFactory(odbcProviderName).CreateConnection();
            conn.ConnectionString = connectionString;
            return conn;
        }
    }
}


