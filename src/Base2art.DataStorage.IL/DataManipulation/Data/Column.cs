﻿
namespace Base2art.DataStorage.DataManipulation.Data
{
    using System;
    using System.Collections;
    
    public class Column
    {
        private object data;

        private readonly string tableName;
        
        private readonly string columnName;

        private SpecialColumns? columnType;

        private SelectData selectData;

//        private readonly IEnumerable enumerable;
        
        private readonly Type dataType;
        
        private Column(IEnumerable data)
        {
            this.data = data;
        }
        
        private Column(SelectData data)
        {
            this.selectData = data;
        }
        
        private Column(SpecialColumns columnType)
        {
            this.columnType = columnType;
        }

        public Column(SpecialColumns columnType, string columnName)
        {
            this.columnType = columnType;
            this.columnName = columnName;
        }
        
        private Column(string tableName, string data, Type dataType)
        {
            this.tableName = tableName;
            this.columnName = data;
            this.dataType = dataType;
        }
        
        private Column(object data, Type dataType)
        {
            this.data = data;
            this.dataType = dataType;
        }

        public SelectData SelectData
        {
            get { return this.selectData; }
        }

//        public string TableName
//        {
//            get { return tableName; }
//        }
        
        public string ColumnName
        {
            get { return this.columnName; }
        }

        public object ColumnData
        {
            get { return data; }
        }

        public SpecialColumns? SpecialColumn
        {
            get { return this.columnType; }
        }

        public Type DataType
        {
            get { return this.dataType; }
        }
        
        public static Column Simple(string data, Type dataType)
        {
            return new Column(string.Empty, data, dataType);
        }
        
        public static Column Simple(string tableName, string data, Type dataType)
        {
            return new Column(string.Empty, data, dataType);
        }

        public static Column Custom(SpecialColumns data)
        {
            return new Column(data);
        }
        
        public static Column CustomWithAlias(SpecialColumns column, string columnName)
        {
            return new Column(column, columnName);
        }

        public static Column InnerSelect(SelectData data)
        {
            return new Column(data);
        }

        public static Column Data(object data, Type dataType)
        {
            return new Column(data, dataType);
        }
        
        public static Column List(IEnumerable data)
        {
            return new Column(data);
        }
        
        public string FieldNameOrNull()
        {
            return this.columnName;
        }
        
        public override string ToString()
        {
            var printData =  string.Format("{0}{1}{2}", this.columnName, this.selectData, this.data);
            if (!string.IsNullOrWhiteSpace(printData))
            {
                return printData;
            }
            
            if (this.dataType == typeof(string))
            {
                return string.Format("'{0}'", this.data);
            }
            
            return string.Format("{0}", this.data);
        }
    }
}
