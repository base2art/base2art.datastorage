﻿namespace Base2art.DataStorage.DataManipulation.Data
{
    using System.ComponentModel;

    public class OrderingData
    {
        public Column Column { get; set; }

        public ListSortDirection Direction { get; set; }
    }
}


