﻿namespace Base2art.DataStorage.SqlCe.DataDefinition
{
    using System.Text;

    public class DropTableBuilder : Base2art.DataStorage.DataDefinition.DropTableBuilder
    {
        public DropTableBuilder(IEscapeCharacters escapeChars) : base(escapeChars)
        {
        }
        
        public override string BuildSql(string schemaName, string typeName, bool ifExists)
        {
            StringBuilder sb = new StringBuilder();
            if (ifExists)
            {
                sb.AppendLine("IF OBJECT_ID('dbo." + typeName + "', 'U') IS NOT NULL");
            }
            
            sb.Append("DROP TABLE ");
            
            sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
            sb.Append(typeName);
            sb.Append(this.EscapeCharacters.ClosingEscapeSequence);
            sb.AppendLine(";");
            return sb.ToString();
        }
    }
}


