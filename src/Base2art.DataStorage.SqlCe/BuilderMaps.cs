﻿namespace Base2art.DataStorage.SqlCe
{
    using System;
    using Base2art.DataStorage.DataManipulation;
    
    public class BuilderMaps : SharedBuilderMaps
    {
        public override string OpeningEscapeSequence
        {
            get { return "["; }
        }
        
        public override string ClosingEscapeSequence
        {
            get { return "]"; }
        }
        
        public override SelectBuilder CreateSelectBuilder()
        {
            return new SelectBuilder(this);
        }
        
        protected override TimeSpan MapFromNonNullTimeSpan(object value)
        {
            return TimeSpan.FromMilliseconds(Convert.ToInt64(value));
        }
        
        protected override object MapTimeSpan(TimeSpan value)
        {
            return value.TotalMilliseconds;
        }
        
        protected override void Append(
            System.Text.StringBuilder sb,
            bool hasValue,
            bool groupFirst,
            string part1,
            OperatorType fieldOperation,
            Type t1,
            Type t2,
            string part2)
        {
            if ((t1 == typeof(DateTime) || t1 == typeof(DateTime?)) && (t2 == typeof(TimeSpan) || t2 == typeof(TimeSpan?)))
            {
                var format = groupFirst ? " {1}(MILLISECOND, ({2}), {0} )" : " {1}(MILLISECOND, {2}, {0})";
                if (fieldOperation == OperatorType.Add)
                {
                    sb.AppendFormat(
                        format,
                        part1,
                        " DATEADD",
                        part2);
                    return;
                }
                
                if (fieldOperation == OperatorType.Subtract)
                {
                    sb.AppendFormat(
                        format,
                        part1,
                        " DATESUB",
                        part2);
                    return;
                }
            }
            
            base.Append(sb, hasValue, groupFirst, part1, fieldOperation, t1, t2, part2);
        }
        
        public override string NewUUIDSyntax()
        {
            return "NEWID()";
        }
    }
}

