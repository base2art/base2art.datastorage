﻿namespace Base2art.DataStorage.SqlCe
{
    using System;
    using Base2art.DataStorage.DataDefinition;

    public class TableTypeMap : ITypeMap
    {
        public string StringType(int? min, int? max)
        {
            if (max.HasValue)
            {
                if (min.GetValueOrDefault(-1) == max.Value)
                {
                    return "NCHAR(" + (max) + ")";
                }
                
                return "NVARCHAR(" + max.Value + ")";
            }
            
            //            return this.StringType(RangeConstraint.Create(0, 4000));
            return "NTEXT";
        }

        public string ObjectType(int? min, int? max)
        {
            return this.StringType(min, max);
        }

        public string BinaryType(int? min, int? max)
        {
            if (max.HasValue)
            {
                return "VARBINARY(" + max.Value + ")";
            }
            
            //            return this.BinaryType(RangeConstraint.Create(0, 4000));
            return "IMAGE";
        }

        public string XmlType()
        {
            return this.StringType(null, null);
        }

        public string ShortType()
        {
            return "SMALLINT";
        }

        public string TimeSpanType()
        {
            return "BIGINT";
        }

        public string LongType()
        {
            return "BIGINT";
        }

        public string IntType()
        {
            return "INT";
        }

        public string GuidType()
        {
            return "UNIQUEIDENTIFIER";
        }

        public string FloatType()
        {
            return "REAL";
        }

        public string DoubleType()
        {
            return "FLOAT";
        }

        public string DecimalType()
        {
            return "DECIMAL";
        }

        public string DateTimeOffsetType()
        {
            return "DATETIME";
        }

        public string DateTimeType()
        {
            return "DATETIME";
        }

        public string BooleanType()
        {
            return "BIT";
        }
    }
}
