﻿namespace Base2art.DataStorage
{
    using System;
    using Base2art.DataStorage.DataDefinition;

    public class Dbms : IDbms
    {
        private readonly IDataDefiner definer;

        public Dbms(IDataDefiner definer)
        {
            this.definer = definer;
        }
        
        public ITableCreator<T> CreateTable<T>()
        {
            return new TableCreator<T>(this.definer, false);
        }
        
        public ITableCreator<T> CreateOrUpdateTable<T>()
        {
            return new TableCreator<T>(this.definer, true);
        }
        
        public ITableDropper<T> DropTable<T>()
        {
            return new TableDropper<T>(this.definer);
        }
    }
}
