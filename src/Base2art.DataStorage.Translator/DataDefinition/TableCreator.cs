﻿namespace Base2art.DataStorage.DataDefinition
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using Base2art.DataStorage.DataDefinition;
    using Base2art.DataStorage.DataDefinition.Builders;
    using Base2art.DataStorage.DataDefinition.Clauses;
    
    public class TableCreator<T> : ITableCreator<T>
    {
        private readonly IDataDefiner definer;

        private TableFieldsClause<T> fields;
        
        private bool ifNotExists;

        private readonly bool allowUpdate;

        private readonly Dictionary<string, KeyOrIndexBuilder<T>> keyes = new Dictionary<string, KeyOrIndexBuilder<T>>();

        private readonly Dictionary<string, KeyOrIndexBuilder<T>> indexes = new Dictionary<string, KeyOrIndexBuilder<T>>();
        
        public TableCreator(IDataDefiner definer, bool allowUpdate)
        {
            this.allowUpdate = allowUpdate;
            this.definer = definer;
        }

        public ITableCreator<T> WithKey(string keyName, Action<IKeyOrIndexBuilder<T>> fieldsSetup)
        {
            var record = new KeyOrIndexBuilder<T>();
            fieldsSetup(record);
            this.keyes.Add(keyName, record);
            return this;
        }
        
        public ITableCreator<T> WithIndex(string indexName, Action<IKeyOrIndexBuilder<T>> fieldsSetup)
        {
            var record = new KeyOrIndexBuilder<T>();
            fieldsSetup(record);
            this.indexes.Add(indexName, record);
            return this;
        }
        
        public ITableCreator<T> Fields(Action<IFieldSetupBuilder<T>> fieldsSetup)
        {
            var record = this.fields ?? new TableFieldsClause<T>();
            fieldsSetup(record);
            this.fields = record;
            return this;
        }

        public ITableCreator<T> IfNotExists()
        {
            this.ifNotExists = true;
            return this;
        }
        
        public System.Threading.Tasks.Task Execute()
        {
            var myIndexes = this.indexes.SelectMany(x => x.Value.GetColumns().Select(y => Tuple.Create(x.Key, y)))
                .ToLookup(x => x.Item1, (x) => x.Item2);
            var myKeyes = this.keyes.SelectMany(x => x.Value.GetColumns().Select(y => Tuple.Create(x.Key, y)))
                .ToLookup(x => x.Item1, (x) => x.Item2);
            
            
            var tableType = typeof(T);
            return this.definer.CreateTable(
                tableType.Schema(),
                tableType.TableName(),
                this.allowUpdate,
                this.ifNotExists,
                (this.fields ?? this.CreateAllFields()).GetColumns(),
                myIndexes,
                myKeyes);
        }

        private TableFieldsClause<T> CreateAllFields()
        {
            var item = new TableFieldsClause<T>();
            
            var type = typeof(T);
            
            var mapRun = new Dictionary<Type, Action<Expression, ParameterExpression, PropertyInfo>>();
            // REGION {5c4591406f8fd1e33c71b4e392e8a5da}
            mapRun[typeof(string)] = (x, y, z) =>
            {
                
                var maxlen = z.GetCustomAttribute<MaxLengthAttribute>();
                var minlen = z.GetCustomAttribute<MinLengthAttribute>();
                var required = z.GetCustomAttribute<RequiredAttribute>();
                
                var requiredValue = required != null;
                if (maxlen == null && minlen == null)
                {
                    item.Field(Expression.Lambda<Func<T, string>>(x, y), requiredValue, null);
                    return;
                }
                
                int? maxlenValue = maxlen != null ? maxlen.Length : (int?)null;
                int? minlenValue = minlen != null ? minlen.Length : (int?)null;
                
                item.Field(
                    Expression.Lambda<Func<T, string>>(x, y),
                    requiredValue,
                    Range.Create<int>(minlenValue.GetValueOrDefault(0), maxlenValue.GetValueOrDefault(2048)));
                return;
            };
            
            mapRun[typeof(byte[])] = (x, y, z) => item.Field(Expression.Lambda<Func<T, byte[]>>(x, y));
            mapRun[typeof(Guid?)] = (x, y, z) => item.Field(Expression.Lambda<Func<T, Guid?>>(x, y));
            mapRun[typeof(long?)] = (x, y, z) => item.Field(Expression.Lambda<Func<T, long?>>(x, y));
            mapRun[typeof(int?)] = (x, y, z) => item.Field(Expression.Lambda<Func<T, int?>>(x, y));
            mapRun[typeof(DateTime?)] = (x, y, z) => item.Field(Expression.Lambda<Func<T, DateTime?>>(x, y));
            mapRun[typeof(DateTimeOffset?)] = (x, y, z) => item.Field(Expression.Lambda<Func<T, DateTimeOffset?>>(x, y));
            mapRun[typeof(TimeSpan?)] = (x, y, z) => item.Field(Expression.Lambda<Func<T, TimeSpan?>>(x, y));
            mapRun[typeof(short?)] = (x, y, z) => item.Field(Expression.Lambda<Func<T, short?>>(x, y));
            mapRun[typeof(System.Xml.XmlDocument)] = (x, y, z) => item.Field(Expression.Lambda<Func<T, System.Xml.XmlDocument>>(x, y));
            mapRun[typeof(System.Xml.Linq.XElement)] = (x, y, z) => item.Field(Expression.Lambda<Func<T, System.Xml.Linq.XElement>>(x, y));
            mapRun[typeof(decimal?)] = (x, y, z) => item.Field(Expression.Lambda<Func<T, decimal?>>(x, y));
            mapRun[typeof(float?)] = (x, y, z) => item.Field(Expression.Lambda<Func<T, float?>>(x, y));
            mapRun[typeof(double?)] = (x, y, z) => item.Field(Expression.Lambda<Func<T, double?>>(x, y));
            mapRun[typeof(bool?)] = (x, y, z) => item.Field(Expression.Lambda<Func<T, bool?>>(x, y));
            mapRun[typeof(System.Collections.Generic.Dictionary<string, object>)] = (x, y, z) => item.Field(Expression.Lambda<Func<T, System.Collections.Generic.Dictionary<string, object>>>(x, y));
            
            mapRun[typeof(Guid)] = (x, y, z) => item.Field(Expression.Lambda<Func<T, Guid>>(x, y));
            mapRun[typeof(long)] = (x, y, z) => item.Field(Expression.Lambda<Func<T, long>>(x, y));
            mapRun[typeof(int)] = (x, y, z) => item.Field(Expression.Lambda<Func<T, int>>(x, y));
            mapRun[typeof(DateTime)] = (x, y, z) => item.Field(Expression.Lambda<Func<T, DateTime>>(x, y));
            mapRun[typeof(DateTimeOffset)] = (x, y, z) => item.Field(Expression.Lambda<Func<T, DateTimeOffset>>(x, y));
            mapRun[typeof(TimeSpan)] = (x, y, z) => item.Field(Expression.Lambda<Func<T, TimeSpan>>(x, y));
            mapRun[typeof(short)] = (x, y, z) => item.Field(Expression.Lambda<Func<T, short>>(x, y));
            mapRun[typeof(decimal)] = (x, y, z) => item.Field(Expression.Lambda<Func<T, decimal>>(x, y));
            mapRun[typeof(float)] = (x, y, z) => item.Field(Expression.Lambda<Func<T, float>>(x, y));
            mapRun[typeof(double)] = (x, y, z) => item.Field(Expression.Lambda<Func<T, double>>(x, y));
            mapRun[typeof(bool)] = (x, y, z) => item.Field(Expression.Lambda<Func<T, bool>>(x, y));
            // END-REGION {5c4591406f8fd1e33c71b4e392e8a5da}
            
            foreach (var prop in type.GetPublicProperties())
            {
                //                var setter = prop.GetSetMethod(false);
                var getter = prop.GetGetMethod(false);
                
                //setter != null &&
                if (getter != null)
                {
                    ParameterExpression argParam = Expression.Parameter(type, "s");
                    Expression nameProperty = Expression.Property(argParam, prop);
                    
                    if (mapRun.ContainsKey(prop.PropertyType))
                    {
                        mapRun[prop.PropertyType](nameProperty, argParam, prop);
                    }
                    else
                    {
                        throw new ArgumentOutOfRangeException(prop.PropertyType.ToString());
                    }
                }
            }
            
            return item;
        }
    }
    
    
}
