﻿namespace Base2art.DataStorage.DataDefinition.Clauses
{
    using System;
    using Base2art.DataStorage.DataDefinition;
    using Base2art.DataStorage.DataDefinition.Builders;

    public partial class TableFieldsClause<T>
    {

        public IFieldSetupBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, bool>> caller)
        {
            return this.FieldOf(caller, DataTypes.Boolean);
        }
        
        public IFieldSetupBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, bool?>> caller)
        {
            return this.FieldOf(caller, DataTypes.Boolean);
        }
        
        public IFieldSetupBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, decimal>> caller)
        {
            return this.FieldOf(caller, DataTypes.Decimal);
        }
        
        public IFieldSetupBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, decimal?>> caller)
        {
            return this.FieldOf(caller, DataTypes.Decimal);
        }
        
        public IFieldSetupBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, double>> caller)
        {
            return this.FieldOf(caller, DataTypes.Double);
        }
        
        public IFieldSetupBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, double?>> caller)
        {
            return this.FieldOf(caller, DataTypes.Double);
        }
        
        public IFieldSetupBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, float>> caller)
        {
            return this.FieldOf(caller, DataTypes.Float);
        }
        
        public IFieldSetupBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, float?>> caller)
        {
            return this.FieldOf(caller, DataTypes.Float);
        }
        
        public IFieldSetupBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, int>> caller)
        {
            return this.FieldOf(caller, DataTypes.Int);
        }
        
        public IFieldSetupBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, int?>> caller)
        {
            return this.FieldOf(caller, DataTypes.Int);
        }
        
        public IFieldSetupBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, long>> caller)
        {
            return this.FieldOf(caller, DataTypes.Long);
        }
        
        public IFieldSetupBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, long?>> caller)
        {
            return this.FieldOf(caller, DataTypes.Long);
        }
        
        public IFieldSetupBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, short>> caller)
        {
            return this.FieldOf(caller, DataTypes.Short);
        }
        
        public IFieldSetupBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, short?>> caller)
        {
            return this.FieldOf(caller, DataTypes.Short);
        }
        
        public IFieldSetupBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, DateTime>> caller)
        {
            return this.FieldOf(caller, DataTypes.DateTime);
        }
        
        public IFieldSetupBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, DateTime?>> caller)
        {
            return this.FieldOf(caller, DataTypes.DateTime);
        }
        
        public IFieldSetupBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, DateTimeOffset>> caller)
        {
            return this.FieldOf(caller, DataTypes.DateTimeOffset);
        }
        
        public IFieldSetupBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, DateTimeOffset?>> caller)
        {
            return this.FieldOf(caller, DataTypes.DateTimeOffset);
        }
        
        public IFieldSetupBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, TimeSpan>> caller)
        {
            return this.FieldOf(caller, DataTypes.TimeSpan);
        }
        
        public IFieldSetupBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, TimeSpan?>> caller)
        {
            return this.FieldOf(caller, DataTypes.TimeSpan);
        }
        
        public IFieldSetupBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, Guid>> caller)
        {
            return this.FieldOf(caller, DataTypes.Guid);
        }
        
        public IFieldSetupBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, Guid?>> caller)
        {
            return this.FieldOf(caller, DataTypes.Guid);
        }
        
        public IFieldSetupBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, string>> caller)
        {
            return this.FieldOf(caller, DataTypes.String, false, null);
        }
        
        public IFieldSetupBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, string>> caller, bool forceValue, Range<int> lengthRanges)
        {
            return this.FieldOf(caller, DataTypes.String, forceValue, lengthRanges);
        }
        
        public IFieldSetupBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, byte[]>> caller)
        {
            return this.FieldOf(caller, DataTypes.Binary, false, null);
        }
        
        public IFieldSetupBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, byte[]>> caller, bool forceValue, Range<int> lengthRanges)
        {
            return this.FieldOf(caller, DataTypes.Binary, forceValue, lengthRanges);
        }
        
        public IFieldSetupBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, System.Xml.XmlDocument>> caller)
        {
            return this.FieldOf(caller, DataTypes.Xml, false, null);
        }
        
        public IFieldSetupBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, System.Xml.XmlDocument>> caller, bool forceValue)
        {
            return this.FieldOf(caller, DataTypes.Xml, forceValue, null);
        }
        
        public IFieldSetupBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, System.Xml.Linq.XElement>> caller)
        {
            return this.FieldOf(caller, DataTypes.Xml, false, null);
        }
        
        public IFieldSetupBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, System.Xml.Linq.XElement>> caller, bool forceValue)
        {
            return this.FieldOf(caller, DataTypes.Xml, forceValue, null);
        }
        
        public IFieldSetupBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, System.Collections.Generic.Dictionary<string, object>>> caller)
        {
            return this.FieldOf(caller, DataTypes.Object, false, null);
        }
        
        public IFieldSetupBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, System.Collections.Generic.Dictionary<string, object>>> caller, bool forceValue)
        {
            return this.FieldOf(caller, DataTypes.Object, forceValue, null);
        }
    }
}
