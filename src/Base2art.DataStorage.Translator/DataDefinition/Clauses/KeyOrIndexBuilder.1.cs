﻿
using System;
using Base2art.DataStorage.DataDefinition.Builders;

namespace Base2art.DataStorage.DataDefinition.Clauses
{
    public partial class KeyOrIndexBuilder<T>
    {
        public IKeyOrIndexBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, bool>> caller)
        {
            return this.FieldOf(caller, DataTypes.Boolean);
        }
        
        public IKeyOrIndexBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, bool?>> caller)
        {
            return this.FieldOf(caller, DataTypes.Boolean);
        }
        
        public IKeyOrIndexBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, decimal>> caller)
        {
            return this.FieldOf(caller, DataTypes.Decimal);
        }
        
        public IKeyOrIndexBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, decimal?>> caller)
        {
            return this.FieldOf(caller, DataTypes.Decimal);
        }
        
        public IKeyOrIndexBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, double>> caller)
        {
            return this.FieldOf(caller, DataTypes.Double);
        }
        
        public IKeyOrIndexBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, double?>> caller)
        {
            return this.FieldOf(caller, DataTypes.Double);
        }
        
        public IKeyOrIndexBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, float>> caller)
        {
            return this.FieldOf(caller, DataTypes.Float);
        }
        
        public IKeyOrIndexBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, float?>> caller)
        {
            return this.FieldOf(caller, DataTypes.Float);
        }
        
        public IKeyOrIndexBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, int>> caller)
        {
            return this.FieldOf(caller, DataTypes.Int);
        }
        
        public IKeyOrIndexBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, int?>> caller)
        {
            return this.FieldOf(caller, DataTypes.Int);
        }
        
        public IKeyOrIndexBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, long>> caller)
        {
            return this.FieldOf(caller, DataTypes.Long);
        }
        
        public IKeyOrIndexBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, long?>> caller)
        {
            return this.FieldOf(caller, DataTypes.Long);
        }
        
        public IKeyOrIndexBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, short>> caller)
        {
            return this.FieldOf(caller, DataTypes.Short);
        }
        
        public IKeyOrIndexBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, short?>> caller)
        {
            return this.FieldOf(caller, DataTypes.Short);
        }
        
        public IKeyOrIndexBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, DateTime>> caller)
        {
            return this.FieldOf(caller, DataTypes.DateTime);
        }
        
        public IKeyOrIndexBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, DateTime?>> caller)
        {
            return this.FieldOf(caller, DataTypes.DateTime);
        }
        
        public IKeyOrIndexBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, DateTimeOffset>> caller)
        {
            return this.FieldOf(caller, DataTypes.DateTimeOffset);
        }
        
        public IKeyOrIndexBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, DateTimeOffset?>> caller)
        {
            return this.FieldOf(caller, DataTypes.DateTimeOffset);
        }
        
        public IKeyOrIndexBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, TimeSpan>> caller)
        {
            return this.FieldOf(caller, DataTypes.TimeSpan);
        }
        
        public IKeyOrIndexBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, TimeSpan?>> caller)
        {
            return this.FieldOf(caller, DataTypes.TimeSpan);
        }
        
        public IKeyOrIndexBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, Guid>> caller)
        {
            return this.FieldOf(caller, DataTypes.Guid);
        }
        
        public IKeyOrIndexBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, Guid?>> caller)
        {
            return this.FieldOf(caller, DataTypes.Guid);
        }
        
        public IKeyOrIndexBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, string>> caller)
        {
            return this.FieldOfClass(caller, DataTypes.String);
        }
        
        public IKeyOrIndexBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, byte[]>> caller)
        {
            return this.FieldOfClass(caller, DataTypes.Binary);
        }
        
        public IKeyOrIndexBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, System.Xml.XmlDocument>> caller)
        {
            return this.FieldOfClass(caller, DataTypes.Xml);
        }
        
        public IKeyOrIndexBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, System.Xml.XmlDocument>> caller, bool forceValue)
        {
            return this.FieldOfClass(caller, DataTypes.Xml);
        }
        
        public IKeyOrIndexBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, System.Xml.Linq.XElement>> caller)
        {
            return this.FieldOfClass(caller, DataTypes.Xml);
        }
        
        public IKeyOrIndexBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, System.Xml.Linq.XElement>> caller, bool forceValue)
        {
            return this.FieldOfClass(caller, DataTypes.Xml);
        }
        
        public IKeyOrIndexBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, System.Collections.Generic.Dictionary<string, object>>> caller)
        {
            return this.FieldOfClass(caller, DataTypes.Object);
        }
        
        public IKeyOrIndexBuilder<T> Field(System.Linq.Expressions.Expression<Func<T, System.Collections.Generic.Dictionary<string, object>>> caller, bool forceValue)
        {
            return this.FieldOfClass(caller, DataTypes.Object);
        }
    }
}
