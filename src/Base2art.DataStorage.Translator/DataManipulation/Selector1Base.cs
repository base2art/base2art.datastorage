﻿namespace Base2art.DataStorage.DataManipulation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using Base2art.DataStorage.DataManipulation.Builders;
    using Base2art.DataStorage.DataManipulation.Clauses;
    using Base2art.DataStorage.DataManipulation.Data;
    using Base2art.DataStorage.Utils;
    
    internal class Selector1Base<T> : IDataGetter<SelectData>
    {
        private readonly IDataManipulator manipulator;

        private WhereClauseWithValue<T> wheres;

        private FieldSelectClause<T> fields;

        private bool hasFieldsDeclared;

        private OrderByBuilder<T> orderBy;
        
        private int? offset;
        
        private int? limit;

        private bool distinct;

        private GroupByBuilder<T> groupBy;
        
        private Type backing = typeof(T);

        private bool nolock;
        
        public Selector1Base(IDataManipulator manipulator)
        {
            this.manipulator = manipulator;
        }

        public IDataManipulator Manipulator
        {
            get { return this.manipulator; }
        }
        
        internal WhereClauseWithValue<T> WhereData
        {
            get { return this.wheres; }
            set { this.wheres = value; }
        }

        internal FieldSelectClause<T> FieldData
        {
            get { return this.fields; }
            set { this.fields = value; }
        }

        internal OrderByBuilder<T> OrderData
        {
            get { return this.orderBy; }
            set { this.orderBy = value; }
        }

        internal GroupByBuilder<T> Group1Data
        {
            get { return this.groupBy; }
            set { this.groupBy = value; }
        }

        public bool HasFieldsDeclared
        {
            get{ return this.hasFieldsDeclared; }
        }
        
        public void DeclareFieldsInUse()
        {
            this.hasFieldsDeclared = true;
        }
        
        public Selector1Base<T> Limit(int limit)
        {
            this.limit = limit;
            return this;
        }
        
        public Selector1Base<T> Offset(int offset)
        {
            this.offset = offset;
            return this;
        }

        public Selector1Base<T> Distinct()
        {
            this.distinct = true;
            return this;
        }

        public Selector1Base<T> WithNoLock()
        {
            this.nolock = true;
            return this;
        }
        
        public Selector1Base<T> Where(Action<IWhereClauseBuilder<T>> recordSetup)
        {
            var record = this.wheres ?? new WhereClauseWithValue<T>();
            recordSetup(record);
            this.wheres = record;
            return this;
        }

        public Selector1Base<T> Fields(Action<IFieldListBuilder<T>> fieldSetup)
        {
            var record = this.fields ?? new FieldSelectClause<T>();
            fieldSetup(record);
            this.fields = record;
            this.hasFieldsDeclared = true;
            return this;
        }
        
        public Selector1Base<T> OrderBy(Action<IOrderByBuilder<T>> recordSetup)
        {
            var record = this.orderBy ?? new OrderByBuilder<T>();
            recordSetup(record);
            this.orderBy = record;
            return this;
        }

        public Selector2Base<T, TAggregate> GroupBy<TAggregate>(Action<IGroupByBuilder<T>> recordSetup)
        {
            var record = this.groupBy ?? new GroupByBuilder<T>();
            recordSetup(record);
            this.groupBy = record;
            return new Selector2Base<T, TAggregate>(
                this,
                JoinType.AggregateGrouped,
                new Tuple<int, Column, int, Column>[0]);
        }

        public Selector2Base<T, TAggregate> WithCalculated<TAggregate>()
        {
            return new Selector2Base<T, TAggregate>(
                this,
                JoinType.Aggregate,
                new Tuple<int, Column, int, Column>[0]);
        }

        public Selector2Base<T, TJoin> Join<TJoin>(Expression<Func<T, TJoin, bool>> joinSetup)
        {
            return new Selector2Base<T, TJoin>(
                this,
                JoinType.Inner,
                this.GetJoins(joinSetup, joinSetup.Body as BinaryExpression));
        }
        
        public Selector2Base<T, TJoin> LeftJoin<TJoin>(Expression<Func<T, TJoin, bool>> joinSetup)
        {
            return new Selector2Base<T, TJoin>(
                this,
                JoinType.Left,
                this.GetJoins(joinSetup, joinSetup.Body as BinaryExpression));
        }
        
        public Selector2Base<T, TJoin> RightJoin<TJoin>(Expression<Func<T, TJoin, bool>> joinSetup)
        {
            return new Selector2Base<T, TJoin>(
                this,
                JoinType.Right,
                this.GetJoins(joinSetup, joinSetup.Body as BinaryExpression));
        }
        
        public Selector2Base<T, TJoin> FullJoin<TJoin>(Expression<Func<T, TJoin, bool>> joinSetup)
        {
            return new Selector2Base<T, TJoin>(
                this,
                JoinType.Full,
                this.GetJoins(joinSetup, joinSetup.Body as BinaryExpression));
        }
        
        public Selector2Base<T, TJoin> CrossJoin<TJoin>()
        {
            return new Selector2Base<T, TJoin>(
                this,
                JoinType.Cross,
                new Tuple<int, Column, int, Column>[0]);
        }
        
        public SelectData GetData()
        {
            return new SelectData {
                SchemaName =  this.backing.Schema(),
                TableName =  this.backing.TableName(),
                Distinct = this.distinct,
                FieldList = this.fields.Convert(!this.HasFieldsDeclared),
                WhereClause = this.wheres.Convert(),
                Offset = this.offset,
                Limit = this.limit,
                Ordering = this.orderBy.Convert(),
                Grouping = this.groupBy.Convert(),
                WithNoLock = this.nolock
            };
        }
    }
}

