﻿namespace Base2art.DataStorage.DataManipulation
{
    using System;
    using System.Threading.Tasks;
    using Base2art.DataStorage.DataManipulation.Builders;
    using Base2art.DataStorage.DataManipulation.Clauses;
    
    internal class Updater<T> : IQueryUpdate<T>
    {
        private readonly IDataManipulator manipulator;

        private WhereClauseWithValue<T> wheres;

        private RecordClauseWithValue<T> records;

        public Updater(IDataManipulator manipulator)
        {
            this.manipulator = manipulator;
        }
        
        public IQueryUpdate<T> Set(Action<ISetListBuilder<T>> recordSetup)
        {
            var record = new RecordClauseWithValue<T>();
            recordSetup(record);
            this.records = record;
            return this;
        }

        public IQueryUpdate<T> Where(Action<IWhereClauseBuilder<T>> recordSetup)
        {
            var record = new WhereClauseWithValue<T>();
            recordSetup(record);
            this.wheres = record;
            return this;
        }

        public Task Execute()
        {
            var tableType = typeof(T);
            return this.manipulator.UpdateAsync(tableType.Schema(), tableType.TableName(), this.records.Convert(), this.wheres.Convert());
        }
    }
}
