﻿namespace Base2art.DataStorage.DataManipulation
{
    using System;
    using System.Threading.Tasks;
    using Base2art.DataStorage.DataManipulation.Builders;
    using Base2art.DataStorage.DataManipulation.Clauses;

    internal class Deleter<T> : IQueryDelete<T>
    {
        private WhereClauseWithValue<T> wheres;

        private readonly IDataManipulator manipulator;
        
        public Deleter(IDataManipulator manipulator)
        {
            this.manipulator = manipulator;
        }

        public IQueryDelete<T> Where(Action<IWhereClauseBuilder<T>> recordSetup)
        {
            var record = new WhereClauseWithValue<T>();
            recordSetup(record);
            this.wheres = record;
            return this;
        }

        public Task Execute()
        {
            var type = typeof(T);
            return this.manipulator.DeleteAsync(type.Schema(), type.TableName(), this.wheres.Convert());
        }
    }
}
