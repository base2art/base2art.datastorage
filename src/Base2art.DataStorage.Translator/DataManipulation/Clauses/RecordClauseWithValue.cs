﻿namespace Base2art.DataStorage.DataManipulation.Clauses
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using Base2art.DataStorage.DataManipulation.Builders;
    using Base2art.DataStorage.Utils;
    
    // TYPED
    internal class RecordClauseWithValue<T> : ISetListBuilder<T>
    {
        private readonly Dictionary<string, object> valueMap = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
        
        // REGION {d9f739dbc7a73a871ebbcfd6ae80536f}

        ISetListBuilder<T> ISetListBuilder<T>.Field(Expression<Func<T, string>> caller, string data)
        {
            this.valueMap[caller.Body.GetMemberName(typeof(T).TableName())] = data;
            return this;
        }

        ISetListBuilder<T> ISetListBuilder<T>.Field(Expression<Func<T, byte[]>> caller, byte[] data)
        {
            this.valueMap[caller.Body.GetMemberName(typeof(T).TableName())] = data;
            return this;
        }

        ISetListBuilder<T> ISetListBuilder<T>.Field(Expression<Func<T, Guid?>> caller, Guid? data)
        {
            this.valueMap[caller.Body.GetMemberName(typeof(T).TableName())] = data;
            return this;
        }

        ISetListBuilder<T> ISetListBuilder<T>.Field(Expression<Func<T, long?>> caller, long? data)
        {
            this.valueMap[caller.Body.GetMemberName(typeof(T).TableName())] = data;
            return this;
        }

        ISetListBuilder<T> ISetListBuilder<T>.Field(Expression<Func<T, int?>> caller, int? data)
        {
            this.valueMap[caller.Body.GetMemberName(typeof(T).TableName())] = data;
            return this;
        }

        ISetListBuilder<T> ISetListBuilder<T>.Field(Expression<Func<T, DateTime?>> caller, DateTime? data)
        {
            this.valueMap[caller.Body.GetMemberName(typeof(T).TableName())] = data;
            return this;
        }

        ISetListBuilder<T> ISetListBuilder<T>.Field(Expression<Func<T, TimeSpan?>> caller, TimeSpan? data)
        {
            this.valueMap[caller.Body.GetMemberName(typeof(T).TableName())] = data;
            return this;
        }

        ISetListBuilder<T> ISetListBuilder<T>.Field(Expression<Func<T, short?>> caller, short? data)
        {
            this.valueMap[caller.Body.GetMemberName(typeof(T).TableName())] = data;
            return this;
        }

        ISetListBuilder<T> ISetListBuilder<T>.Field(Expression<Func<T, System.Xml.XmlDocument>> caller, System.Xml.XmlDocument data)
        {
            this.valueMap[caller.Body.GetMemberName(typeof(T).TableName())] = data;
            return this;
        }

        ISetListBuilder<T> ISetListBuilder<T>.Field(Expression<Func<T, System.Xml.Linq.XElement>> caller, System.Xml.Linq.XElement data)
        {
            this.valueMap[caller.Body.GetMemberName(typeof(T).TableName())] = data;
            return this;
        }

        ISetListBuilder<T> ISetListBuilder<T>.Field(Expression<Func<T, decimal?>> caller, decimal? data)
        {
            this.valueMap[caller.Body.GetMemberName(typeof(T).TableName())] = data;
            return this;
        }

        ISetListBuilder<T> ISetListBuilder<T>.Field(Expression<Func<T, float?>> caller, float? data)
        {
            this.valueMap[caller.Body.GetMemberName(typeof(T).TableName())] = data;
            return this;
        }

        ISetListBuilder<T> ISetListBuilder<T>.Field(Expression<Func<T, double?>> caller, double? data)
        {
            this.valueMap[caller.Body.GetMemberName(typeof(T).TableName())] = data;
            return this;
        }

        ISetListBuilder<T> ISetListBuilder<T>.Field(Expression<Func<T, bool?>> caller, bool? data)
        {
            this.valueMap[caller.Body.GetMemberName(typeof(T).TableName())] = data;
            return this;
        }

        ISetListBuilder<T> ISetListBuilder<T>.Field(Expression<Func<T, System.Collections.Generic.Dictionary<string, object>>> caller, System.Collections.Generic.Dictionary<string, object> data)
        {
            this.valueMap[caller.Body.GetMemberName(typeof(T).TableName())] = data;
            return this;
        }
        // END-REGION {d9f739dbc7a73a871ebbcfd6ae80536f}


        

        public IEnumerable<string> GetNames()
        {
            return this.valueMap.Keys;
        }

        public IDictionary<string, object> GetFieldDataMap()
        {
            var data = new Dictionary<string, object>();
            
            foreach (var item in this.valueMap.Keys)
            {
                data[item] = this.valueMap[item];
            }
            
            return data;
        }
    }
}
