﻿
namespace Base2art.DataStorage.DataManipulation.Clauses
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Linq;
    using Base2art.DataStorage.DataManipulation.Builders;
    using Base2art.DataStorage.DataManipulation.Data;
    using Base2art.DataStorage.Utils;
    
    partial class WhereClauseWithValue<T>
    {
        // REGION {c25aecc88b3c18c22b8dae6340962d42}
        IWhereClauseBuilder<T> IWhereClauseBuilder<T>.FieldNotIn<TOther>(
            Expression<Func<T, string>> caller,
            IEnumerable<TOther> data)
        {
            var mi = caller.GetMemberInfo(typeof(T).TableName());
            this.notInMap.Add(Tuple.Create<Column, IEnumerable>(mi, data));
            return this;
        }


        IWhereClauseBuilder<T> IWhereClauseBuilder<T>.FieldNotIn<TOther>(
            Expression<Func<T, byte[]>> caller,
            IEnumerable<TOther> data)
        {
            var mi = caller.GetMemberInfo(typeof(T).TableName());
            this.notInMap.Add(Tuple.Create<Column, IEnumerable>(mi, data));
            return this;
        }


        IWhereClauseBuilder<T> IWhereClauseBuilder<T>.FieldNotIn<TOther>(
            Expression<Func<T, Guid?>> caller,
            IEnumerable<TOther> data)
        {
            var mi = caller.GetMemberInfo(typeof(T).TableName());
            this.notInMap.Add(Tuple.Create<Column, IEnumerable>(mi, data));
            return this;
        }


        IWhereClauseBuilder<T> IWhereClauseBuilder<T>.FieldNotIn<TOther>(
            Expression<Func<T, long?>> caller,
            IEnumerable<TOther> data)
        {
            var mi = caller.GetMemberInfo(typeof(T).TableName());
            this.notInMap.Add(Tuple.Create<Column, IEnumerable>(mi, data));
            return this;
        }


        IWhereClauseBuilder<T> IWhereClauseBuilder<T>.FieldNotIn<TOther>(
            Expression<Func<T, int?>> caller,
            IEnumerable<TOther> data)
        {
            var mi = caller.GetMemberInfo(typeof(T).TableName());
            this.notInMap.Add(Tuple.Create<Column, IEnumerable>(mi, data));
            return this;
        }


        IWhereClauseBuilder<T> IWhereClauseBuilder<T>.FieldNotIn<TOther>(
            Expression<Func<T, DateTime?>> caller,
            IEnumerable<TOther> data)
        {
            var mi = caller.GetMemberInfo(typeof(T).TableName());
            this.notInMap.Add(Tuple.Create<Column, IEnumerable>(mi, data));
            return this;
        }


        IWhereClauseBuilder<T> IWhereClauseBuilder<T>.FieldNotIn<TOther>(
            Expression<Func<T, TimeSpan?>> caller,
            IEnumerable<TOther> data)
        {
            var mi = caller.GetMemberInfo(typeof(T).TableName());
            this.notInMap.Add(Tuple.Create<Column, IEnumerable>(mi, data));
            return this;
        }


        IWhereClauseBuilder<T> IWhereClauseBuilder<T>.FieldNotIn<TOther>(
            Expression<Func<T, short?>> caller,
            IEnumerable<TOther> data)
        {
            var mi = caller.GetMemberInfo(typeof(T).TableName());
            this.notInMap.Add(Tuple.Create<Column, IEnumerable>(mi, data));
            return this;
        }


        IWhereClauseBuilder<T> IWhereClauseBuilder<T>.FieldNotIn<TOther>(
            Expression<Func<T, System.Xml.XmlDocument>> caller,
            IEnumerable<TOther> data)
        {
            var mi = caller.GetMemberInfo(typeof(T).TableName());
            this.notInMap.Add(Tuple.Create<Column, IEnumerable>(mi, data));
            return this;
        }


        IWhereClauseBuilder<T> IWhereClauseBuilder<T>.FieldNotIn<TOther>(
            Expression<Func<T, System.Xml.Linq.XElement>> caller,
            IEnumerable<TOther> data)
        {
            var mi = caller.GetMemberInfo(typeof(T).TableName());
            this.notInMap.Add(Tuple.Create<Column, IEnumerable>(mi, data));
            return this;
        }


        IWhereClauseBuilder<T> IWhereClauseBuilder<T>.FieldNotIn<TOther>(
            Expression<Func<T, decimal?>> caller,
            IEnumerable<TOther> data)
        {
            var mi = caller.GetMemberInfo(typeof(T).TableName());
            this.notInMap.Add(Tuple.Create<Column, IEnumerable>(mi, data));
            return this;
        }


        IWhereClauseBuilder<T> IWhereClauseBuilder<T>.FieldNotIn<TOther>(
            Expression<Func<T, float?>> caller,
            IEnumerable<TOther> data)
        {
            var mi = caller.GetMemberInfo(typeof(T).TableName());
            this.notInMap.Add(Tuple.Create<Column, IEnumerable>(mi, data));
            return this;
        }


        IWhereClauseBuilder<T> IWhereClauseBuilder<T>.FieldNotIn<TOther>(
            Expression<Func<T, double?>> caller,
            IEnumerable<TOther> data)
        {
            var mi = caller.GetMemberInfo(typeof(T).TableName());
            this.notInMap.Add(Tuple.Create<Column, IEnumerable>(mi, data));
            return this;
        }


        IWhereClauseBuilder<T> IWhereClauseBuilder<T>.FieldNotIn<TOther>(
            Expression<Func<T, bool?>> caller,
            IEnumerable<TOther> data)
        {
            var mi = caller.GetMemberInfo(typeof(T).TableName());
            this.notInMap.Add(Tuple.Create<Column, IEnumerable>(mi, data));
            return this;
        }


        IWhereClauseBuilder<T> IWhereClauseBuilder<T>.FieldNotIn<TOther>(
            Expression<Func<T, System.Collections.Generic.Dictionary<string, object>>> caller,
            IEnumerable<TOther> data)
        {
            var mi = caller.GetMemberInfo(typeof(T).TableName());
            this.notInMap.Add(Tuple.Create<Column, IEnumerable>(mi, data));
            return this;
        }
        // END-REGION {c25aecc88b3c18c22b8dae6340962d42}
    }
}
