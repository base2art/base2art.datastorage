﻿namespace Base2art.DataStorage.DataManipulation.Clauses
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Linq;
    using Base2art.DataStorage.DataManipulation.Builders;
    using Base2art.DataStorage.DataManipulation.Data;
    using Base2art.DataStorage.Utils;

    internal class FieldSelectClause<T> : IFieldListBuilder<T>
    {
        private readonly List<Tuple<Column, OperatorType?, Column>> valueMap = new List<Tuple<Column, OperatorType?, Column>>();

        IFieldListBuilder<T> IFieldListBuilder<T>.All()
        {
            this.valueMap.Add(Tuple.Create<Column, OperatorType?, Column>(Column.Custom(SpecialColumns.All), null, null));
            return this;
        }
        
        IFieldListBuilder<T> IFieldListBuilder<T>.Count(Expression<Func<T, long?>> caller)
        {
            var data = caller.GetComplexMemberInfo(typeof(T).TableName(), false);
            this.valueMap.Add(Tuple.Create<Column, OperatorType?, Column>(Column.CustomWithAlias(SpecialColumns.Count, data.Item1.ColumnName), null, null));
            return this;
        }
        
        // REGION {19df13eb786a015aad0dfe93db403ffc}

        IFieldListBuilder<T> IFieldListBuilder<T>.Field(Expression<Func<T, string>> caller)
        {
            return this.Add(caller);
        }

        IFieldListBuilder<T> IFieldListBuilder<T>.Field(Expression<Func<T, byte[]>> caller)
        {
            return this.Add(caller);
        }

        IFieldListBuilder<T> IFieldListBuilder<T>.Field(Expression<Func<T, Guid?>> caller)
        {
            return this.Add(caller);
        }

        IFieldListBuilder<T> IFieldListBuilder<T>.Field(Expression<Func<T, long?>> caller)
        {
            return this.Add(caller);
        }

        IFieldListBuilder<T> IFieldListBuilder<T>.Field(Expression<Func<T, int?>> caller)
        {
            return this.Add(caller);
        }

        IFieldListBuilder<T> IFieldListBuilder<T>.Field(Expression<Func<T, DateTime?>> caller)
        {
            return this.Add(caller);
        }

        IFieldListBuilder<T> IFieldListBuilder<T>.Field(Expression<Func<T, TimeSpan?>> caller)
        {
            return this.Add(caller);
        }

        IFieldListBuilder<T> IFieldListBuilder<T>.Field(Expression<Func<T, short?>> caller)
        {
            return this.Add(caller);
        }

        IFieldListBuilder<T> IFieldListBuilder<T>.Field(Expression<Func<T, System.Xml.XmlDocument>> caller)
        {
            return this.Add(caller);
        }

        IFieldListBuilder<T> IFieldListBuilder<T>.Field(Expression<Func<T, System.Xml.Linq.XElement>> caller)
        {
            return this.Add(caller);
        }

        IFieldListBuilder<T> IFieldListBuilder<T>.Field(Expression<Func<T, decimal?>> caller)
        {
            return this.Add(caller);
        }

        IFieldListBuilder<T> IFieldListBuilder<T>.Field(Expression<Func<T, float?>> caller)
        {
            return this.Add(caller);
        }

        IFieldListBuilder<T> IFieldListBuilder<T>.Field(Expression<Func<T, double?>> caller)
        {
            return this.Add(caller);
        }

        IFieldListBuilder<T> IFieldListBuilder<T>.Field(Expression<Func<T, bool?>> caller)
        {
            return this.Add(caller);
        }

        IFieldListBuilder<T> IFieldListBuilder<T>.Field(Expression<Func<T, System.Collections.Generic.Dictionary<string, object>>> caller)
        {
            return this.Add(caller);
        }
        // END-REGION {19df13eb786a015aad0dfe93db403ffc}

        private IFieldListBuilder<T> Add(Expression caller)
        {
            var data = caller.GetComplexMemberInfo(typeof(T).TableName(), false);
            this.valueMap.Add(data);
            return this;
        }
        
        public Tuple<Column, OperatorType?, Column>[] GetNames()
        {
            return this.valueMap.ToArray();
        }
    }
}


