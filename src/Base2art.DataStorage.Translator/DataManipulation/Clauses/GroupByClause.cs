﻿namespace Base2art.DataStorage.DataManipulation.Clauses
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq.Expressions;
    using Base2art.DataStorage.DataManipulation.Builders;
    using Base2art.DataStorage.DataManipulation.Data;
    using Base2art.DataStorage.Utils;
    
    public class GroupByBuilder<T> : IGroupByBuilder<T>
    {
        private readonly List<Column> lists = new List<Column>();

        
        public Column[] GetNames()
        {
            return this.lists.ToArray();
        }
        
        // REGION {23ca8150eebec9e1ec583888305f96a4}

        public IGroupByBuilder<T> Field(Expression<Func<T, string>> caller)
        {
            this.lists.Add(Column.Simple(typeof(T).TableName(), caller.Body.GetMemberName(typeof(T).TableName()), typeof(string)));
            return this;
        }

        public IGroupByBuilder<T> Field(Expression<Func<T, byte[]>> caller)
        {
            this.lists.Add(Column.Simple(typeof(T).TableName(), caller.Body.GetMemberName(typeof(T).TableName()), typeof(byte[])));
            return this;
        }

        public IGroupByBuilder<T> Field(Expression<Func<T, Guid?>> caller)
        {
            this.lists.Add(Column.Simple(typeof(T).TableName(), caller.Body.GetMemberName(typeof(T).TableName()), typeof(Guid?)));
            return this;
        }

        public IGroupByBuilder<T> Field(Expression<Func<T, long?>> caller)
        {
            this.lists.Add(Column.Simple(typeof(T).TableName(), caller.Body.GetMemberName(typeof(T).TableName()), typeof(long?)));
            return this;
        }

        public IGroupByBuilder<T> Field(Expression<Func<T, int?>> caller)
        {
            this.lists.Add(Column.Simple(typeof(T).TableName(), caller.Body.GetMemberName(typeof(T).TableName()), typeof(int?)));
            return this;
        }

        public IGroupByBuilder<T> Field(Expression<Func<T, DateTime?>> caller)
        {
            this.lists.Add(Column.Simple(typeof(T).TableName(), caller.Body.GetMemberName(typeof(T).TableName()), typeof(DateTime?)));
            return this;
        }

        public IGroupByBuilder<T> Field(Expression<Func<T, TimeSpan?>> caller)
        {
            this.lists.Add(Column.Simple(typeof(T).TableName(), caller.Body.GetMemberName(typeof(T).TableName()), typeof(TimeSpan?)));
            return this;
        }

        public IGroupByBuilder<T> Field(Expression<Func<T, short?>> caller)
        {
            this.lists.Add(Column.Simple(typeof(T).TableName(), caller.Body.GetMemberName(typeof(T).TableName()), typeof(short?)));
            return this;
        }

        public IGroupByBuilder<T> Field(Expression<Func<T, System.Xml.XmlDocument>> caller)
        {
            this.lists.Add(Column.Simple(typeof(T).TableName(), caller.Body.GetMemberName(typeof(T).TableName()), typeof(System.Xml.XmlDocument)));
            return this;
        }

        public IGroupByBuilder<T> Field(Expression<Func<T, System.Xml.Linq.XElement>> caller)
        {
            this.lists.Add(Column.Simple(typeof(T).TableName(), caller.Body.GetMemberName(typeof(T).TableName()), typeof(System.Xml.Linq.XElement)));
            return this;
        }

        public IGroupByBuilder<T> Field(Expression<Func<T, decimal?>> caller)
        {
            this.lists.Add(Column.Simple(typeof(T).TableName(), caller.Body.GetMemberName(typeof(T).TableName()), typeof(decimal?)));
            return this;
        }

        public IGroupByBuilder<T> Field(Expression<Func<T, float?>> caller)
        {
            this.lists.Add(Column.Simple(typeof(T).TableName(), caller.Body.GetMemberName(typeof(T).TableName()), typeof(float?)));
            return this;
        }

        public IGroupByBuilder<T> Field(Expression<Func<T, double?>> caller)
        {
            this.lists.Add(Column.Simple(typeof(T).TableName(), caller.Body.GetMemberName(typeof(T).TableName()), typeof(double?)));
            return this;
        }

        public IGroupByBuilder<T> Field(Expression<Func<T, bool?>> caller)
        {
            this.lists.Add(Column.Simple(typeof(T).TableName(), caller.Body.GetMemberName(typeof(T).TableName()), typeof(bool?)));
            return this;
        }

        public IGroupByBuilder<T> Field(Expression<Func<T, System.Collections.Generic.Dictionary<string, object>>> caller)
        {
            this.lists.Add(Column.Simple(typeof(T).TableName(), caller.Body.GetMemberName(typeof(T).TableName()), typeof(System.Collections.Generic.Dictionary<string, object>)));
            return this;
        }
        // END-REGION {23ca8150eebec9e1ec583888305f96a4}
    }
}
