﻿
using System;
using Base2art.DataStorage.DataManipulation.Builders;

namespace Base2art.DataStorage.DataManipulation.Clauses
{
    public class SpecialFieldClause<T> : ISpecialFieldBuilder<T>
    {
        public bool HasCount
        {
            get;
            set;
        }

        public void Count()
        {
            this.HasCount = true;
        }
    }
}
