﻿namespace Base2art.DataStorage.DataManipulation.Clauses
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq.Expressions;
    using Base2art.DataStorage.DataManipulation.Builders;
    using Base2art.DataStorage.DataManipulation.Data;
    using Base2art.DataStorage.Utils;
    
    public class OrderByBuilder<T> : IOrderByBuilder<T>
    {
        private readonly List<Tuple<Column, ListSortDirection>> lists = new List<Tuple<Column, ListSortDirection>>();
        
        public Tuple<Column, ListSortDirection>[] GetNames()
        {
            return this.lists.ToArray();
        }
        
        public IOrderByBuilder<T> Random()
        {
            this.lists.Add(Tuple.Create(Column.Custom(SpecialColumns.NewUUID), ListSortDirection.Ascending));
            return this;
        }
        
        // REGION {23ca8150eebec9e1ec583888305f96a4}

        public IOrderByBuilder<T> Field(Expression<Func<T, string>> caller, ListSortDirection direction)
        {
            this.lists.Add(Tuple.Create(Column.Simple(typeof(T).TableName(), caller.Body.GetMemberName(typeof(T).TableName()), typeof(string)), direction));
            return this;
        }

        public IOrderByBuilder<T> Field(Expression<Func<T, byte[]>> caller, ListSortDirection direction)
        {
            this.lists.Add(Tuple.Create(Column.Simple(typeof(T).TableName(), caller.Body.GetMemberName(typeof(T).TableName()), typeof(byte[])), direction));
            return this;
        }

        public IOrderByBuilder<T> Field(Expression<Func<T, Guid?>> caller, ListSortDirection direction)
        {
            this.lists.Add(Tuple.Create(Column.Simple(typeof(T).TableName(), caller.Body.GetMemberName(typeof(T).TableName()), typeof(Guid?)), direction));
            return this;
        }

        public IOrderByBuilder<T> Field(Expression<Func<T, long?>> caller, ListSortDirection direction)
        {
            this.lists.Add(Tuple.Create(Column.Simple(typeof(T).TableName(), caller.Body.GetMemberName(typeof(T).TableName()), typeof(long?)), direction));
            return this;
        }

        public IOrderByBuilder<T> Field(Expression<Func<T, int?>> caller, ListSortDirection direction)
        {
            this.lists.Add(Tuple.Create(Column.Simple(typeof(T).TableName(), caller.Body.GetMemberName(typeof(T).TableName()), typeof(int?)), direction));
            return this;
        }

        public IOrderByBuilder<T> Field(Expression<Func<T, DateTime?>> caller, ListSortDirection direction)
        {
            this.lists.Add(Tuple.Create(Column.Simple(typeof(T).TableName(), caller.Body.GetMemberName(typeof(T).TableName()), typeof(DateTime?)), direction));
            return this;
        }

        public IOrderByBuilder<T> Field(Expression<Func<T, TimeSpan?>> caller, ListSortDirection direction)
        {
            this.lists.Add(Tuple.Create(Column.Simple(typeof(T).TableName(), caller.Body.GetMemberName(typeof(T).TableName()), typeof(TimeSpan?)), direction));
            return this;
        }

        public IOrderByBuilder<T> Field(Expression<Func<T, short?>> caller, ListSortDirection direction)
        {
            this.lists.Add(Tuple.Create(Column.Simple(typeof(T).TableName(), caller.Body.GetMemberName(typeof(T).TableName()), typeof(short?)), direction));
            return this;
        }

        public IOrderByBuilder<T> Field(Expression<Func<T, System.Xml.XmlDocument>> caller, ListSortDirection direction)
        {
            this.lists.Add(Tuple.Create(Column.Simple(typeof(T).TableName(), caller.Body.GetMemberName(typeof(T).TableName()), typeof(System.Xml.XmlDocument)), direction));
            return this;
        }

        public IOrderByBuilder<T> Field(Expression<Func<T, System.Xml.Linq.XElement>> caller, ListSortDirection direction)
        {
            this.lists.Add(Tuple.Create(Column.Simple(typeof(T).TableName(), caller.Body.GetMemberName(typeof(T).TableName()), typeof(System.Xml.Linq.XElement)), direction));
            return this;
        }

        public IOrderByBuilder<T> Field(Expression<Func<T, decimal?>> caller, ListSortDirection direction)
        {
            this.lists.Add(Tuple.Create(Column.Simple(typeof(T).TableName(), caller.Body.GetMemberName(typeof(T).TableName()), typeof(decimal?)), direction));
            return this;
        }

        public IOrderByBuilder<T> Field(Expression<Func<T, float?>> caller, ListSortDirection direction)
        {
            this.lists.Add(Tuple.Create(Column.Simple(typeof(T).TableName(), caller.Body.GetMemberName(typeof(T).TableName()), typeof(float?)), direction));
            return this;
        }

        public IOrderByBuilder<T> Field(Expression<Func<T, double?>> caller, ListSortDirection direction)
        {
            this.lists.Add(Tuple.Create(Column.Simple(typeof(T).TableName(), caller.Body.GetMemberName(typeof(T).TableName()), typeof(double?)), direction));
            return this;
        }

        public IOrderByBuilder<T> Field(Expression<Func<T, bool?>> caller, ListSortDirection direction)
        {
            this.lists.Add(Tuple.Create(Column.Simple(typeof(T).TableName(), caller.Body.GetMemberName(typeof(T).TableName()), typeof(bool?)), direction));
            return this;
        }

        public IOrderByBuilder<T> Field(Expression<Func<T, System.Collections.Generic.Dictionary<string, object>>> caller, ListSortDirection direction)
        {
            this.lists.Add(Tuple.Create(Column.Simple(typeof(T).TableName(), caller.Body.GetMemberName(typeof(T).TableName()), typeof(System.Collections.Generic.Dictionary<string, object>)), direction));
            return this;
        }
        // END-REGION {23ca8150eebec9e1ec583888305f96a4}
    }
}
