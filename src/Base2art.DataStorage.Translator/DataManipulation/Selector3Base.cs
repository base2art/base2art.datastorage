﻿namespace Base2art.DataStorage.DataManipulation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using Base2art.DataStorage.DataManipulation.Builders;
    using Base2art.DataStorage.DataManipulation.Clauses;
    using Base2art.DataStorage.DataManipulation.Data;

    internal class Selector3Base<T1, T2, T3> :  IDataGetter<SelectData>
    {
        private readonly Selector2Base<T1, T2> selector;

        private WhereClauseWithValue<T3> wheres;

        private FieldSelectClause<T3> fields;

        private OrderByBuilder<T3> orderBy;

        private GroupByBuilder<T3> groupBy;
        
        private readonly JoinType jointype;

        private readonly Tuple<int, Column, int, Column>[] columnJoins;

        public Selector3Base(
            Selector2Base<T1, T2> selector,
            JoinType jointype,
            Tuple<int, Column, int, Column>[] columnJoins)
        {
            this.selector = selector;
            this.jointype = jointype;
            this.columnJoins = columnJoins;
        }
        
        public FieldSelectClause<T1> Field1Data
        {
            get { return this.selector.Field1Data; }
            set { this.selector.Field1Data = value; }
        }
        
        public FieldSelectClause<T2> Field2Data
        {
            get { return this.selector.Field2Data; }
            set { this.selector.Field2Data = value; }
        }
        
        public FieldSelectClause<T3> Field3Data
        {
            get { return this.fields;}
            set { this.fields = value; }
        }
        
        internal OrderByBuilder<T1> Order1Data
        {
            get{ return this.selector.Order1Data; }
            set{ this.selector.Order1Data = value; }
        }
        
        internal OrderByBuilder<T2> Order2Data
        {
            get{ return this.selector.Order2Data; }
            set{ this.selector.Order2Data = value; }
        }
        
        internal OrderByBuilder<T3> Order3Data
        {
            get { return this.orderBy;}
            set { this.orderBy = value; }
        }

        public WhereClauseWithValue<T1> Where1Data
        {
            get{ return this.selector.Where1Data; }
            set{ this.selector.Where1Data = value; }
        }

        public WhereClauseWithValue<T2> Where2Data
        {
            get{ return this.selector.Where2Data; }
            set{ this.selector.Where2Data = value; }
        }

        public WhereClauseWithValue<T3> Where3Data
        {
            get { return this.wheres; }
            set { this.wheres = value; }
        }

        internal GroupByBuilder<T1> Group1Data
        {
            get { return this.selector.Group1Data; }
            set { this.selector.Group1Data = value; }
        }

        internal GroupByBuilder<T2> Group2Data
        {
            get { return this.selector.Group2Data; }
            set { this.selector.Group2Data = value; }
        }

        internal GroupByBuilder<T3> Group3Data
        {
            get { return this.groupBy; }
            set { this.groupBy = value; }
        }

        public IDataManipulator Manipulator
        {
            get { return this.selector.Manipulator; }
        }

        public bool HasFieldsDeclared
        {
            get{ return this.selector.HasFieldsDeclared; }
        }
        
        public void DeclareFieldsInUse()
        {
            this.selector.DeclareFieldsInUse();
        }

        public Selector3Base<T1, T2, T3> Limit(int limit)
        {
            this.selector.Limit(limit);
            return this;
        }
        
        public Selector3Base<T1, T2, T3> Offset(int offset)
        {
            this.selector.Offset(offset);
            return this;
        }
        
        public Selector3Base<T1, T2, T3> WithNoLock()
        {
            this.selector.WithNoLock();
            return this;
        }

        public Selector3Base<T1, T2, T3> Distinct()
        {
            this.selector.Distinct();
            return this;
        }
        
        public Selector3Base<T1, T2, T3> Where1(Action<IWhereClauseBuilder<T1>> recordSetup)
        {
            this.selector.Where1Data = this.selector.Where1Data ?? new WhereClauseWithValue<T1>();
            recordSetup(this.selector.Where1Data);
            return this;
        }
        
        public Selector3Base<T1, T2, T3> Where2(Action<IWhereClauseBuilder<T2>> recordSetup)
        {
            this.selector.Where2Data = this.selector.Where2Data ?? new WhereClauseWithValue<T2>();
            recordSetup(this.selector.Where2Data);
            return this;
        }
        
        public Selector3Base<T1, T2, T3> Where3(Action<IWhereClauseBuilder<T3>> recordSetup)
        {
            var record = this.wheres ?? new WhereClauseWithValue<T3>();
            recordSetup(record);
            this.wheres = record;
            return this;
        }

        public Selector3Base<T1, T2, T3> Fields1(Action<IFieldListBuilder<T1>> fieldSetup)
        {
            this.selector.Field1Data = this.selector.Field1Data ?? new FieldSelectClause<T1>();
            fieldSetup(this.selector.Field1Data);
            this.selector.DeclareFieldsInUse();
            return this;
        }

        public Selector3Base<T1, T2, T3> Fields2(Action<IFieldListBuilder<T2>> fieldSetup)
        {
            this.selector.Field2Data = this.selector.Field2Data ?? new FieldSelectClause<T2>();
            fieldSetup(this.selector.Field2Data);
            this.DeclareFieldsInUse();
            return this;
        }

        public Selector3Base<T1, T2, T3> Fields3(Action<IFieldListBuilder<T3>> fieldSetup)
        {
            var record = this.fields ?? new FieldSelectClause<T3>();
            fieldSetup(record);
            this.fields = record;
            this.DeclareFieldsInUse();
            return this;
        }

        public Selector3Base<T1, T2, T3> OrderBy1(Action<IOrderByBuilder<T1>> recordSetup)
        {
            this.selector.Order1Data = this.selector.Order1Data ?? new OrderByBuilder<T1>();
            recordSetup(this.selector.Order1Data);
            return this;
        }

        public Selector3Base<T1, T2, T3> OrderBy2(Action<IOrderByBuilder<T2>> recordSetup)
        {
            this.selector.Order2Data = this.selector.Order2Data ?? new OrderByBuilder<T2>();
            recordSetup(this.selector.Order2Data);
            return this;
        }

        public Selector3Base<T1, T2, T3> OrderBy3(Action<IOrderByBuilder<T3>> recordSetup)
        {
            var record = this.orderBy ?? new OrderByBuilder<T3>();
            recordSetup(record);
            this.orderBy = record;
            return this;
        }

        public Selector4Base<T1, T2, T3, TAggregate> GroupBy1<TAggregate>(Action<IGroupByBuilder<T1>> recordSetup)
        {
            this.selector.Group1Data = this.selector.Group1Data ?? new GroupByBuilder<T1>();
            recordSetup(this.selector.Group1Data);
            return new Selector4Base<T1, T2, T3, TAggregate>(
                this,
                JoinType.AggregateGrouped,
                new Tuple<int, Column, int, Column>[0]);
        }

        public Selector4Base<T1, T2, T3, TAggregate> GroupBy2<TAggregate>(Action<IGroupByBuilder<T2>> recordSetup)
        {
            this.selector.Group2Data = this.selector.Group2Data ?? new GroupByBuilder<T2>();
            recordSetup(this.selector.Group2Data);
            return new Selector4Base<T1, T2, T3, TAggregate>(
                this,
                JoinType.AggregateGrouped,
                new Tuple<int, Column, int, Column>[0]);
        }

        public Selector4Base<T1, T2, T3, TAggregate> GroupBy3<TAggregate>(Action<IGroupByBuilder<T3>> recordSetup)
        {
            var record = this.groupBy ?? new GroupByBuilder<T3>();
            recordSetup(record);
            this.groupBy = record;
            return new Selector4Base<T1, T2, T3, TAggregate>(
                this,
                JoinType.AggregateGrouped,
                new Tuple<int, Column, int, Column>[0]);
        }

        public Selector4Base<T1, T2, T3, TAggregate> WithCalculated<TAggregate>()
        {
            return new Selector4Base<T1, T2, T3, TAggregate>(
                this,
                JoinType.Aggregate,
                new Tuple<int, Column, int, Column>[0]);
        }

        public Selector4Base<T1, T2, T3, TJoin> Join<TJoin>(Expression<Func<T1, T2, T3, TJoin, bool>> joinSetup)
        {
            return new Selector4Base<T1, T2, T3, TJoin>(
                this,
                JoinType.Inner,
                this.GetJoins(joinSetup, joinSetup.Body as BinaryExpression));
        }
        
        public Selector4Base<T1, T2, T3, TJoin> LeftJoin<TJoin>(Expression<Func<T1, T2, T3, TJoin, bool>> joinSetup)
        {
            return new Selector4Base<T1, T2, T3, TJoin>(
                this,
                JoinType.Left,
                this.GetJoins(joinSetup, joinSetup.Body as BinaryExpression));
        }
        
        public Selector4Base<T1, T2, T3, TJoin> RightJoin<TJoin>(Expression<Func<T1, T2, T3, TJoin, bool>> joinSetup)
        {
            return new Selector4Base<T1, T2, T3, TJoin>(
                this,
                JoinType.Right,
                this.GetJoins(joinSetup, joinSetup.Body as BinaryExpression));
        }
        
        public Selector4Base<T1, T2, T3, TJoin> FullJoin<TJoin>(Expression<Func<T1, T2, T3, TJoin, bool>> joinSetup)
        {
            return new Selector4Base<T1, T2, T3, TJoin>(
                this,
                JoinType.Full,
                this.GetJoins(joinSetup, joinSetup.Body as BinaryExpression));
        }
        
        public Selector4Base<T1, T2, T3, TJoin> CrossJoin<TJoin>()
        {
            return new Selector4Base<T1, T2, T3, TJoin>(
                this,
                JoinType.Cross,
                new Tuple<int, Column, int, Column>[0]);
        }
        
        public SelectData GetData()
        {
            var data = this.selector.GetData();
            Func<SelectData, SelectData> findNullJoin = null;
            findNullJoin = (x)=>
            {
                if (x.JoinData == null)
                {
                    return x;
                }
                
                return findNullJoin(x.JoinData.SelectData);
            };
            
            var thisData = new SelectData
            {
                SchemaName = typeof(T3).Schema(),
                TableName = typeof(T3).TableName(),
                Distinct = data.Distinct,
                FieldList = this.fields.Convert(!this.HasFieldsDeclared),
                WhereClause = this.wheres.Convert(),
                Offset = data.Offset,
                Limit = data.Limit,
                Ordering = this.orderBy.Convert(),
                Grouping = this.groupBy.Convert()
            };
            
            var dataToUpdate = findNullJoin(data);
            dataToUpdate.JoinData = new JoinData();
            
            dataToUpdate.JoinData.JoinType = this.jointype;
            dataToUpdate.JoinData.SelectData = thisData;
            dataToUpdate.JoinData.OnData = this.columnJoins.Select(x=>Tuple.Create(x.Item1, x.Item2, OperatorType.IdentityEquality, x.Item3, x.Item4)).ToArray();
            
            return data;
        }
    }
}
