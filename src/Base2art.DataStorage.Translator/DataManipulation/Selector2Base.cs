﻿namespace Base2art.DataStorage.DataManipulation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using Base2art.DataStorage.DataManipulation.Builders;
    using Base2art.DataStorage.DataManipulation.Clauses;
    using Base2art.DataStorage.DataManipulation.Data;
    using Base2art.DataStorage.Utils;

    internal class Selector2Base<T1, T2> : IDataGetter<SelectData>
    {
        private readonly Selector1Base<T1> selector;

        private WhereClauseWithValue<T2> wheres;

        private FieldSelectClause<T2> fields;

        private OrderByBuilder<T2> orderBy;

        private GroupByBuilder<T2> groupBy;
        
        private readonly JoinType jointype;

        private readonly Tuple<int, Column, int, Column>[] columnJoins;
        
        public Selector2Base(
            Selector1Base<T1> selector,
            JoinType jointype,
            Tuple<int, Column, int, Column>[] columnJoins)
        {
            this.columnJoins = columnJoins;
            this.selector = selector;
            this.jointype = jointype;
        }

        public IDataManipulator Manipulator
        {
            get { return this.selector.Manipulator; }
        }
        
        public FieldSelectClause<T1> Field1Data
        {
            get{ return this.selector.FieldData; }
            set{ this.selector.FieldData = value; }
        }

        public WhereClauseWithValue<T1> Where1Data
        {
            get{ return this.selector.WhereData; }
            set{ this.selector.WhereData = value; }
        }
        
        
        internal WhereClauseWithValue<T2> Where2Data
        {
            get { return this.wheres; }
            set { this.wheres = value; }
        }

        internal FieldSelectClause<T2> Field2Data
        {
            get { return this.fields; }
            set { this.fields = value; }
        }

        internal OrderByBuilder<T1> Order1Data
        {
            get{ return this.selector.OrderData; }
            set{ this.selector.OrderData = value; }
        }
        
        internal OrderByBuilder<T2> Order2Data
        {
            get { return this.orderBy; }
            set { this.orderBy = value; }
        }

        internal GroupByBuilder<T1> Group1Data
        {
            get { return this.selector.Group1Data; }
            set { this.selector.Group1Data = value; }
        }

        internal GroupByBuilder<T2> Group2Data
        {
            get { return this.groupBy; }
            set { this.groupBy = value; }
        }

        public bool HasFieldsDeclared
        {
            get{ return this.selector.HasFieldsDeclared; }
        }
        
        public void DeclareFieldsInUse()
        {
            this.selector.DeclareFieldsInUse();
        }
        
        public Selector2Base<T1, T2> Limit(int limit)
        {
            this.selector.Limit(limit);
            return this;
        }
        
        public Selector2Base<T1, T2> Offset(int offset)
        {
            this.selector.Offset(offset);
            return this;
        }
        
        public Selector2Base<T1, T2> WithNoLock()
        {
            this.selector.WithNoLock();
            return this;
        }
        
        public Selector2Base<T1, T2> Distinct()
        {
            this.selector.Distinct();
            return this;
        }
        
        public Selector2Base<T1, T2> Where1(Action<IWhereClauseBuilder<T1>> recordSetup)
        {
            this.selector.WhereData = this.selector.WhereData ?? new WhereClauseWithValue<T1>();
            recordSetup(this.selector.WhereData);
            return this;
        }
        
        public Selector2Base<T1, T2> Where2(Action<IWhereClauseBuilder<T2>> recordSetup)
        {
            var record = this.wheres ?? new WhereClauseWithValue<T2>();
            recordSetup(record);
            this.wheres = record;
            return this;
        }

        public Selector2Base<T1, T2> Fields1(Action<IFieldListBuilder<T1>> fieldSetup)
        {
            this.selector.FieldData = this.selector.FieldData ?? new FieldSelectClause<T1>();
            fieldSetup(this.selector.FieldData);
            this.DeclareFieldsInUse();
            return this;
        }

        public Selector2Base<T1, T2> Fields2(Action<IFieldListBuilder<T2>> fieldSetup)
        {
            var record = this.fields ?? new FieldSelectClause<T2>();
            fieldSetup(record);
            this.fields = record;
            this.DeclareFieldsInUse();
            return this;
        }

        public Selector2Base<T1, T2> OrderBy1(Action<IOrderByBuilder<T1>> recordSetup)
        {
            this.selector.OrderData = this.selector.OrderData ?? new OrderByBuilder<T1>();
            recordSetup(this.selector.OrderData);
            return this;
        }

        public Selector2Base<T1, T2> OrderBy2(Action<IOrderByBuilder<T2>> recordSetup)
        {
            var record = this.orderBy ?? new OrderByBuilder<T2>();
            recordSetup(record);
            this.orderBy = record;
            return this;
        }

        public Selector3Base<T1, T2, TAggregate> GroupBy1<TAggregate>(Action<IGroupByBuilder<T1>> recordSetup)
        {
            this.selector.Group1Data = this.selector.Group1Data ?? new GroupByBuilder<T1>();
            recordSetup(this.selector.Group1Data);
            return new Selector3Base<T1, T2, TAggregate>(
                this,
                JoinType.AggregateGrouped,
                new Tuple<int, Column, int, Column>[0]);
        }

        public Selector3Base<T1, T2, TAggregate> GroupBy2<TAggregate>(Action<IGroupByBuilder<T2>> recordSetup)
        {
            var record = this.groupBy ?? new GroupByBuilder<T2>();
            recordSetup(record);
            this.groupBy = record;
            return new Selector3Base<T1, T2, TAggregate>(
                this,
                JoinType.AggregateGrouped,
                new Tuple<int, Column, int, Column>[0]);
        }

        public Selector3Base<T1, T2, TAggregate> WithCalculated<TAggregate>()
        {
            return new Selector3Base<T1, T2, TAggregate>(
                this,
                JoinType.Aggregate,
                new Tuple<int, Column, int, Column>[0]);
        }
        
        public Selector3Base<T1, T2, TJoin> Join<TJoin>(Expression<Func<T1, T2, TJoin, bool>> joinSetup)
        {
            return new Selector3Base<T1, T2, TJoin>(
                this,
                JoinType.Inner,
                this.GetJoins(joinSetup, joinSetup.Body as BinaryExpression));
        }
        
        public Selector3Base<T1, T2, TJoin> LeftJoin<TJoin>(Expression<Func<T1, T2, TJoin, bool>> joinSetup)
        {
            return new Selector3Base<T1, T2, TJoin>(
                this,
                JoinType.Left,
                this.GetJoins(joinSetup, joinSetup.Body as BinaryExpression));
        }
        
        public Selector3Base<T1, T2, TJoin> RightJoin<TJoin>(Expression<Func<T1, T2, TJoin, bool>> joinSetup)
        {
            return new Selector3Base<T1, T2, TJoin>(
                this,
                JoinType.Right,
                this.GetJoins(joinSetup, joinSetup.Body as BinaryExpression));
        }
        
        public Selector3Base<T1, T2, TJoin> FullJoin<TJoin>(Expression<Func<T1, T2, TJoin, bool>> joinSetup)
        {
            return new Selector3Base<T1, T2, TJoin>(
                this,
                JoinType.Full,
                this.GetJoins(joinSetup, joinSetup.Body as BinaryExpression));
        }
        
        public Selector3Base<T1, T2, TJoin> CrossJoin<TJoin>()
        {
            return new Selector3Base<T1, T2, TJoin>(
                this,
                JoinType.Cross,
                new Tuple<int, Column, int, Column>[0]);
        }
        
        public SelectData GetData()
        {
            var data = this.selector.GetData();
            Func<SelectData, SelectData> findNullJoin = null;
            findNullJoin = (x) =>
            {
                if (x.JoinData == null)
                {
                    return x;
                }
                
                return findNullJoin(x.JoinData.SelectData);
            };
            
            var thisData = new SelectData
            {
                SchemaName = typeof(T2).Schema(),
                TableName = typeof(T2).TableName(),
                Distinct = data.Distinct,
                FieldList = this.fields.Convert(!this.HasFieldsDeclared),
                WhereClause = this.wheres.Convert(),
                Offset = data.Offset,
                Limit = data.Limit,
                Ordering = this.orderBy.Convert(),
                Grouping = this.groupBy.Convert()
            };
            
            var dataToUpdate = findNullJoin(data);
            dataToUpdate.JoinData = new JoinData();
            
            dataToUpdate.JoinData.JoinType = this.jointype;
            dataToUpdate.JoinData.SelectData = thisData;
            dataToUpdate.JoinData.OnData = this.columnJoins.Select(x => Tuple.Create(x.Item1, x.Item2, OperatorType.IdentityEquality, x.Item3, x.Item4)).ToArray();
            
            return data;
        }
    }
}
