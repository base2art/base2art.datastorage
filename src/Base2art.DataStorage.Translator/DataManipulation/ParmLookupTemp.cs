﻿namespace Base2art.DataStorage.DataManipulation
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Linq.Expressions;
    using Base2art.DataStorage.DataManipulation.Data;
    using Base2art.DataStorage.Utils;

    public static class ParmLookupTemp
    {
        
        public static Tuple<int, Column, int, Column>[] GetJoins(
            this IDataGetter<SelectData> getter,
            LambdaExpression joinSetup,
            BinaryExpression bin)
        {
            List<BinaryExpression> exprs = new List<BinaryExpression>();
            AppendExpressions(bin, exprs);
            return exprs.Select(x =>
                                {
                                    var tables = GetTableNames(joinSetup.Parameters, x);
                                    
                                    var leftMi = x.Left.GetMemberInfo(tables.Item2);
                                    var rightMi = x.Right.GetMemberInfo(tables.Item4);
                                    return Tuple.Create(tables.Item1, leftMi, tables.Item3, rightMi);
                                })
                .ToArray();
        }
        
        private static Tuple<int, string, int, string> GetTableNames(
            ReadOnlyCollection<ParameterExpression> joinSetupParameters,
            BinaryExpression bin)
        {
            var parmNames = joinSetupParameters.Select(x => x.Name).ToArray();
            var parmTypes = joinSetupParameters.Select(x => x.Type.TableName()).ToArray();
            
            //            var left = joinSetupParameters[0].Name;
            //            var center = joinSetupParameters[1].Name;
            //            var right = joinSetupParameters[2].Name;
            //            var far_right = joinSetupParameters[3].Name;
            
            int i1 = -1;
            int i2 = -1;
            string l1 = null;
            string l2 = null;
            
            for (int i = 0; i < parmNames.Length; i++)
            {
                if (bin.Left.IsParameterUsed(parmNames[i]))
                {
                    l1 = parmTypes[i];
                    i1 = i;
                }
                
                
                if (bin.Right.IsParameterUsed(parmNames[i]))
                {
                    l2 = parmTypes[i];
                    i2 = i;
                }
            }
            
            if (string.IsNullOrWhiteSpace(l1) || string.IsNullOrWhiteSpace(l2))
            {
                throw new InvalidOperationException("must use table as parameter id");
            }
            
            return Tuple.Create(i1, l1, i2, l2);
        }

        private static void AppendExpressions(BinaryExpression bin, List<BinaryExpression> exprs)
        {
            if (bin.NodeType == ExpressionType.AndAlso)
            {
                AppendExpressions(bin.Left as BinaryExpression, exprs);
                AppendExpressions(bin.Right as BinaryExpression, exprs);
            }
            else
            {
                exprs.Add(bin);
            }
        }
    }
}
