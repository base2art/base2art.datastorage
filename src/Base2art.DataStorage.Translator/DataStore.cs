﻿
namespace Base2art.DataStorage
{
    using System;
    using Base2art.DataStorage.DataManipulation;
    
    public class DataStore : IDataStore
    {
        private readonly IDataManipulator manipulator;

        public DataStore(IDataManipulator manipulator)
        {
            this.manipulator = manipulator;
        }

        public IQueryInsert<T> Insert<T>()
        {
            return new Inserter<T>(this.manipulator);
        }

        public IQuerySingleSelect<T> SelectSingle<T>()
        {
            return new SingleSelector1<T>(this.manipulator);
        }

        public IQuerySelect<T> Select<T>()
        {
            return new QuerySelector1<T>(this.manipulator);
        }

        public IQueryDelete<T> Delete<T>()
        {
            return new Deleter<T>(this.manipulator);
        }

        public IQueryUpdate<T> Update<T>()
        {
            return new Updater<T>(this.manipulator);
        }
    }
}


