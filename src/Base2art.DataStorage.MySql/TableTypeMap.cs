﻿namespace Base2art.DataStorage.MySql
{
    using System;
    using Base2art.DataStorage.DataDefinition;

    public class TableTypeMap : ITypeMap
    {
        public  string StringType(int? min, int? max)
        {
            if (!max.HasValue)
            {
                return "TEXT";
            }
            
            if (min.GetValueOrDefault(-1) == max.Value)
            {
                return "CHAR(" + (max) + ")";
            }
            
            return "VARCHAR(" + (max) + ")";
        }

        public string ObjectType(int? min, int? max)
        {
            return "TEXT";
        }

        public string BinaryType(int? min, int? max)
        {
            if (!max.HasValue)
            {
                return "BLOB";
            }
            
            return "VARBINARY(" + (max) + ")";
        }

        public string XmlType()
        {
            return "TEXT";
        }

        public string ShortType()
        {
            return "SMALLINT";
        }

        public string TimeSpanType()
        {
            return "TIME";
        }

        public string LongType()
        {
            return "BIGINT";
        }

        public string IntType()
        {
            return "INT";
        }

        public string GuidType()
        {
            return "BINARY(16)";
        }

        public string FloatType()
        {
            return "DOUBLE";
        }

        public string DoubleType()
        {
            return "DOUBLE";
        }

        public string DecimalType()
        {
            return "DECIMAL";
        }

        public string DateTimeOffsetType()
        {
            return "DATETIME";
        }

        public string DateTimeType()
        {
            return "DATETIME";
        }

        public string BooleanType()
        {
            return "BIT";
        }
    }
}
