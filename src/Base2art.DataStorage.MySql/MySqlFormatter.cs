﻿namespace Base2art.DataStorage.MySql
{
    using System;
    using Base2art.DataStorage.DataDefinition;
    using Base2art.DataStorage.DataManipulation;
    
    public class MySqlFormatter : FormatterBase
    {
        private readonly bool isDebug;

        public MySqlFormatter(IExecutionEngine engine, bool isDebug, TimeSpan defaultTimeout) 
            : base(engine, null, isDebug, new BuilderMaps(), new DefaultStorageTypeMap(new TableTypeMap()), defaultTimeout)
        {
            this.isDebug = isDebug;
        }
        
        protected override string ExceptionTypeName
        {
            get { return "MySql.Data.MySqlClient.MySqlException"; }
        }
        
        protected override DeleteBuilder CreateDeleteBuilder(SharedBuilderMaps commonBuilder)
        {
            return new DeleteBuilder(commonBuilder);
        }
        
        protected override InsertSelectBuilder CreateInsertSelectBuilder(SharedBuilderMaps commonBuilder)
        {
            return new InsertSelectBuilder(commonBuilder);
        }
        
        protected override InsertRecordsBuilder CreateInsertRecordsBuilder(SharedBuilderMaps commonBuilder)
        {
            return new InsertRecordsBuilder(commonBuilder);
        }

        protected override SelectBuilder CreateSelectBuilder(SharedBuilderMaps commonBuilder)
        {
            return new SelectBuilder(commonBuilder);
        }

        protected override UpdateBuilder CreateUpdateBuilder(SharedBuilderMaps commonBuilder)
        {
            return new UpdateBuilder(commonBuilder);
        }
        
        protected override CreateTableBuilder CreateTableBuilder(IStorageTypeMap commonBuilder)
        {
            return new CreateTableBuilder(commonBuilder, this.EscapeCharacters);
        }
        
        protected override DropTableBuilder CreateDropTableBuilder()
        {
            return new DropTableBuilder(this.EscapeCharacters);
        }
        
        protected override CreateTableAltererBuilder CreateTableAltererBuilder(IStorageTypeMap commonBuilder)
        {
            return new CreateTableAltererBuilder(commonBuilder, this.EscapeCharacters);
        }
        
        protected override CreateTableIndexesBuilder CreateTableIndexesBuilder(IStorageTypeMap commonBuilder)
        {
            return new CreateTableIndexesBuilder(commonBuilder, this.EscapeCharacters);
        }
        
        protected override CreateTableKeysBuilder CreateTableKeysBuilder(IStorageTypeMap commonBuilder)
        {
            return new CreateTableKeysBuilder(commonBuilder, this.EscapeCharacters);
        }
    }
}
