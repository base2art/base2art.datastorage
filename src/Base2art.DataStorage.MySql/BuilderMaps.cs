﻿namespace Base2art.DataStorage.MySql
{
    using System;
    using Base2art.DataStorage.DataManipulation;
    
    public class BuilderMaps : SharedBuilderMaps
    {
        public override string OpeningEscapeSequence
        {
            get { return "`"; }
        }
        
        public override string ClosingEscapeSequence
        {
            get { return "`"; }
        }
        
        protected override bool SuportsSqlConcatAsAddition
        {
            get { return false; }
        }
        
        public override SelectBuilder CreateSelectBuilder()
        {
            return new SelectBuilder(this);
        }

        public static Guid FromMySql(byte[] bytearray)
        {
            var ba = new byte[16];
            ba[3] = bytearray[0];
            ba[2] = bytearray[1];
            ba[1] = bytearray[2];
            ba[0] = bytearray[3];
            
            ba[5] = bytearray[4];
            ba[4] = bytearray[5];
            
            ba[7] = bytearray[6];
            ba[6] = bytearray[7];
            
            ba[8] = bytearray[8];
            ba[9] = bytearray[9];
            
            ba[10] = bytearray[10];
            ba[11] = bytearray[11];
            ba[12] = bytearray[12];
            ba[13] = bytearray[13];
            ba[14] = bytearray[14];
            ba[15] = bytearray[15];
            
            return new Guid(ba);
        }
        
        public static byte[] ToMySql(Guid value)
        {
            var bytearray = value.ToByteArray();
            var ba = new byte[16];
            ba[3] = bytearray[0];
            ba[2] = bytearray[1];
            ba[1] = bytearray[2];
            ba[0] = bytearray[3];
            
            ba[5] = bytearray[4];
            ba[4] = bytearray[5];
            
            ba[7] = bytearray[6];
            ba[6] = bytearray[7];
            
            ba[8] = bytearray[8];
            ba[9] = bytearray[9];
            
            ba[10] = bytearray[10];
            ba[11] = bytearray[11];
            ba[12] = bytearray[12];
            ba[13] = bytearray[13];
            ba[14] = bytearray[14];
            ba[15] = bytearray[15];
            
            return ba;
        }
        
        protected override object MapGuid(Guid value)
        {
            // "" + new Guid().ToString("N") + ""
            return ToMySql(value);
        }
        
        protected override Guid MapFromNonNullGuid(object value)
        {
            return FromMySql((byte[])value);
        }
        
        protected override object MapDateTime(DateTime value)
        {
            return value.ToString("yyyyMMddHHmmss");
        }
        
        protected override TimeSpan MapFromNonNullTimeSpan(object value)
        {
             var result = base.MapFromNonNullTimeSpan(value);
            return result;
        }
        
        protected override object MapTimeSpan(TimeSpan value)
        {
            var result = string.Format("{0:hh}:{0:mm}:{0:ss}", value);
            return result;
        }
        
        public override string NewUUIDSyntax()
        {
            return "unhex(REPLACE(UUID(),'-',''))";
        }
        
        protected override object MapObjectToSql(System.Collections.Generic.Dictionary<string, object> value)
        {
            return base.MapObjectToSql(value);
        }
        
        protected override System.Collections.Generic.Dictionary<string, object> MapFromNonNullObject(object value)
        {
            return base.MapFromNonNullObject(value);
        }
    }
}
