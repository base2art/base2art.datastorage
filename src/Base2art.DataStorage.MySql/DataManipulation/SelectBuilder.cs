﻿namespace Base2art.DataStorage.MySql.DataManipulation
{
    using Base2art.DataStorage.DataManipulation;
    
    public class SelectBuilder : Base2art.DataStorage.DataManipulation.SelectBuilder
    {
        public SelectBuilder(SharedBuilderMaps commonBuilder) : base(commonBuilder)
        {
        }
        
        protected override string TableLevelNoLockHint
        {
            get { return ""; }
        }
    }
}
