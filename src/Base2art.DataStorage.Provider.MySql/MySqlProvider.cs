﻿namespace Base2art.DataStorage.Provider.MySql
{
    using System;
    using Base2art.Dapper;
    using Base2art.DataStorage.Dapper;
    using Base2art.DataStorage.MySql;
    
    public class MySqlProvider : IDataStorageProvider
    {
        private readonly bool isDebug;

        private readonly TimeSpan defaultCommandTimeout;
        
        public MySqlProvider(bool isDebug, TimeSpan defaultCommandTimeout)
        {
            this.defaultCommandTimeout = defaultCommandTimeout;
            this.isDebug = isDebug;
        }
        
        public IDataStore CreateDataStoreAccess(NamedConnectionString named)
        {
            return new DataStore(this.Backing(named));
        }
        
        public IDbms CreateDbmsAccess(NamedConnectionString named)
        {
            return new Dbms(this.Backing(named));
        }

        private MySqlFormatter Backing(NamedConnectionString named)
        {
            var cstr = named.ConnectionString;
            
            var factory = new OdbcConnectionFactory<global::MySql.Data.MySqlClient.MySqlConnection>(cstr);
            return new MySqlFormatter(new DapperExecutionEngine(factory), this.isDebug, this.defaultCommandTimeout);
        }
    }
}