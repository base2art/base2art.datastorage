﻿
using System;
using System.Collections.Generic;
using Base2art.DataStorage.InMemory;

namespace Base2art.DataStorage.Provider.InMemory
{
    public class InMemoryProvider : IDataStorageProvider
    {
        public IDataStore CreateDataStoreAccess(NamedConnectionString named)
        {
            return new DataStore(Backing(named));
        }
        
        public IDbms CreateDbmsAccess(NamedConnectionString named)
        {
            return new Dbms(Backing(named));
        }

        private static DataDefiner Backing(NamedConnectionString named)
        {
            return new DataDefiner(true);
        }
    }
}