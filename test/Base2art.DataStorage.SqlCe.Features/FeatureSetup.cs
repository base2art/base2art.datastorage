﻿namespace Base2art.DataStorage.SqlCe.Features
{
    using System;
    using System.Data.SqlServerCe;
    using System.IO;
    using System.Net;
    using System.Net.Sockets;
    using Base2art.Dapper;
    using Base2art.DataStorage.Dapper;
    
    public static class FeatureSetup
    {
        private static readonly Lazy<string> ipAddress;
        
        static FeatureSetup()
        {
            ipAddress = new Lazy<string>(GetLocalIPAddress);
        }

        public static SqlCeFormatter Definer()
        {
            var cstr = ConnectionString();
            SqlCeEngine engine = new SqlCeEngine(cstr);
            engine.CreateDatabase();
            var factory = new OdbcConnectionStringFactory(cstr, "System.Data.SqlServerCe.4.0");
            
            return new SqlCeFormatter(new DapperExecutionEngine(factory), true);
        }
        
        public static string ConnectionString()
        {
            var builder = new SqlCeConnectionStringBuilder();
            builder.Password = "nunitUser!";
            builder.DataSource = "Nunit.sdf";
            
            if (File.Exists(builder.DataSource))
            {
                File.Delete(builder.DataSource);
            }
            
            return builder.ToString();
        }

        public static string IPAddress()
        {
            return ipAddress.Value;
        }
        
        private static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            
            throw new Exception("Local IP Address Not Found!");
        }
    }
}
