﻿
namespace Base2art.DataStorage.SqlCe.Features
{
    using System;
    using System.Data.SqlServerCe;
    using Base2art.DataStorage.Specs;
    using NUnit.Framework;

    [TestFixture]
    public class CreateTableFeature
    {
        private SqlCeFormatter definer;

        private CreateTableSpec selecting;

        [SetUp]
        public void BeforEach()
        {
            this.definer = FeatureSetup.Definer();
            this.selecting = new CreateTableSpec(typeof(SqlCeException), canDropTables: false);
        }

        [Test]
        public async void ShouldCreateTable()
        {
            IDataStore db = new DataStore(definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.ShouldCreateTable(dbms, db);
        }

        [Test]
        public async void ShouldBeAbleToDeclareFields()
        {
            IDataStore db = new DataStore(definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.ShouldBeAbleToDeclareFields(dbms, db);
        }

        [Test]
        public async void ShouldBeAbleToInferFields()
        {
            IDataStore db = new DataStore(definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.ShouldBeAbleToInferFields(dbms, db);
        }

        [Test]
        public async void ShouldBeAbleToInferRequiredFields()
        {
            IDataStore db = new DataStore(definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.ShouldBeAbleToInferRequiredFields(dbms, db);
        }

        [Test]
        public async void ShouldBeAbleToUpgradeTables()
        {
            IDataStore db = new DataStore(definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.ShouldBeAbleToUpgradeTables(dbms, db);
        }

        [Test]
        public async void ShouldBeAbleToAddKeyes()
        {
            IDataStore db = new DataStore(definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.ShouldBeAbleToAddKeyes(dbms, db);
        }

        [Test]
        public async void ShouldBeAbleToAddIndexes()
        {
            IDataStore db = new DataStore(definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.ShouldBeAbleToAddIndexes(dbms, db);
        }
    }
}



