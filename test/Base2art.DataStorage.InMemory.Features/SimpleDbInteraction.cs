﻿
namespace Base2art.DataStorage.InMemory.Features
{
    using System;
    using Base2art.DataStorage.Specs;
    using NUnit.Framework;
    
    [TestFixture]
    public class SimpleDbInteraction
    {
        private DataDefiner definer;

        private SimpleDbInteraction_Selecting selecting;

        private SimpleDbInteraction_Inserting inserting;
        
        [SetUp]
        public void BeforEach()
        {
            this.definer = new InMemory.DataDefiner(true);
            this.selecting = new SimpleDbInteraction_Selecting();
            this.inserting = new SimpleDbInteraction_Inserting();
        }
        
        [Test]
        public async void Should_FetchDistinct()
        {
            IDataStore db = new DataStore(definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_FetchDistinct(dbms, db);
        }
        
        [Test]
        public async void Should_Join_CrossJoin()
        {
            IDataStore db = new DataStore(definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_Join_CrossJoin(dbms, db);
        }
        
        [Test]
        public async void Should_Join_InnerJoin()
        {
            IDataStore db = new DataStore(definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_Join_InnerJoin(dbms, db);
        }
        
        [Test]
        public async void Should_Join_LeftJoin()
        {
            IDataStore db = new DataStore(definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_Join_LeftJoin(dbms, db);
        }
        
        [Test]
        public async void Should_Join_LeftJoin_NonNullableWhere()
        {
            IDataStore db = new DataStore(definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_Join_LeftJoin_NonNullableWhere(dbms, db);
        }
        
        [Test]
        public async void Should_Join_LeftJoin2Times()
        {
            IDataStore db = new DataStore(definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_Join_LeftJoin2Times(dbms, db);
        }
        
        
        [Test]
        public async void Should_Join_RightJoin()
        {
            IDataStore db = new DataStore(definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_Join_RightJoin(dbms, db);
        }
        
        [Test]
        public async void Should_LimitAndOffsetResults()
        {
            IDataStore db = new DataStore(definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_LimitAndOffsetResults(dbms, db);
        }
        
        [Test]
        public async void Should_OrderResults()
        {
            IDataStore db = new DataStore(definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_OrderResults(dbms, db);
        }
        
        [Test]
        public async void Should_GroupResults()
        {
            IDataStore db = new DataStore(definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_GroupResults(dbms, db);
        }
        
        [Test]
        public async void Should_CountResults()
        {
            IDataStore db = new DataStore(definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_CountResults(dbms, db);
        }
        
        [Test]
        public async void Should_ScopeToRequestedFields()
        {
            IDataStore db = new DataStore(definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_ScopeToRequestedFields(dbms, db);
        }
        
        [Test]
        public async void Should_Select_WithFiltering()
        {
            IDataStore db = new DataStore(definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_Select_WithFiltering(dbms, db);
        }
        
        [Test]
        public async void Should_Select_WithFilteringMultipleWheres()
        {
            IDataStore db = new DataStore(definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_Select_WithFilteringMultipleWheres(dbms, db);
        }
        
        [Test]
        public async void Should_SimpleSelect()
        {
            IDataStore db = new DataStore(definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_SimpleSelect(dbms, db);
        }
        
        [Test]
        public async void Should_SelectSingleField()
        {
            IDataStore db = new DataStore(definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_SelectSingleField(dbms, db);
        }
        
        [Test]
        public async void Should_SelectNullTernaryFilter()
        {
            IDataStore db = new DataStore(definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_SelectNullTernaryFilter(dbms, db);
        }
        
        [Test]
        public async void Should_InsertIntoFromSelect()
        {
            IDataStore db = new DataStore(definer);
            IDbms dbms = new Dbms(this.definer);
            await this.inserting.Should_InsertIntoFromSelect(dbms, db);
        }
        
        [Test]
        public async void Should_InsertMultiple()
        {
            IDataStore db = new DataStore(definer);
            IDbms dbms = new Dbms(this.definer);
            await this.inserting.Should_InsertMultiple(dbms, db);
        }
        
        [Test]
        public async void Should_SelectFieldInQuery()
        {
            IDataStore db = new DataStore(definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_SelectFieldInQuery(dbms, db);
        }
        
        [Test]
        public async void Should_SelectFieldIn()
        {
            IDataStore db = new DataStore(definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_SelectFieldIn(dbms, db);
        }
        
        [Test]
        public async void Should_SelectMultpleTables()
        {
            IDataStore db = new DataStore(definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_SelectMultpleTables(dbms, db);
        }
        
        [Test]
        public async void Should_Join_2Times()
        {
            IDataStore db = new DataStore(definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_Join_2Times(dbms, db);
        }
        
        [Test]
        public async void Should_Join_2Ons()
        {
            IDataStore db = new DataStore(definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_Join_2Ons(dbms, db);
        }
        
        [Test]
        public async void Should_OrderRandomly()
        {
            IDataStore db = new DataStore(definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_OrderRandomly(dbms, db);
        }
        
        [Test]
        public async void Should_EscapeTableNames()
        {
            IDataStore db = new DataStore(definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_EscapeTableNames(dbms, db);
        }
        
        [Test]
        public async void Should_UpdateData()
        {
            IDataStore db = new DataStore(definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_UpdateData(dbms, db);
        }
        
//        [Test]
//        public async void Should_IsNullReturn()
//        {
//            IDataStore db = new DataStore(definer);
//            IDbms dbms = new Dbms(this.definer);
//            await this.selecting.Should_IsNullReturn(dbms, db);
//        }
    }
}
