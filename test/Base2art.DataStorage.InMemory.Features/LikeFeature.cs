﻿
using System;
using FluentAssertions;
using NUnit.Framework;

namespace Base2art.DataStorage.InMemory.Features
{
    public class LikeFeature
    {
        
        [TestCase(true, "%", "")]
        [TestCase(true, "%", " ")]
        [TestCase(true, "%", "asdfa asdf asdf")]
        [TestCase(true, "%", "%")]
        [TestCase(false, "_", "")]
        [TestCase(true, "_", " ")]
        [TestCase(true, "_", "4")]
        [TestCase(true, "_", "C")]
        [TestCase(false, "_", "CX")]
        [TestCase(false, "[ABCD]", "")]
        [TestCase(true, "[ABCD]", "A")]
        [TestCase(true, "[ABCD]", "b")]
        [TestCase(false, "[ABCD]", "X")]
        [TestCase(false, "[ABCD]", "AB")]
        [TestCase(true, "[B-D]", "C")]
        [TestCase(true, "[B-D]", "D")]
        [TestCase(false, "[B-D]", "A")]
        [TestCase(false, "[^B-D]", "C")]
        [TestCase(false, "[^B-D]", "D")]
        [TestCase(true, "[^B-D]", "A")]
        [TestCase(true, "%TEST[ABCD]XXX", "lolTESTBXXX")]
        [TestCase(false, "%TEST[ABCD]XXX", "lolTESTZXXX")]
        [TestCase(false, "%TEST[^ABCD]XXX", "lolTESTBXXX")]
        [TestCase(true, "%TEST[^ABCD]XXX", "lolTESTZXXX")]
        [TestCase(true, "%TEST[B-D]XXX", "lolTESTBXXX")]
        [TestCase(true, "%TEST[^B-D]XXX", "lolTESTZXXX")]
        [TestCase(true, "%Stuff.txt", "Stuff.txt")]
        [TestCase(true, "%Stuff.txt", "MagicStuff.txt")]
        [TestCase(false, "%Stuff.txt", "MagicStuff.txt.img")]
        [TestCase(false, "%Stuff.txt", "Stuff.txt.img")]
        [TestCase(false, "%Stuff.txt", "MagicStuff001.txt.img")]
        [TestCase(true, "Stuff.txt%", "Stuff.txt")]
        [TestCase(false, "Stuff.txt%", "MagicStuff.txt")]
        [TestCase(false, "Stuff.txt%", "MagicStuff.txt.img")]
        [TestCase(true, "Stuff.txt%", "Stuff.txt.img")]
        [TestCase(false, "Stuff.txt%", "MagicStuff001.txt.img")]
        [TestCase(true, "%Stuff.txt%", "Stuff.txt")]
        [TestCase(true, "%Stuff.txt%", "MagicStuff.txt")]
        [TestCase(true, "%Stuff.txt%", "MagicStuff.txt.img")]
        [TestCase(true, "%Stuff.txt%", "Stuff.txt.img")]
        [TestCase(false, "%Stuff.txt%", "MagicStuff001.txt.img")]
        [TestCase(true, "%Stuff%.txt", "Stuff.txt")]
        [TestCase(true, "%Stuff%.txt", "MagicStuff.txt")]
        [TestCase(false, "%Stuff%.txt", "MagicStuff.txt.img")]
        [TestCase(false, "%Stuff%.txt", "Stuff.txt.img")]
        [TestCase(false, "%Stuff%.txt", "MagicStuff001.txt.img")]
        [TestCase(true, "%Stuff%.txt", "MagicStuff001.txt")]
        [TestCase(true, "Stuff%.txt%", "Stuff.txt")]
        [TestCase(false, "Stuff%.txt%", "MagicStuff.txt")]
        [TestCase(false, "Stuff%.txt%", "MagicStuff.txt.img")]
        [TestCase(true, "Stuff%.txt%", "Stuff.txt.img")]
        [TestCase(false, "Stuff%.txt%", "MagicStuff001.txt.img")]
        [TestCase(false, "Stuff%.txt%", "MagicStuff001.txt")]
        [TestCase(true, "%Stuff%.txt%", "Stuff.txt")]
        [TestCase(true, "%Stuff%.txt%", "MagicStuff.txt")]
        [TestCase(true, "%Stuff%.txt%", "MagicStuff.txt.img")]
        [TestCase(true, "%Stuff%.txt%", "Stuff.txt.img")]
        [TestCase(true, "%Stuff%.txt%", "MagicStuff001.txt.img")]
        [TestCase(true, "%Stuff%.txt%", "MagicStuff001.txt")]
        [TestCase(true, "_Stuff_.txt_", "1Stuff3.txt4")]
        [TestCase(false, "_Stuff_.txt_", "1Stuff.txt4")]
        [TestCase(false, "_Stuff_.txt_", "1Stuff3.txt")]
        [TestCase(false, "_Stuff_.txt_", "Stuff3.txt4")]
        public void ShouldLike(bool result, string pattern, string testString)
        {
            pattern.SqlLike(testString).Should().Be(result);
            
        }
    }
}
