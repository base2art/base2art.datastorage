﻿
namespace Base2art.DataStorage
{
    using System;
    using System.ComponentModel;
    using System.Threading.Tasks;
    using Base2art.DataStorage.Data;
    using Base2art.DataStorage.DataDefinition;
    using Base2art.DataStorage.DataManipulation;
    using Base2art.Tasks;
    using FluentAssertions;
    
    public class SimpleDbInteraction_Inserting
    {
        private readonly TimeSpan? delay;

        private readonly bool canDropTables;
        
        public SimpleDbInteraction_Inserting(TimeSpan? delay = null, bool canDropTables = true)
        {
            this.delay = delay;
            this.canDropTables = canDropTables;
        }
        
        public bool CanDropTables
        {
            get { return this.canDropTables; }
        }
        
        public void BeforEach()
        {
        }
        
        public async Task Should_InsertIntoFromSelect(IDbms dbms, IDataStore db)
        {
            if (this.CanDropTables)
            {
                await dbms.DropTable<person>().IfExists().Execute();
            }
            
            await dbms.CreateTable<person>()
                .Fields(r =>
                        r.Field(x => x.name, false, Range.Create(0, 250))
                        .Field(x => x.social_security_number, false, Range.Create(11, 11))
                        .Field(x => x.living_parent_count)
                        .Field(x => x.when))
                .Execute();
            
            await db.Insert<person>()
                .Record(r =>
                        r.Field(x => x.name, "Scott")
                        .Field(x => x.social_security_number, "123-14-2135")
                        .Field(x => x.living_parent_count, 2)
                        .Field(x => x.when, new DateTime(2017, 02, 03)))
                .Execute();
            
            await this.Delay();
            
            await db.Insert<person>()
                .Fields(r =>
                        r.Field(x => x.name)
                        .Field(x => x.social_security_number)
                        .Field(x => x.when)
                        .Field(x => x.living_parent_count))
                .Records(
                    db.Select<person>()
                    .Fields(r =>
                            r.Field(x => x.social_security_number)
                            .Field(x => x.social_security_number)
                            .Field(x => x.when)
                            .Field(x => x.living_parent_count - 1)))
                .Execute();
            
            
            await this.Delay();
            
            var derived = await db.Select<person>()
                .Where(r => r.Field(x => x.name, "%-2135", Operations.Like))
                .Execute()
                .Then()
                .ToArray();
            
            derived.Length.Should().Be(1);
            derived[0].name.Should().Be("123-14-2135");
            derived[0].social_security_number.Should().Be("123-14-2135");
            derived[0].living_parent_count.Should().Be(1);
            derived[0].when.Should().Be(new DateTime(2017,02,03));
        }
        
        public async Task Should_InsertMultiple(IDbms dbms, IDataStore db)
        {
            if (this.CanDropTables)
            {
                await dbms.DropTable<person>().IfExists().Execute();
            }
            
            await dbms.CreateTable<person>()
                .Fields(r =>
                        r.Field(x => x.name, false, Range.Create(0, 250))
                        .Field(x => x.social_security_number, false, Range.Create(11, 11))
                        .Field(x => x.living_parent_count))
                .Execute();
            
            await db.Insert<person>()
                .Record(r =>
                        r.Field(x => x.name, "Scott")
                        .Field(x => x.social_security_number, "123-14-2135")
                        .Field(x => x.living_parent_count, 2))
                .Record(r =>
                        r.Field(x => x.name, "Matt")
                        .Field(x => x.social_security_number, "456-14-7892")
                        .Field(x => x.living_parent_count, 3))
                .Record(r =>
                        r.Field(x => x.name, "Chyna")
                        .Field(x => x.social_security_number, "938-28-2672")
                        .Field(x => x.living_parent_count, 1))
                .Execute();
            
            
            
            await this.Delay();
            
            var derived = await db.Select<person>()
                .Where(r => r.Field(x => x.social_security_number, "%-14-%", Operations.Like))
                .Execute()
                .Then()
                .ToArray();
            
            derived.Length.Should().Be(2);
            derived[0].name.Should().Be("Scott");
            derived[0].social_security_number.Should().Be("123-14-2135");
            derived[0].living_parent_count.Should().Be(2);
            
            derived[1].name.Should().Be("Matt");
            derived[1].social_security_number.Should().Be("456-14-7892");
            derived[1].living_parent_count.Should().Be(3);
        }

        private Task Delay()
        {
            return this.delay.HasValue ? Task.Delay(this.delay.Value) : Task.FromResult(true);
        }
    }
}

