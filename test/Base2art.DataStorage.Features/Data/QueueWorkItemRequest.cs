﻿
using System;

namespace Base2art.DataStorage.Data
{
    public class QueueWorkItemRequest
    {
        private readonly string applicationKey;

        private readonly string workItemType;

        private readonly string workItemData;

        private readonly string requestedBy;

        private readonly DateTimeOffset requestedAt;

        private readonly int retryCount;

        private readonly TimeSpan timeout;
        
        public QueueWorkItemRequest(
            string applicationKey,
            string workItemType,
            string workItemData,
            string requestedBy,
            DateTimeOffset requestedAt,
            int retryCount,
            TimeSpan timeout)
        {
            this.timeout = timeout;
            this.applicationKey = applicationKey;
            this.requestedBy = requestedBy;
            this.requestedAt = requestedAt;
            this.retryCount = retryCount;
            this.workItemType = workItemType;
            this.workItemData = workItemData;
        }

        public string ApplicationKey
        {
            get { return this.applicationKey; }
        }

        public string RequestedBy
        {
            get { return this.requestedBy; }
        }

        public DateTimeOffset RequestedAt
        {
            get { return this.requestedAt; }
        }

        public int RetryCount
        {
            get { return this.retryCount; }
        }

        public TimeSpan Timeout
        {
            get { return this.timeout; }
        }

        public string WorkItemType
        {
            get { return this.workItemType; }
        }

        public string WorkItemData
        {
            get { return this.workItemData; }
        }
    }
}
