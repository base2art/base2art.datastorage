﻿
namespace Base2art.DataStorage.SqlServer.Features
{
    using System;
    using Base2art.DataStorage.Specs;
    using NUnit.Framework;

    [TestFixture]
    public class AllDataTypeInteractionInterfaceFeature
    {
        private SqlFormatter definer;

        private AllDataTypeInteractionInterfaceSpec selecting;

        [SetUp]
        public void BeforEach()
        {
            this.definer = FeatureSetup.Definer();
            this.selecting = new AllDataTypeInteractionInterfaceSpec(canDropTables: true);
        }

        [Test]
        public async void CreateInsertSelectDelete()
        {
            IDataStore db = new DataStore(definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.CreateInsertSelectDelete(dbms, db);
        }

        [Test]
        public async void CreateAllGenerically()
        {
            IDataStore db = new DataStore(definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.CreateAllGenerically(dbms, db);
        }
    }
}

