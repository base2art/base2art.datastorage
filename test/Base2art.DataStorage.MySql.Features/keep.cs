﻿

/*
 
        [Test]
        public async void ShouldCreateInsertSelect()
        {
            IDbms dbms = new Dbms(definer);
            
            await dbms.CreateTable<person>()
                .Fields(r =>
                        r.Field(x => x.name)
                        .Field(x => x.social_security_number))
                .Execute();
            
            IDataStore db = new DataStore(definer);
            await db.Insert<person>()
                .Record(r =>
                        r.Field(x => x.name, "Scott")
                        .Field(x => x.social_security_number, "123-14-2135"))
                .Execute();
            
            await db.Insert<person>()
                .Record(r =>
                        r.Field(x => x.name, "Matt")
                        .Field(x => x.social_security_number, "123-34-2135"))
                .Execute();
            
            var matts = await db.Select<person>()
                .Where(r => r.Field(x => x.name, "Matt", (x, y) => x == y))
                .Execute()
                .Then()
                .ToArray();
            
            matts.Length.Should().Be(1);
            matts[0].name.Should().Be("Matt");
            
            var scott = await db.Select<person>()
                .Where(r => r.Field(x => x.name, "Scott", (x, y) => x == y))
                .Execute()
                .Then()
                .ToArray();
            
            scott.Length.Should().Be(1);
            scott[0].name.Should().Be("Scott");
            
            
            var notMatts = await db.Select<person>()
                .Where(r => r.Field(x => x.name, "Matt", (x, y) => x != y))
                .Execute()
                .Then()
                .ToArray();
            
            notMatts.Length.Should().Be(1);
            notMatts[0].name.Should().Be("Scott");
            
            
            var ssnLike = await db.Select<person>()
                .Where(r => r.Field(x => x.social_security_number, "%-14-%", Operations.Like))
                .Execute()
                .Then()
                .ToArray();
            
            ssnLike.Length.Should().Be(1);
            ssnLike[0].name.Should().Be("Scott");
            
            
            var ssnNotLike = await db.Select<person>()
                .Where(r => r.Field(x => x.social_security_number, "%-14-%", Operations.NotLike))
                .Execute()
                .Then()
                .ToArray();
            
            ssnNotLike.Length.Should().Be(1);
            ssnNotLike[0].name.Should().Be("Matt");
        }
        
        [Test]
        public async void ShouldCreateInsertSelect_Filtering()
        {
            IDbms dbms = new Dbms(this.definer);
            
            await dbms.CreateTable<person>()
                .Fields(r =>
                        r.Field(x => x.name)
                        .Field(x => x.social_security_number)
                        .Field(x => x.living_parent_count))
                .Execute();
            
            IDataStore db = new DataStore(this.definer);
            await db.Insert<person>()
                .Record(r =>
                        r.Field(x => x.name, "Todd")
                        .Field(x => x.living_parent_count, 0)
                        .Field(x => x.social_security_number, "123-14-2135"))
                .Execute();
            
            await db.Insert<person>()
                .Record(r =>
                        r.Field(x => x.name, "Scott")
                        .Field(x => x.living_parent_count, 1)
                        .Field(x => x.social_security_number, "123-14-2135"))
                .Execute();
            
            await db.Insert<person>()
                .Record(r =>
                        r.Field(x => x.name, "Matt")
                        .Field(x => x.living_parent_count, 2)
                        .Field(x => x.social_security_number, "123-34-2135"))
                .Execute();
            
            var greaterThan = await db.Select<person>()
                .Where(r => r.Field(x => x.living_parent_count, x => x > 1))
                .Execute()
                .Then()
                .ToArray();
            
            greaterThan.Length.Should().Be(1);
            greaterThan[0].name.Should().Be("Matt");
            
            var greaterThanEqual = await db.Select<person>()
                .Where(r => r.Field(x => x.living_parent_count, x => x >= 1))
                .Execute()
                .Then()
                .ToArray();
            
            greaterThanEqual.Length.Should().Be(2);
            greaterThanEqual[1].name.Should().Be("Matt");
            greaterThanEqual[0].name.Should().Be("Scott");
            
            var lessThanEqual = await db.Select<person>()
                .Where(r => r.Field(x => x.living_parent_count, x => x <= 1))
                .Execute()
                .Then()
                .ToArray();
            
            lessThanEqual.Length.Should().Be(2);
            lessThanEqual[0].name.Should().Be("Todd");
            lessThanEqual[1].name.Should().Be("Scott");
            
            var lessThan = await db.Select<person>()
                .Where(r => r.Field(x => x.living_parent_count, x => x < 1))
                .Execute()
                .Then()
                .ToArray();
            
            lessThan.Length.Should().Be(1);
            lessThan[0].name.Should().Be("Todd");
        }
        
        [Test]
        public async void ShouldBeAbleToDeclareFields()
        {
            IDbms dbms = new Dbms(this.definer);
            
            await dbms.CreateTable<person>()
                .Fields(r =>
                        r.Field(x => x.name)
                        .Field(x => x.social_security_number)
                        .Field(x => x.living_parent_count))
                .Execute();
            
            IDataStore db = new DataStore(this.definer);
            await db.Insert<person>()
                .Record(r =>
                        r.Field(x => x.name, "Todd")
                        .Field(x => x.living_parent_count, 0)
                        .Field(x => x.social_security_number, "123-14-2112"))
                .Execute();
            
            await db.Insert<person>()
                .Record(r =>
                        r.Field(x => x.name, "Scott")
                        .Field(x => x.living_parent_count, 1)
                        .Field(x => x.social_security_number, "123-14-1235"))
                .Execute();
            
            await db.Insert<person>()
                .Record(r =>
                        r.Field(x => x.name, "Matt")
                        .Field(x => x.living_parent_count, 2)
                        .Field(x => x.social_security_number, "123-34-2313"))
                .Execute();
            
            await db.Insert<person>()
                .Record(r =>
                        r.Field(x => x.name, "Matt")
                        .Field(x => x.living_parent_count, 3)
                        .Field(x => x.social_security_number, "123-34-2135"))
                .Execute();
            
            var data = await db.Select<person>()
                .Fields(r => r.Field(x => x.name))
                .Where(r => r.Field(x => x.living_parent_count, x => x >= 1))
                .OrderBy(r =>
                         r.Field(x => x.name, ListSortDirection.Descending)
                         .Field(x => x.social_security_number, ListSortDirection.Descending))
                .Execute()
                .Then()
                .ToArray();
            
            data.Length.Should().Be(3);
            data[0].name.Should().Be("Scott");
            data[0].social_security_number.Should().Be(null);
            data[1].name.Should().Be("Matt");
            data[1].social_security_number.Should().Be(null);
            data[2].name.Should().Be("Matt");
            data[2].social_security_number.Should().Be(null);
        }
        
        [Test]
        public async void ShouldCreateInsertSelect_Ordering()
        {
            IDbms dbms = new Dbms(this.definer);
            
            await dbms.CreateTable<person>()
                .Fields(r =>
                        r.Field(x => x.name)
                        .Field(x => x.social_security_number)
                        .Field(x => x.living_parent_count))
                .Execute();
            
            IDataStore db = new DataStore(this.definer);
            await db.Insert<person>()
                .Record(r =>
                        r.Field(x => x.name, "Todd")
                        .Field(x => x.living_parent_count, 0)
                        .Field(x => x.social_security_number, "123-14-2112"))
                .Execute();
            
            await db.Insert<person>()
                .Record(r =>
                        r.Field(x => x.name, "Scott")
                        .Field(x => x.living_parent_count, 1)
                        .Field(x => x.social_security_number, "123-14-1235"))
                .Execute();
            
            await db.Insert<person>()
                .Record(r =>
                        r.Field(x => x.name, "Matt")
                        .Field(x => x.living_parent_count, 2)
                        .Field(x => x.social_security_number, "123-34-2313"))
                .Execute();
            
            await db.Insert<person>()
                .Record(r =>
                        r.Field(x => x.name, "Matt")
                        .Field(x => x.living_parent_count, 3)
                        .Field(x => x.social_security_number, "123-34-2135"))
                .Execute();
            
            
            var greaterThanEqual_Asc1 = await db.Select<person>()
                .Where(r => r.Field(x => x.living_parent_count, x => x >= 1))
                .OrderBy(r =>
                         r.Field(x => x.name, ListSortDirection.Ascending)
                         .Field(x => x.social_security_number, ListSortDirection.Ascending))
                .Execute()
                .Then()
                .ToArray();
            
            greaterThanEqual_Asc1.Length.Should().Be(3);
            greaterThanEqual_Asc1[0].name.Should().Be("Matt");
            greaterThanEqual_Asc1[0].social_security_number.Should().Be("123-34-2135");
            greaterThanEqual_Asc1[1].name.Should().Be("Matt");
            greaterThanEqual_Asc1[1].social_security_number.Should().Be("123-34-2313");
            greaterThanEqual_Asc1[2].name.Should().Be("Scott");
            greaterThanEqual_Asc1[2].social_security_number.Should().Be("123-14-1235");
            
            
            var greaterThanEqual_Asc2 = await db.Select<person>()
                .Where(r => r.Field(x => x.living_parent_count, x => x >= 1))
                .OrderBy(r =>
                         r.Field(x => x.name, ListSortDirection.Ascending)
                         .Field(x => x.social_security_number, ListSortDirection.Descending))
                .Execute()
                .Then()
                .ToArray();
            
            greaterThanEqual_Asc2.Length.Should().Be(3);
            greaterThanEqual_Asc2[0].name.Should().Be("Matt");
            greaterThanEqual_Asc2[0].social_security_number.Should().Be("123-34-2313");
            greaterThanEqual_Asc2[1].name.Should().Be("Matt");
            greaterThanEqual_Asc2[1].social_security_number.Should().Be("123-34-2135");
            greaterThanEqual_Asc2[2].name.Should().Be("Scott");
            greaterThanEqual_Asc2[2].social_security_number.Should().Be("123-14-1235");
            
            var greaterThanEqual_Desc1 = await db.Select<person>()
                .Where(r => r.Field(x => x.living_parent_count, x => x >= 1))
                .OrderBy(r =>
                         r.Field(x => x.name, ListSortDirection.Descending)
                         .Field(x => x.social_security_number, ListSortDirection.Ascending))
                .Execute()
                .Then()
                .ToArray();
            
            greaterThanEqual_Desc1.Length.Should().Be(3);
            greaterThanEqual_Desc1[0].name.Should().Be("Scott");
            greaterThanEqual_Desc1[0].social_security_number.Should().Be("123-14-1235");
            greaterThanEqual_Desc1[1].name.Should().Be("Matt");
            greaterThanEqual_Desc1[1].social_security_number.Should().Be("123-34-2135");
            greaterThanEqual_Desc1[2].name.Should().Be("Matt");
            greaterThanEqual_Desc1[2].social_security_number.Should().Be("123-34-2313");
            
            
            var greaterThanEqual_Desc2 = await db.Select<person>()
                .Where(r => r.Field(x => x.living_parent_count, x => x >= 1))
                .OrderBy(r =>
                         r.Field(x => x.name, ListSortDirection.Descending)
                         .Field(x => x.social_security_number, ListSortDirection.Descending))
                .Execute()
                .Then()
                .ToArray();
            
            greaterThanEqual_Desc2.Length.Should().Be(3);
            greaterThanEqual_Desc2[0].name.Should().Be("Scott");
            greaterThanEqual_Desc2[0].social_security_number.Should().Be("123-14-1235");
            greaterThanEqual_Desc2[1].name.Should().Be("Matt");
            greaterThanEqual_Desc2[1].social_security_number.Should().Be("123-34-2313");
            greaterThanEqual_Desc2[2].name.Should().Be("Matt");
            greaterThanEqual_Desc2[2].social_security_number.Should().Be("123-34-2135");
        }
        
        [Test]
        public async void ShouldCreateInsertSelect_offsetting()
        {
            IDbms dbms = new Dbms(this.definer);
            
            await dbms.CreateTable<person>()
                .Fields(r =>
                        r.Field(x => x.name)
                        .Field(x => x.social_security_number)
                        .Field(x => x.living_parent_count))
                .Execute();
            
            IDataStore db = new DataStore(this.definer);
            await db.Insert<person>()
                .Record(r =>
                        r.Field(x => x.name, "Todd")
                        .Field(x => x.living_parent_count, 0)
                        .Field(x => x.social_security_number, "123-14-2112"))
                .Execute();
            
            await db.Insert<person>()
                .Record(r =>
                        r.Field(x => x.name, "Scott")
                        .Field(x => x.living_parent_count, 1)
                        .Field(x => x.social_security_number, "123-14-1235"))
                .Execute();
            
            await db.Insert<person>()
                .Record(r =>
                        r.Field(x => x.name, "Matt")
                        .Field(x => x.living_parent_count, 2)
                        .Field(x => x.social_security_number, "123-34-2313"))
                .Execute();
            
            await db.Insert<person>()
                .Record(r =>
                        r.Field(x => x.name, "Matt")
                        .Field(x => x.living_parent_count, 3)
                        .Field(x => x.social_security_number, "123-34-2135"))
                .Execute();
            
            
            var greaterThanEqual_Asc1 = await db.Select<person>()
                .Where(r => r.Field(x => x.living_parent_count, x => x >= 1))
                .Offset(1)
                .Limit(1)
                .OrderBy(r =>
                         r.Field(x => x.name, ListSortDirection.Ascending)
                         .Field(x => x.social_security_number, ListSortDirection.Ascending))
                .Execute()
                .Then()
                .ToArray();
            
            greaterThanEqual_Asc1.Length.Should().Be(1);
            greaterThanEqual_Asc1[0].name.Should().Be("Matt");
            greaterThanEqual_Asc1[0].social_security_number.Should().Be("123-34-2313");
        }
        
        [Test]
        public async void ShouldCreateInsertSelect_Distinct()
        {
            IDbms dbms = new Dbms(this.definer);
            
            await dbms.CreateTable<person>()
                .Fields(r =>
                        r.Field(x => x.name)
                        .Field(x => x.social_security_number)
                        .Field(x => x.living_parent_count))
                .Execute();
            
            IDataStore db = new DataStore(this.definer);
            await db.Insert<person>()
                .Record(r =>
                        r.Field(x => x.name, "Todd")
                        .Field(x => x.living_parent_count, 0)
                        .Field(x => x.social_security_number, "123-14-2112"))
                .Execute();
            
            await db.Insert<person>()
                .Record(r =>
                        r.Field(x => x.name, "Matt")
                        .Field(x => x.living_parent_count, 1)
                        .Field(x => x.social_security_number, "123-14-1235"))
                .Execute();
            
            await db.Insert<person>()
                .Record(r =>
                        r.Field(x => x.name, "Matt")
                        .Field(x => x.living_parent_count, 2)
                        .Field(x => x.social_security_number, "123-34-2313"))
                .Execute();
            
            await db.Insert<person>()
                .Record(r =>
                        r.Field(x => x.name, "Matt")
                        .Field(x => x.living_parent_count, 3)
                        .Field(x => x.social_security_number, "123-34-2135"))
                .Execute();
            
            
            var greaterThanEqual_Asc1 = await db.Select<person>()
                .Distinct()
                .Where(r => r.Field(x => x.living_parent_count, x => x >= 1))
                .OrderBy(r =>
                         r.Field(x => x.name, ListSortDirection.Ascending)
                         .Field(x => x.social_security_number, ListSortDirection.Ascending))
                .Execute()
                .Then()
                .ToArray();
            
            greaterThanEqual_Asc1.Length.Should().Be(3);
            
            var greaterThanEqual = await db.Select<person>()
                .Distinct()
                .Fields(r => r.Field(x => x.name))
                .Where(r => r.Field(x => x.living_parent_count, x => x >= 1))
                .OrderBy(r =>
                         r.Field(x => x.name, ListSortDirection.Ascending)
                         .Field(x => x.social_security_number, ListSortDirection.Ascending))
                .Execute()
                .Then()
                .ToArray();
            
            greaterThanEqual.Length.Should().Be(1);
            greaterThanEqual[0].name.Should().Be("Matt");
            greaterThanEqual[0].social_security_number.Should().BeNull();
        }
        
        
        
        [Test]
        public async void ShouldCreateInsertSelect_InnerJoin()
        {
            IDbms dbms = new Dbms(this.definer);
            
            await dbms.CreateTable<category>()
                .Fields(r =>
                        r.Field(x => x.id)
                        .Field(x => x.name))
                .Execute();
            
            await dbms.CreateTable<article>()
                .Fields(r =>
                        r.Field(x => x.id)
                        .Field(x => x.category_id)
                        .Field(x => x.name))
                .Execute();
            
            IDataStore db = new DataStore(this.definer);
            
            var healthId = Guid.NewGuid();
            var sportsId = Guid.NewGuid();
            var politicsId = Guid.NewGuid();
            
            await db.Insert<category>()
                .Record(r =>
                        r.Field(x => x.id, healthId)
                        .Field(x => x.name, "Health"))
                .Execute();
            
            await db.Insert<category>()
                .Record(r =>
                        r.Field(x => x.id, sportsId)
                        .Field(x => x.name, "Sports"))
                .Execute();
            
            await db.Insert<category>()
                .Record(r =>
                        r.Field(x => x.id, politicsId)
                        .Field(x => x.name, "Politics"))
                .Execute();
            
            await db.Insert<article>()
                .Record(r =>
                        r.Field(x => x.id, Guid.NewGuid())
                        .Field(x => x.category_id, sportsId)
                        .Field(x => x.name, "Cubs Win!!"))
                .Execute();
            
            await db.Insert<article>()
                .Record(r =>
                        r.Field(x => x.id, Guid.NewGuid())
                        .Field(x => x.category_id, sportsId)
                        .Field(x => x.name, "Penguins Win!!"))
                .Execute();
            
            await db.Insert<article>()
                .Record(r =>
                        r.Field(x => x.id, Guid.NewGuid())
                        .Field(x => x.category_id, politicsId)
                        .Field(x => x.name, "Trump Win!!"))
                .Execute();
            
            
            var greaterThanEqual = await db.Select<category>()
                .Join<article>((x,y) =>  x.id == y.category_id)
                .Execute()
                .Then()
                .ToArray();
            
            greaterThanEqual.Length.Should().Be(3);
        }
        
        
        
        [Test]
        public async void ShouldCreateInsertSelect_CrossJoin()
        {
            IDbms dbms = new Dbms(this.definer);
            
            await dbms.CreateTable<category>()
                .Fields(r =>
                        r.Field(x => x.id)
                        .Field(x => x.name))
                .Execute();
            
            await dbms.CreateTable<article>()
                .Fields(r =>
                        r.Field(x => x.id)
                        .Field(x => x.category_id)
                        .Field(x => x.name))
                .Execute();
            
            IDataStore db = new DataStore(this.definer);
            
            var healthId = Guid.NewGuid();
            var sportsId = Guid.NewGuid();
            var politicsId = Guid.NewGuid();
            
            await db.Insert<category>()
                .Record(r =>
                        r.Field(x => x.id, healthId)
                        .Field(x => x.name, "Health"))
                .Execute();
            
            await db.Insert<category>()
                .Record(r =>
                        r.Field(x => x.id, sportsId)
                        .Field(x => x.name, "Sports"))
                .Execute();
            
            await db.Insert<category>()
                .Record(r =>
                        r.Field(x => x.id, politicsId)
                        .Field(x => x.name, "Politics"))
                .Execute();
            
            await db.Insert<article>()
                .Record(r =>
                        r.Field(x => x.id, Guid.NewGuid())
                        .Field(x => x.category_id, sportsId)
                        .Field(x => x.name, "Cubs Win!!"))
                .Execute();
            
            await db.Insert<article>()
                .Record(r =>
                        r.Field(x => x.id, Guid.NewGuid())
                        .Field(x => x.category_id, sportsId)
                        .Field(x => x.name, "Penguins Win!!"))
                .Execute();
            
            await db.Insert<article>()
                .Record(r =>
                        r.Field(x => x.id, Guid.NewGuid())
                        .Field(x => x.category_id, politicsId)
                        .Field(x => x.name, "Trump Win!!"))
                .Execute();
            
            
            var greaterThanEqual = await db.Select<category>()
                .CrossJoin<article>()
                .Execute()
                .Then()
                .ToArray();
            
            greaterThanEqual.Length.Should().Be(9);
        }
        
        [Test]
        public async void ShouldCreateInsertSelect_LeftJoin()
        {
            IDbms dbms = new Dbms(this.definer);
            
            await dbms.CreateTable<category>()
                .Fields(r =>
                        r.Field(x => x.id)
                        .Field(x => x.name))
                .Execute();
            
            await dbms.CreateTable<article>()
                .Fields(r =>
                        r.Field(x => x.id)
                        .Field(x => x.category_id)
                        .Field(x => x.name))
                .Execute();
            
            IDataStore db = new DataStore(this.definer);
            
            var healthId = Guid.NewGuid();
            var sportsId = Guid.NewGuid();
            var politicsId = Guid.NewGuid();
            
            await db.Insert<category>()
                .Record(r =>
                        r.Field(x => x.id, healthId)
                        .Field(x => x.name, "Health"))
                .Execute();
            
            await db.Insert<category>()
                .Record(r =>
                        r.Field(x => x.id, sportsId)
                        .Field(x => x.name, "Sports"))
                .Execute();
            
            await db.Insert<category>()
                .Record(r =>
                        r.Field(x => x.id, politicsId)
                        .Field(x => x.name, "Politics"))
                .Execute();
            
            await db.Insert<article>()
                .Record(r =>
                        r.Field(x => x.id, Guid.NewGuid())
                        .Field(x => x.category_id, sportsId)
                        .Field(x => x.name, "Cubs Win!!"))
                .Execute();
            
            await db.Insert<article>()
                .Record(r =>
                        r.Field(x => x.id, Guid.NewGuid())
                        .Field(x => x.category_id, sportsId)
                        .Field(x => x.name, "Penguins Win!!"))
                .Execute();
            
            await db.Insert<article>()
                .Record(r =>
                        r.Field(x => x.id, Guid.NewGuid())
                        .Field(x => x.category_id, politicsId)
                        .Field(x => x.name, "Trump Win!!"))
                .Execute();
            
            
            var greaterThanEqual = await db.Select<category>()
                .LeftJoin<article>((x,y) =>  x.id == y.category_id)
                .Execute()
                .Then()
                .ToArray();
            
            greaterThanEqual.Length.Should().Be(4);
        }
        
        [Test]
        public async void ShouldCreateInsertSelect_RightJoin()
        {
            IDbms dbms = new Dbms(this.definer);
            
            await dbms.CreateTable<category>()
                .Fields(r =>
                        r.Field(x => x.id)
                        .Field(x => x.name))
                .Execute();
            
            await dbms.CreateTable<article>()
                .Fields(r =>
                        r.Field(x => x.id)
                        .Field(x => x.category_id)
                        .Field(x => x.name))
                .Execute();
            
            IDataStore db = new DataStore(this.definer);
            
            var healthId = Guid.NewGuid();
            var sportsId = Guid.NewGuid();
            var politicsId = Guid.NewGuid();
            
            await db.Insert<category>()
                .Record(r =>
                        r.Field(x => x.id, healthId)
                        .Field(x => x.name, "Health"))
                .Execute();
            
            await db.Insert<category>()
                .Record(r =>
                        r.Field(x => x.id, sportsId)
                        .Field(x => x.name, "Sports"))
                .Execute();
            
            await db.Insert<category>()
                .Record(r =>
                        r.Field(x => x.id, politicsId)
                        .Field(x => x.name, "Politics"))
                .Execute();
            
            await db.Insert<article>()
                .Record(r =>
                        r.Field(x => x.id, Guid.NewGuid())
                        .Field(x => x.category_id, sportsId)
                        .Field(x => x.name, "Cubs Win!!"))
                .Execute();
            
            await db.Insert<article>()
                .Record(r =>
                        r.Field(x => x.id, Guid.NewGuid())
                        .Field(x => x.category_id, sportsId)
                        .Field(x => x.name, "Penguins Win!!"))
                .Execute();
            
            await db.Insert<article>()
                .Record(r =>
                        r.Field(x => x.id, Guid.NewGuid())
                        .Field(x => x.category_id, politicsId)
                        .Field(x => x.name, "Trump Win!!"))
                .Execute();
            
            
            var greaterThanEqual = await db.Select<article>()
                .RightJoin<category>((x,y) =>  x.category_id == y.id)
                .Execute()
                .Then()
                .ToArray();
            
            greaterThanEqual.Length.Should().Be(4);
//
//
            //            await db.Insert<article>()
            //                .Record(r =>
            //                        r.Field(x => x.id, Guid.NewGuid())
            //                        .Field(x => x.category_id, Guid.NewGuid())
            //                        .Field(x => x.name, "MisCategorized"))
            //                .Execute();
//
            //            greaterThanEqual = await db.Select<article>()
            //                .RightJoin<category>((x,y) =>  x.category_id == y.id)
            //                .Execute()
            //                .Then()
            //                .ToArray();
//
            //            greaterThanEqual.Length.Should().Be(4);
        }
 */