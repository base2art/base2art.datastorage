﻿
using System;
using System.Net;
using System.Net.Sockets;
using Base2art.Dapper;
using Base2art.DataStorage.Dapper;

namespace Base2art.DataStorage.MySql.Features
{
    public static class FeatureSetup
    {
        private static readonly Lazy<string> ipAddress;
        
        static FeatureSetup()
        {
            ipAddress = new Lazy<string>(GetLocalIPAddress);
        }

        public static MySqlFormatter Definer()
        {
            var factory = new OdbcConnectionStringFactory(ConnectionString(), "MySql.Data.MySqlClient");
            return new MySql.MySqlFormatter(new DapperExecutionEngine(factory), true, TimeSpan.FromSeconds(10));
        }
        
        public static string ConnectionString()
        {
            var builder = new global::MySql.Data.MySqlClient.MySqlConnectionStringBuilder();
            builder.UserID = "nunit";
            builder.Database = "nunit";
            builder.Password = "nunitUser!";
            builder.Server = IPAddress() == "10.0.2.15" ? "10.0.2.2" : "127.0.0.1";
            builder.Pooling = true;
            
            return builder.ToString();
        }

        public static string IPAddress()
        {
            return ipAddress.Value;
        }
        
        private static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            
            throw new Exception("Local IP Address Not Found!");
        }
    }
}
