﻿
namespace Base2art.DataStorage.Provided.Features
{
    using System;
    using System.Data.SqlClient;
    using Base2art.DataStorage.Specs;
    using NUnit.Framework;

    [TestFixture]
    public class CreateTableFeature
    {
        private IDataStorageProvider definer;

        private CreateTableSpec selecting;

        [SetUp]
        public void BeforEach()
        {
            this.definer = FeatureSetup.Definer();
            this.selecting = new CreateTableSpec(typeof(SqlException), canDropTables: true, throwsExceptionOnOverflow: true);
        }

        [Test]
        public async void ShouldCreateTable(NamedConnectionString cstr)
        {
            IDataStore db = this.definer.CreateDataStoreAccess(cstr);
            IDbms dbms = this.definer.CreateDbmsAccess(cstr);
            await this.selecting.ShouldCreateTable(dbms, db);
        }

        [Test]
        public async void ShouldBeAbleToDeclareFields(NamedConnectionString cstr)
        {
            IDataStore db = this.definer.CreateDataStoreAccess(cstr);
            IDbms dbms = this.definer.CreateDbmsAccess(cstr);
            await this.selecting.ShouldBeAbleToDeclareFields(dbms, db);
        }

        [Test]
        public async void ShouldBeAbleToInferFields(NamedConnectionString cstr)
        {
            IDataStore db = this.definer.CreateDataStoreAccess(cstr);
            IDbms dbms = this.definer.CreateDbmsAccess(cstr);
            await this.selecting.ShouldBeAbleToInferFields(dbms, db);
        }

        [Test]
        public async void ShouldBeAbleToInferRequiredFields(NamedConnectionString cstr)
        {
            IDataStore db = this.definer.CreateDataStoreAccess(cstr);
            IDbms dbms = this.definer.CreateDbmsAccess(cstr);
            await this.selecting.ShouldBeAbleToInferRequiredFields(dbms, db);
        }

        [Test]
        public async void ShouldBeAbleToUpgradeTables(NamedConnectionString cstr)
        {
            IDataStore db = this.definer.CreateDataStoreAccess(cstr);
            IDbms dbms = this.definer.CreateDbmsAccess(cstr);
            await this.selecting.ShouldBeAbleToUpgradeTables(dbms, db);
        }

        [Test]
        public async void ShouldBeAbleToAddKeyes(NamedConnectionString cstr)
        {
            IDataStore db = this.definer.CreateDataStoreAccess(cstr);
            IDbms dbms = this.definer.CreateDbmsAccess(cstr);
            await this.selecting.ShouldBeAbleToAddKeyes(dbms, db);
        }

        [Test]
        public async void ShouldBeAbleToAddIndexes(NamedConnectionString cstr)
        {
            IDataStore db = this.definer.CreateDataStoreAccess(cstr);
            IDbms dbms = this.definer.CreateDbmsAccess(cstr);
            await this.selecting.ShouldBeAbleToAddIndexes(dbms, db);
        }
    }
}



