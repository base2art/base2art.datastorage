﻿
namespace Base2art.DataStorage.Provided.Features
{
    using System;
    using Base2art.DataStorage.Specs;
    using NUnit.Framework;

    [TestFixture]
    public class AllDataTypeInteractionFeature
    {
        private IDataStorageProvider definer;

        private AllDataTypeInteractionSpec selecting;

        [SetUp]
        public void BeforEach()
        {
            this.definer = FeatureSetup.Definer();
            this.selecting = new AllDataTypeInteractionSpec(canDropTables: true);
        }

        [Test]
        public async void CreateInsertSelectDelete(NamedConnectionString cstr)
        {
            IDataStore db = this.definer.CreateDataStoreAccess(cstr);
            IDbms dbms = this.definer.CreateDbmsAccess(cstr);
            await this.selecting.CreateInsertSelectDelete(dbms, db);
        }

        [Test]
        public async void CreateAllGenerically(NamedConnectionString cstr)
        {
            IDataStore db = this.definer.CreateDataStoreAccess(cstr);
            IDbms dbms = this.definer.CreateDbmsAccess(cstr);
            await this.selecting.CreateAllGenerically(dbms, db);
        }
    }
}

