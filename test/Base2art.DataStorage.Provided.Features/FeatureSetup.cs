﻿namespace Base2art.DataStorage.Provided.Features
{
    using System;
    using System.IO;
    using System.Net;
    using System.Net.Sockets;
    using Base2art.Dapper;
    using Base2art.DataStorage.Dapper;
    
    public static class FeatureSetup
    {
        private static readonly Lazy<string> ipAddress;
        
        static FeatureSetup()
        {
            ipAddress = new Lazy<string>(GetLocalIPAddress);
        }

        public static IDataStorageProvider Definer()
        {
            return null;
//            var cstr = ConnectionString();
//            var factory = new OdbcConnectionStringFactory(cstr, "System.Data.SqlClient");
//            
//            return new SqlFormatter(new DapperExecutionEngine(factory), true);
        }
        
        public static string ConnectionString()
        {
            var path = Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                "base2art",
                "data-storage",
                "connectionString-sqlserver.config");
            
            if (File.Exists(path)) 
            {
                return File.ReadAllText(path).Replace("\r", "\n").Replace("\n", "");
            }
            
            var builder = new System.Data.SqlClient.SqlConnectionStringBuilder();
            builder.InitialCatalog = "nunit";
            builder.DataSource = ".";
            builder.Pooling = true;
            builder.IntegratedSecurity = true;
            
            return builder.ToString();
        }

        public static string IPAddress()
        {
            return ipAddress.Value;
        }
        
        private static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            
            throw new Exception("Local IP Address Not Found!");
        }
    }
}
