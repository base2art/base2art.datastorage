
if (Test-Path "C:\Users\tyoung\code\.nuget\NuGet.Exe") {
  C:\Users\tyoung\code\.nuget\NuGet.Exe restore
}


C:\Windows\Microsoft.NET\Framework64\v4.0.30319\MSBuild.exe /t:Clean /p:Configuration=Release /m
C:\Windows\Microsoft.NET\Framework64\v4.0.30319\MSBuild.exe /t:Build /p:Configuration=Release /m


cd conf

$specs = Get-ChildItem -Filter "*.nuspec"

foreach ($item in $specs) {

  ..\packages\NuGet.CommandLine.3.4.3\tools\NuGet.exe pack  $item.Name
}

$packs = Get-ChildItem -Filter "*.nupkg"
if (-Not (Test-Path "..\output\")) {
  mkdir -Path "..\output\"
}


Set-Content -Path "..\nugetpush.ps1" -Value ""
$apiKey = Get-Content -Path "~\code\nuget-key.api"
foreach ($item in $packs) {
  mv $item.FullName "..\output\" -Force
  Add-Content -Path "..\nugetpush.ps1" -Value ".\packages\NuGet.CommandLine.3.4.3\tools\NuGet.exe push .\output\$($item.Name)  $($apikey) -Source https://www.nuget.org/api/v2/package"
}



cd ..

